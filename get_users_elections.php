<?php	
ini_set('display_errors', 1);
error_reporting(E_ALL);
	
//require_once('login.php');
	
//require_once('classroomloadproperties.php');	

// [[Workaround code]]
$sessionID = isset($_POST['sessionID']) ? $_POST['sessionID'] : "";
        

$showElections = 20;

if (isset($_GET['showelections']))

{

	if (is_numeric ($_GET['showelections']))

	{

		if ($_GET['showelections']>=0)

		{
			
			$showElections = $_GET['showelections'];
	
		}
	
	}

}

$moreElections = false;
	

$statement = $db->prepare("SELECT count(sessionID) FROM Sessions WHERE Sessions.username=? AND hidden = false");
$statement->bindValue(1, isset($sessionID) ? $sessionID : "");
$statement->execute();	
$row = $statement->Fetch();
if ($row[0] > $showElections)

{

	$moreElections = true;
		
}



$statement = $db->prepare("SELECT sessionID, created, passcodetype, passcode, title,locked FROM Sessions WHERE Sessions.username=? AND hidden=false ORDER BY sessionID DESC limit ?");
$statement->bindValue(1, $_SESSION['USERNAME']);
$statement->bindValue(2, (int)$showElections,PDO::PARAM_INT);
$statement->execute();	

$results = array();

while($row = $statement->Fetch())

{

       	$election = array();

	//Created

	$is_created = $row[1];
		
	if($is_created == '0' || $is_created == 0 ) $is_created = "Generating";

	else $is_created = "Ready";


	//Passcode type

	$passcode_type = ucfirst($row[2]);


	//Passcode

	if ($passcode_type == "Individual") 
		$passcode = "<a href=\"../getpasscodes.php?sessionid=$row[0]\">Click Here</a>";

	else if ($passcode_type == "Group")
	{	
		$passcode = "<a onclick=\"document.getElementById('passcode".$row[0]."').innerHTML='".$row[3]."';return false;\">Click Here</a>";
	}
			
	else
	{
		$passcode = "None";
	}
	//echo($passcode);

	//Title
              
	$title = $row[4];
      
        if(is_null($title) || $title=="") $title = "&lt;None&gt;";

     
        //Truncate title to 30 chars
            
        $title = truncate_title($title);



	$election = array($row[0], $is_created, $passcode_type, $passcode, $title,$row[5],false,false);
			
				array_push($results, $election);

}

  

foreach ($results as &$election)
		
{
		
	$sessionID=$election[0];


	$statement = $db->prepare("SELECT COUNT(*) FROM (SELECT questionText FROM Type12Questions WHERE sessionID = ? AND finished = false UNION SELECT questionText FROM Type3Questions WHERE finished = false AND sessionID =? UNION SELECT questionText FROM Type4Questions WHERE sessionID = ? AND finished = false) as questions");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $sessionID);
	$statement->bindValue(3, $sessionID);
	$statement->execute();			
	$row = $statement->Fetch();			
				
	if ($row[0]=="0")
		
	{
					
		$election[6] = true;
				
	}

}
							
	
//check if any ballots have been allocated or cast
	
foreach ($results as &$election)
	
{
	
	$sessionID=$election[0];

	$statement = $db->prepare("SELECT COUNT(*) FROM (SELECT status FROM Type12Status WHERE sessionID = ? AND NOT status = 0 UNION SELECT sessionID FROM Type3Answers WHERE sessionID =? UNION SELECT sessionID FROM Type4Answers WHERE sessionID = ?) as statuses");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $sessionID);
	$statement->bindValue(3, $sessionID);
	$statement->execute();			
	$row = $statement->Fetch();
				
	if ($temp!="0")
			
	{
			
		$election[7] = true;
		
	}
					
}
		



function truncate_title($title)
        
{
                	if(strlen($title) > 30) return substr($title, 0, 30) . '...';


	return $title;
        
}

?>
