<?php 
ini_set('display_errors', 1);
error_reporting(E_ALL);

	require_once ('./classroominclude.php');
	require_once ('./gethash.php');
	$smarty->assign('highlighted','bulletinboard');
	session_start();
	if (isset($_SESSION['USERNAME']))
	{
		$smarty->assign('username',$_SESSION['USERNAME']);
	}
	if(isset($_SESSION['is_admin']))
	{
			$smarty->assign('admin','true');
	}	
	if (isset($_GET['sessionid']))
  	{
		///query database for this session ID and return true if it exists
		// $sessionID=mysql_real_escape_string($sessionID); // PDO already does it, in more recent versions of PHP it may give an error/warning and clear the string
		
		$sessionID = $_GET['sessionid'];

		$smarty->assign('sessionID',$sessionID);
		
		
		if ($sessionID == "")
		{		
			$smarty->display('allresultsmenu.tpl');
			exit;
		}

		//check if this election is hidden
		
		
		//Get title
		$statement = $db->prepare("SELECT title,hidden FROM Sessions WHERE sessionID = ?");
		$statement->bindValue(1, $sessionID);	
		$statement->execute();	
		if($row = $statement->Fetch())
		{
			$title = $row[0];
			if($title == "") $title = "&lt;None&gt;";
			$smarty->assign('sessionTitle',$title);
			if ($row[1])
			{
				$smarty->display('allresults/hidden.tpl');
				exit;
			}
		}
		else
		{
			//no such election
			$smarty->display('allresults/allresultsmenu.tpl');
			exit;
		}

	
		$results = null;
		$statement = $db->prepare("SELECT finished,questionText,questionID FROM Type12Questions WHERE sessionID = ?");
		$statement->bindValue(1, $sessionID);	
		$statement->execute();		
		while($row = $statement->Fetch())
		{
			$results[$row[2]]['type'] = "12";
			$results[$row[2]]['questiontext'] = $row[1];
			$results[$row[2]]['questionnumber'] = $row[2];
			if ($row[0] == true)
			{	
				$results[$row[2]]['finished']=true;
				$counter = 0;
				$statement2 = $db->prepare("SELECT answerText,votesCast FROM Type12Answers WHERE sessionID = ? AND questionID = ? ORDER BY answerNumber");
				$statement2->bindValue(1, $sessionID);	
				$statement2->bindValue(2, $row[2]);
				$statement2->execute();	
				while ($row2 = $statement2->Fetch())
				{
					$results[$row[2]]['results'][$counter]['answer']=$row2[0];
					$results[$row[2]]['results'][$counter]['votes']=$row2[1];	
					$counter++;					
				}			

			}
			else
			{
				$results[$row[2]]['finished']=false;				
			}
		}
		$statement = $db->prepare("SELECT questionText,questionID,finished FROM Type3Questions WHERE sessionID = ?");
		$statement->bindValue(1, $sessionID);	
		$statement->execute();		

		while($row = $statement->Fetch())
		{
			$results[$row[1]]['questiontext'] = $row[0];
			$results[$row[1]]['questionnumber'] = $row[1];			
			$results[$row[1]]['finished'] = $row[2];			
			$results[$row[1]]['type'] = "3";
			$statement2 = $db->prepare("SELECT answer, COUNT(answer) FROM Type3Answers WHERE sessionID= ? AND questionID = ? GROUP BY answer ORDER BY COUNT(answer) DESC");
			$statement2->bindValue(1, $sessionID);	
			$statement2->bindValue(2, $row[1]);
			$statement2->execute();			

			$counter = 0;
			while ($row2 = $statement2->Fetch())
			{
				$results[$row[1]]['results'][$counter]['answer']=$row2[0];
				$results[$row[1]]['results'][$counter]['votes']=$row2[1];
				$counter++;					
			}

		}

		$statement = $db->prepare("SELECT questionText,questionID,finished FROM Type4Questions WHERE sessionID = ?");
		$statement->bindValue(1, $sessionID);	
		$statement->execute();		

		while($row = $statement->Fetch())
		{
			$results[$row[1]]['questiontext'] = $row[0];		
			$results[$row[1]]['finished'] = $row[2];			
			$results[$row[1]]['type'] = "4";
			$statement2 = $db->prepare("SELECT answer FROM Type4Answers WHERE sessionID= ? AND questionID = ?");
			$statement2->bindValue(1, $sessionID);	
			$statement2->bindValue(2, $row[1]);
			$statement2->execute();	
			$counter = 0;
			while ($row2 = $statement2->Fetch())
			{
				$results[$row[1]]['results'][$counter]['answer']=$row2[0];	
				$counter++;					
			}

		}
		if (!is_null($results))
		{
			ksort($results);
		}
		$smarty->assign('results',$results);
		$smarty->display('allresults/allresults.tpl');

	}
	else
	{
		$smarty->display('allresults/allresultsmenu.tpl');
	}


?>
