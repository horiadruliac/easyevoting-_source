 <?php 
	require_once ('./classroominclude.php');
	require_once ('./auth2.php');
	$smarty->assign('highlighted','manageelections');
	if (isset($_SESSION['USERNAME']))
	{
		$smarty->assign('username',$_SESSION['USERNAME']);
	}
	if(isset($_SESSION['is_admin']))
	{
		$smarty->assign('admin','true');
	}		
	if ( isset($_POST['sessionid']) && isset($_POST['questionid']))
	{
		
		$sessionID=$_POST['sessionid'];
		//check that the session belong to this user
		$statement = $db->prepare("SELECT * FROM Sessions WHERE sessionID= ? AND username = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $_SESSION['USERNAME']);	
		$statement->execute();	
		$row = $statement->Fetch();
		if (!$row)
		{
			$smarty->display('noauth.tpl');
			exit;
		}
	
	
		//check if this is a valid type 3 question
		$statement = $db->prepare("SELECT * FROM Type3Questions WHERE sessionID = ? AND questionID = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $_POST['questionid']);	
		$statement->execute();
		$row = $statement->Fetch();
		if (!$row)
		{
			//it was type three so finish
                        $statement = $db->prepare("UPDATE Type3Questions SET finished = true WHERE sessionID = ? AND questionID = ?");
                        $statement->bindValue(1, $sessionID);
                        $statement->bindValue(2, $_POST['questionid']);	
                        $statement->execute();
		}
		else
		{
                        $statement = $db->prepare("SELECT * FROM Type4Questions WHERE sessionID = ? AND questionID = ?");
                        $statement->bindValue(1, $sessionID);
                        $statement->bindValue(2, $_POST['questionid']);	
                        $statement->execute();
                        $row = $statement->Fetch();
                        if (!$row)
			{
				//it was type four so finish
                                $statement = $db->prepare("UPDATE Type4Questions SET finished = true WHERE sessionID = ? AND questionID = ?");
                                $statement->bindValue(1, $sessionID);
                                $statement->bindValue(2, $_POST['questionid']);	
                                $statement->execute();
			}
			else
			{
				$sessionID=$_POST['sessionid'];
				$questionNumber=$_POST['questionid'];
				//check if this is a valid type 1 or 2 question
				$statement = $db->prepare("SELECT * FROM Type12Questions WHERE sessionID = ? AND questionID = ?");
                                $statement->bindValue(1, $sessionID);
                                $statement->bindValue(2, $questionNumber);	
                                $statement->execute();
                                $row = $statement->Fetch();
                                if (!$row)
				{
					global $baseurl;
					// post request to signature generator
					$url = "https://".$baseurl."/dofinishquestion.php?sessionid=".$sessionID."&questionid=".$questionNumber;

					$ch = curl_init();
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); 
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
					curl_setopt($ch, CURLOPT_URL,$url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_TIMEOUT, 1);

					$data = curl_exec($ch); 
					//echo($data);
					curl_close($ch);
		//			$data = trim($data);
		//			$data = urldecode($data);
	//test
		//			return $data;
		
				
				}
			
				else
				{
					$smarty->display('noauth.tpl');
					exit;
				}
			}
		}

				
		$smarty->display('finishedquestion.tpl');
			
				
			
	}
	else
	{
		$smarty->display('noauth.tpl');
	}
?>