 <?php 
	require_once ('./include.php');
	require_once ('./auth.php');

		$statement = $db->prepare("SELECT * FROM BPA_Coordinators WHERE ID =?");
		$statement->bindValue(1, $_SERVER['PHP_AUTH_USER']);	
		$statement->execute();		
		$row = $statement->Fetch();
		if ($row == false)
		{
 			//not an administrator so display polite no page.
			$smarty->display('BPA/notcoordinator.tpl');
		}
		else
		{
			// check if post variables set to generate election
			if (isset($_POST['title']) )  
			{
					
		
				// generate election ID				
				$submissionid = 0;
				$starttime ="";
				$endtime = "";
				// Original: $timestamp = strtotime(mysql_real_escape_string($_POST['startdate']));
				$timestamp = strtotime($_POST['startdate']);

				if ($timestamp == false)
				{
					$smarty->display('BPA/incorrectdate.tpl');
					exit;
				}
				else
				{
					$starttime = date( 'Y-m-d H:i:s', $timestamp );		
				}
				//Original: $timestamp2 = strtotime(mysql_real_escape_string($_POST['enddate']));
				$timestamp2 = strtotime($_POST['enddate']);	
				if ($timestamp2 == false)
				{
					$smarty->display('BPA/incorrectdate.tpl');
					exit;
				}
				else if ($timestamp2 < $timestamp)
				{
					$smarty->display('BPA/endbeforestart.tpl');
					exit;
				}
				else if ($timestamp2 < time())
				{
					$smarty->display('BPA/enddatepast.tpl');
					exit;
				}
				else
				{
					$endtime = date( 'Y-m-d H:i:s', $timestamp2 );
				}
				$statement = $db->prepare("SELECT ID FROM BPA_Submissions ORDER BY ID DESC");
				$statement->execute();		

				if ($row = $statement->Fetch())
				{
					$submissionid = ($row[0] + 1); 
				}

				// create entry in election table
				$statement = $db->prepare("INSERT INTO BPA_Submissions VALUES (?,?,?,?,null)");
				$statement->bindValue(1, $submissionid);	
				$statement->bindValue(2, $starttime);
				$statement->bindValue(3, $endtime);
				//Original: $statement->bindValue(4, mysql_real_escape_string($_POST['title']));
				$statement->bindValue(4, $_POST['title']);
				$statement->execute();		

				// Done this way for legacy reasons, probably not a good way to do it		
				// create Candidates_ElectionID table
				//uses dynamic table names for legacy reasons
				//TO DO refactor to not do this

				// Original: $statement = $db->prepare("CREATE TABLE BPA_Papers_".mysql_real_escape_string($submissionid)." (ID int, Submitter varchar(10), Title varchar(255), Paper mediumblob, AuthorList text, BibDetails text, Testimony text, PaperFileType VARCHAR(45), PRIMARY KEY(ID));");

				$statement = $db->prepare("CREATE TABLE BPA_Papers_".$submissionid." (ID int, Submitter varchar(10), Title varchar(255), Paper mediumblob, AuthorList text, BibDetails text, Testimony text, PaperFileType VARCHAR(45), PRIMARY KEY(ID));");	
				$statement->execute();


				//populate table
			

								
				// display page with link to election administration page
				$smarty->display('BPA/submissioncreated.tpl');
			
			}
			else
			{


				$smarty->assign('username',$_SERVER['PHP_AUTH_USER']);
				$smarty->display('BPA/createsubmissions.tpl');
			}
		}	
?>