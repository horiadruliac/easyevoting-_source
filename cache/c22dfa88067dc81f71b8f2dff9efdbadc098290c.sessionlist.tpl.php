<?php /*%%SmartyHeaderCode:10387348925b72c66ea19427-20203251%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c22dfa88067dc81f71b8f2dff9efdbadc098290c' => 
    array (
      0 => './templates/sessionlist/sessionlist.tpl',
      1 => 1531385407,
      2 => 'file',
    ),
    'c34f5fd2cb471064ebd2ae578855609cd85c9a06' => 
    array (
      0 => '/var/www/html/evoting/templates/navbar.tpl',
      1 => 1531385397,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10387348925b72c66ea19427-20203251',
  'variables' => 
  array (
    'sessions' => 0,
    'admin' => 0,
    'session' => 0,
    'moreelections' => 0,
    'showelections' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5b72c66eadc993_45182342',
  'cache_lifetime' => 3600,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b72c66eadc993_45182342')) {function content_5b72c66eadc993_45182342($_smarty_tpl) {?><!DOCTYPE html> 
<html lang="en">
<head>
<title>Newcastle University E-Voting: View All Elections</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap-responsive.php" />
<link rel="stylesheet" type="text/css" href="./css/extra-css.css" />
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="./js/html5shim.js"></script>
<link rel="stylesheet" type="text/css" href="./css/ie8.css" />	  
    <![endif]-->

</style>
</head>
<body>

<!--Begin navbar-->
<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container-fluid">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<a class="brand" href="./index.php">Newcastle University E-Voting</a>
				<div class="nav-collapse collapse">
											<span class="navbar-text pull-right">
							<a href="./manage.php" class="navbar-link">(Log in)</a>
						</span>
					                    <ul class="nav">
											<li><a href="./index.php">Home</a></li>
															
						<li class="active"><a href="./sessionlist.php">View All Elections</a></li>
																<li><a href="./classroombulletinboard.php">Bulletin Board</a></li>
																<li><a  href="./help.php">Help</a></li>
																<li><a href="./contact.php">Contact</a></li>
															</ul>
                </div><!--/.nav-collapse -->
        </div>
    </div>
</div>
         <!--End Nav Bar -->








<div class="container-narrow">
<div class="jumbotron">
<h1>Public Elections</h1>
<p class="lead">View the status, creator and election ID of every election</p>
</div>
<div id = "maintext">
<div class="jumbotron">
<h2> There are currently no sessions available.</h2>
</div>
<br/>
</div>
</div>

<script src="./js/jquery.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/sessionlist.js"></script>
</body>
</html>
<?php }} ?>