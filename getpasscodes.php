<?php 
	require_once ('./classroominclude.php');
	require_once ('./auth2.php');
	$smarty->assign('highlighted','manageelections');
	if(isset($_SESSION['is_admin']))
	{
		$smarty->assign('admin','true');
	}	
	if ( isset($_GET['sessionid']))
  	{
		$sessionID = $_GET['sessionid'];
		if ($sessionID =="")
		{
			$smarty->display('passcodechooser.tpl');
			exit;
		}
		
		//check election belongs to user
        

		$statement = $db->prepare("SELECT passcodeType,username FROM Sessions WHERE sessionID=?");
		$statement->bindValue(1, isset($sessionID) ? $sessionID : "");
		$statement->execute();	
		$row = $statement->Fetch();

				//check session belongs to this user
		if($row[1] != $_SESSION['USERNAME'])
		{
			$smarty->display('noauth.tpl');
			exit();
		}
		if ($row[0] == "individual")
		{
			//get all passcodes
			$statement = $db->prepare("SELECT userID FROM Users WHERE sessionID=?");
			$statement->bindValue(1, isset($sessionID) ? $sessionID : "");


			//$passcodes = null;
			//$counter = 0;

			$passcodes = array();

			while ($row = $statement->Fetch())
			{
				array_push($passcodes, $row[1]);
				//$passcodes[$counter]=$row[0];
				//$counter++;
			}
			ksort($passcodes);
			$smarty->assign('sessionid',$sessionID);
			$smarty->assign('passcodes',$passcodes);
			if (isset($_GET['type']) && $_GET['type'] == "xml")
			{
				$smarty->display('passcodexml.tpl');
			}
			else
			{			
				$smarty->display('getpasscodes.tpl');
			}
		}
		else
		{
			$smarty->display('passcodechooser.tpl');
		}
		
	}
	else
	{
		$smarty->display('passcodechooser.tpl');
	}
		
		


?>
