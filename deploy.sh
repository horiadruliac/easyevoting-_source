#!/bin/bash

DIRNAME="e-voting-webcore"
TARNAME="e-voting-webcore.tar.gz"

(
cd ..
tar \
    --exclude='deploy.sh' \
    --exclude='.git*' \
    --exclude='.directory' \
    --exclude='nbproject' \
    --exclude='voteservices/classroom.properties' \
    --exclude="$TARNAME" \
    -zcf "$DIRNAME/$TARNAME" "$DIRNAME"
)