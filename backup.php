<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

require_once ('./classroominclude.php');

require_once('auth2.php');

require('./fpdf/fpdf.php');


if(isset($_SESSION['is_admin']))

{

	$smarty->assign('admin','true');

}

$smarty->assign('highlighted','manageelections');


$sessionID=null;
if(isset($_GET['sessionid']) && ctype_digit($_GET["sessionid"]))
{
$sessionID = $_GET['sessionid'];
	}

	
class PDF extends FPDF 
	{

		
private $electionTitle;
private $sessionId;

		
public function __construct($title, $sessionId)

		{

			$this->electionTitle = $title;
			$this->sessionId = $sessionId;

			parent::__construct();

		}


                function Header()
 		{

                        $this->Image('./img/ncl-logo.png',10,11,35);

                        $this->SetFont('Helvetica', '', 20);

                        $this->Cell(45);

                        $this->Cell(30,10,"Session " . $this->sessionId." ".$this->electionTitle,0,0,'B');
                        						$this->Ln(20);

                }


                function Footer() 
		{

                        $this->SetY(-15);

                        $this->SetFont('Helvetica', '', 10);

                        $this->Cell(0,10,'Page ' . $this->PageNo() . ' of {nb}',0,0,'C');
                		}

        	}
	
// check if this is the user's election or not

	$statement = $db->prepare("SELECT count(*) FROM Sessions WHERE Sessions.username=? AND Sessions.sessionID = ? AND hidden = false");
	$statement->bindValue(1, $_SESSION['USERNAME']);
	$statement->bindValue(2, $sessionID);	
	$statement->execute();	
	$row = $statement->Fetch();
	if ($row[0] == 0)
	
	{

		$smarty->display("noauth.tpl");

		exit();

	}
	
	$title="";


	$statement = $db->prepare("SELECT title FROM Sessions WHERE sessionID=?");
	$statement->bindValue(1, $sessionID);	
	$statement->execute();		
	$row = $statement->Fetch();
	if ($row[0] != "None")

	{
				
		$title = $row[0];

	}




	
$pdf = new PDF($title, $sessionID);

	$pdf->AliasNbPages();

	$pdf->AddPage();

	$pdf->SetFont('Helvetica','',18);

	$pdf->Ln(10);

	$pdf->Cell(10);
	$questions = null;


	$statement = $db->prepare("SELECT questionID,questionText,questionID FROM(SELECT questionID,questionText,'12' AS type FROM Type12Questions WHERE sessionID=? UNION SELECT questionID,questionText,'3' AS type FROM Type3Questions WHERE sessionID=? UNION SELECT questionID,questionText,'4' AS type FROM Type4Questions WHERE sessionID=?) AS results");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $sessionID);	
	$statement->bindValue(3, $sessionID);
	$statement->execute();	
	while($row = $statement->Fetch())
 
	{
            $questionID = $row["questionID"];

		$questions[$questionID]['number']=$row[0];

		$questions[$questionID]['text']=$row[1];

		$questions[$questionID]['type']=$row[2];
	}






	

	
foreach($questions as $question)

	{

		$pdf->Ln(10);

		$pdf->Cell(10);
	
	$pdf->MultiCell(0,10,"Question ".$question['number'].": ".$question['text'],0);

		if ($question['type']=="12")

		{

			$statement = $db->prepare("SELECT answerNumber,answerText FROM Type12Answers WHERE sessionID=? AND questionID =?");
			$statement->bindValue(1, $sessionID);
			$statement->bindValue(2, $question['number']);	
			$statement->execute();			
	while($row = $statement->Fetch())
 
			{
		

				$pdf->Cell(10);
	
			$pdf->MultiCell (0,10,"Answer ".$row[0].": ".$row[1],0);
	
		}






		
}

	}

	
// $con->close(); // ? What should it be?

	
$pdf->Output();


