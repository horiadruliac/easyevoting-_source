<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

	require_once ('./base2n.php');
	
	function generateReceipt ($xmlString,$length)
	{
		//transform xmlstring by removing signature
		$tempString = preg_split("%<signature>%",$xmlString);
		$tempString2 = preg_split("%</signature>%",$xmlString);
		$xmlString = $tempString[0].$tempString2[1];
		
		//get hash of this 
		if ($length == 0)
		{
			return getHash($xmlString);
		}
		return substr(getHash($xmlString),0,$length);
	
	}

	function getHash ($xmlString)
	{
//			global $baseurl;
//
//				// post request to signature generator
//				$url = "http://localhost:4445";
//				$post_string = '<?xml version="1.0" encoding="UTF-8"? >'.$xmlString;
//				$header  = "POST HTTP/1.0 \r\n";
//				$header .= "Content-type: text/xml \r\n";
//				$header .= "Content-length: ".strlen($post_string)." \r\n";
//				$header .= "Content-transfer-encoding: text \r\n";
//				$header .= "Connection: close \r\n\r\n"; 
//				$header = $post_string;
//
//				$ch = curl_init();
//				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
//				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
//				curl_setopt($ch, CURLOPT_URL, $url);
//				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//				curl_setopt($ch, CURLOPT_TIMEOUT, 10);
//				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $header);
//				
//				$data = curl_exec($ch); 
//                                if ($data === false) {
////                                    echo "[[ get hash curl error: ".curl_errno($ch)." ]]<br>"; die();
//                                }
//				
//
//				$data = trim($data);
//				$data = urldecode($data);
//	
//				curl_close($ch);
				
				//return $data;
				
				$temp = hash("sha512",$xmlString,true);
				//$temp = pack("H*",$temp);
				$base32 = new Base2n(5, '0123456789ABCDEFGHJKMNPQRSTVWXYZ', FALSE, TRUE, TRUE);
				$encoded = $base32->encode($temp);
				return $encoded;
	}
    

?>