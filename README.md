# e-voting webcore #

This is the old-version and standalone core for the e-voting system.

## Overview of related projects

* [e-voting-webcore](git clone git@bitbucket.org:NitroGL/e-voting-webcore.git) - Old monolithic version (core + webapp)
* [e-voting](git clone git@bitbucket.org:NitroGL/e-voting.git) - The core (should include database, services, no view is strictly required)
* [e-voting-webapp](git clone git@bitbucket.org:NitroGL/e-voting-webcore.git) - This should be the view interfacing the core (e-voting) working from browsers
* [e-voting-mobile](git clone git@bitbucket.org:NitroGL/e-voting-webcore.git) - This should be the view interfacing the core (e-voting) working as mobile application

### About ###

* E-voting system made easy.
* 0.1

### How do I get set up? ###

Required software and configuration.

* Apache/Web inside-campus server with
    * php and rewrite modules enabled,
    * write access to the served directory,
    * protect sensible files (.htaccess files are already provided: In Apache, use "AllowOverride All" to enable their usage).
* PHP >= 5 with
    * curl
    * rewrite engine (maybe?)
    * pdo, pdo_mysql
    * ldap
* MySQL/MariaDB server
* JRE/OpenJRE

To get it working you need to:

* Create a (password protected) user or choose an existing one, then create a database; for example,

    ``CREATE USER 'evteam'@'localhost' identified by 'password';``

    ``GRANT ALL PRIVILEGES ON `evteam_%`.* to 'evteam'@'localhost';``

    ``CREATE DATABASE `evteam_classroom_voting`;``

* Fill up the database with structure and initial data.
    * Import the file "evteam_classroom_voting-fresh.sql". For example,

    ``$ mysql -u evteam -p evteam_classroom_voting < evteam_classroom_voting-fresh.sql``

    * (?) Fill the Constants table with your public and private key pair (2 rows).
    * (?) Fill the ECParameters table (1 row).

* Copy the whole system to some location served by your apache server. For example,

    ``$ cd /var/www/html/``
    ``$ tar zxf e-voting-webcore.tar.gz``

* Configure the .htaccess (if you make use of them) to rewrite the base "e-voting-webcore" (or the directory you serve).

* The e-voting web application is configured through "voteservices/classroom.properties". You should
    * protect the configuration files from unwanted access,
    * fill in the configuration keys for your system,
    * (you can find a sample configuration in the file "voteservices/classroom.properties-sample").
* Run voteservices through the BallotGenerator.sh script in systems with bash and make sure that two sockets are listening to ports 4449 and 4450.
  Alternatively, you can prepare a batch to run at boot for Windows based machines.


### Publication ###
Siamak F. Shahandashti and Feng Hao, "DRE-ip: A Verifiable E-Voting Scheme without Tallying Authorities," the 21st European Symposium on Research in Computer Security (ESORICS), 2016. [[PDF](https://eprint.iacr.org/2016/670.pdf)]

This paper presents a new "self-enforcing e-voting" system called DRE-ip. Similar to DRE-i (Hao et al. USENIX JETS 2014), DRE-ip provides end-to-end (E2E) verifiability without tallying authorities. But, instead of using pre-computation as in DRE-i, DRE-ip opts for real-time computation, and hence has the advantage in providing stronger guarantee on ballot privacy. Both protocols can be generically implemented for Internet voting as well as polling station voting. However, due to the different underlying computation strategies, DRE-i is particularly suitable for Internet voting while DRE-ip is more suitable for polling station voting. **A patent on DRE-ip has been filed**.

### Best Paper Award (BPA) ###
* The entry point to this submission system is BPA/coordinator.php

### To-Do ###
* Adjust the fresh database

### Known issues ###
* If an election got created, just refreshing (and re-sending post data) will create a new fresh election (FLOOD vulnerability).
* Accessing directly to the page ./generatesession.php will bring the user to a weird template similar to ./coordinator.php.
* Unlocking election does not refresh the page - "Unlock" is still a selectable choice.
* You can create elections of type 1/2 with a single answer (supporting North Korea/Zimbabwe?).

