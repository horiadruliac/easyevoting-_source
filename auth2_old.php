<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

require_once ('./classroominclude.php');

require_once './google/google-api-php-client-2.2.2/vendor/autoload.php';
//to allow post from index
header('Access-Control-Allow-Origin: *');

//get google login info
//$client = new Google_Client();
//$client->setAuthConfigFile('client_secret.json');
//$client->addScope("https://www.googleapis.com/auth/userinfo.email");
/*
session_start();
$client = new Google_Client();
//$client->setAuthConfigFile('client_secret.json');
$client->setClientId("654761778841-u7pd2rmg0fa91qu0mrfdj8vma3akse2s.apps.googleusercontent.com");
$client->setClientSecret("7L4uX1a3vU96H77aKKtccZfl");

$client->addScope("https://www.googleapis.com/auth/userinfo.profile");
$client->addScope("https://www.googleapis.com/auth/userinfo.email");

$plus = new Google_Service_Plus($client);

$me = $plus->people->get("me");
$email = $person['emails'][0]['value'];

*/

/*

if (isset($_GET['code'])) {
    $client->authenticate($_GET['code']);
    $_SESSION['access_token'] = $client->getAccessToken();
	
    $redirect_uri = 'https://' . $_SERVER['HTTP_HOST'];
    header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
    //$redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
    //header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));

}*/

/*
if (!isset($_GET['code'])) {
	    header('HTTP/1.0 401 Unauthorized');
    echo "This page is unavailable without a valid login";
  //$auth_url = $client->createAuthUrl();
  //header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
} else {
  $client->authenticate($_GET['code']);
  $plus = new Google_Service_Plus($client);
  $person = $plus->people->get('me');
  $email = $person['emails'][0]['value'];
  $emailcheck = mysqli_query($conn, "SELECT * FROM Usernames WHERE Username='".$email."' ");
  if (mysqli_num_rows($emailcheck) == 1) {
    $_SESSION["access_token"] = $client->getAccessToken();
    $redirect_uri = 'https://' . $_SERVER['HTTP_HOST'];
    header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
  } else {
	  
	  //create user 
    //$redirect_uri = 'https://' . $_SERVER['HTTP_HOST'] . '/index.php';
    //header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
  }
}*/
//session_start();
//Initialise PHP session and store username

//if username exiss then asign it else return to login page

//$password = $_SERVER['PHP_AUTH_PW'];
//$smarty->assign('username',$email);


//check google client info
//$client = new Google_Client(['client_id' => '654761778841-17vv37gqrqh653eopikukm2kajgsu5k2.apps.googleusercontent.com']);  // Specify the CLIENT_ID of the app that accesses the backend
//$client->setScopes("https://www.googleapis.com/auth/userinfo.profile");

 // if($client->isAccessTokenExpired()) {
  //        $client->refreshToken('refresh-token');
   //   }
/*
$oauth2 = new \Google_Service_Oauth2($client);
$userInfo = $oauth2->userinfo->get();
print_r($userInfo);*/

//check id token from google login 
/*
$client = new Google_Client(['client_id' => '654761778841-17vv37gqrqh653eopikukm2kajgsu5k2.apps.googleusercontent.com']);  // Specify the CLIENT_ID of the app that accesses the backend
$payload = $client->verifyIdToken($id_token);
if ($payload) {
  $userid = $payload['sub'];
  // If request specified a G Suite domain:
  //$domain = $payload['hd'];
} else {
  // Invalid ID token
}*/



session_start();
//if user not already authenticated then ask for username and password

if (!isset($_SESSION['USERNAME']) && !isset($_SESSION['LOGINFLAG'])) 
{
	$_SESSION['LOGINFLAG'] = "TRUE";
    header('WWW-Authenticate: Basic realm="Campus Login"');
    header('HTTP/1.0 401 Unauthorized');
    echo "This page is unavailable without a valid login";
    exit;
} 
//Don't let user try and log on with a blank password as this can be seen as an unauthenticated connection attempt and get a positive response from LDAP server
if ($_SERVER['PHP_AUTH_PW'] == "")
{
	header('WWW-Authenticate: Basic realm="Campus Login"');
    header('HTTP/1.0 401 Unauthorized');
    echo "This page is unavailable without a valid login";
    exit;
}

//Initialise PHP session and store username



$password = $_SERVER['PHP_AUTH_PW'];
$smarty->assign('username',$_SERVER['PHP_AUTH_USER']);






$statement = $db->prepare("SELECT Username FROM Guests WHERE Username=?");
$statement->bindValue(1, $_SERVER['PHP_AUTH_USER']);	
$statement->execute();		
$row = $statement->Fetch();
/*
//if it's not a guest account then check if it's a campus login
if(is_null($row[0]))
{	
	//TO DO add ldap to PHP, for now just authenticate anyone
	
	//try to bind to ldap server, as password is not blank this will only succeed if password is correct
	$ds=ldap_connect("ldap.ncl.ac.uk");  
	$try_bind =ldap_bind($ds,$_SERVER['PHP_AUTH_USER']."@campus.ncl.ac.uk",$_SERVER['PHP_AUTH_PW'] );
	
	//$try_bind = true;
	
		
	//if it is authenticated	
	if($try_bind)
	{
	
		//now see if there's already a user account on the system

		$statement = $db->prepare("SELECT Username FROM Usernames WHERE Username=?");
		$statement->bindValue(1, isset($_SESSION['user']) ? $_SESSION['user'] : null);	
		$statement->execute();		
		$row = $statement->Fetch();		

		
		//if there isn't then create one on our system
		if(is_null($row[0]))
		{
			$statement = $db->prepare("INSERT INTO Usernames VALUES(?)");
			$statement->bindValue(1, isset($_SESSION['user']) ? $_SESSION['user'] : null);	
			$statement->execute();	
		}
		
		//didn't authenticate so either the password's wrong or this is neither a guest account nor a campus login
		else
		{
			header('WWW-Authenticate: Basic realm="Campus Login"');
			header('HTTP/1.0 401 Unauthorized');
			echo "This page is unavailable without a valid login";
			exit;
		}
	}
	else 
	{
		//Check password
		$statement = $db->prepare("SELECT Password FROM Guests WHERE Username=?");
		$statement->bindValue(1, $_SERVER['PHP_AUTH_USER']);	
		$statement->execute();		
		$row = $statement->Fetch();		
		if($_SERVER['PHP_AUTH_PW'] != $row[0])
		{
			header('WWW-Authenticate: Basic realm="Incorrect password, please try again"');
			header('HTTP/1.0 401 Unauthorized');
			echo '<h1>401 You are not authorised to view this page</h1>';
			//echo($result1);
			exit;
			
		}
	}
	$_SESSION['USERNAME']=$_SERVER['PHP_AUTH_USER'];
	if (is_admin())
	{
		$_SESSION['is_admin']=true;
	}
} */

function is_admin()
{
	global $db;

	$statement = $db->prepare("SELECT Username FROM Admins WHERE Username=?");
	$statement->bindValue(1, $_SERVER['PHP_AUTH_USER']);	
	$statement->execute();		
	$row = $statement->Fetch();
	return !(is_null($row[0]));
}

?>
