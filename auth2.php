<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

require_once ('./classroominclude.php');

if(!isset($_SESSION)){
    session_start(); 
} 

require_once ('./gpConfig.php');
require_once ('./oauth_google.php');

if (!isset($_SESSION['USERNAME']) && !isset($_SESSION['LOGINFLAG'])) 
{
    $_SESSION['LOGINFLAG'] = "TRUE";
    $authUrl = $gClient->createAuthUrl();
    echo '<META HTTP-EQUIV=REFRESH CONTENT="1; '.$authUrl.'">';
    exit;
} 


if(isset($_GET['code']))
{
	$gClient->authenticate($_GET['code']);
	$_SESSION['token'] = $gClient->getAccessToken();
	header('Location: ' . filter_var($redirectURL, FILTER_SANITIZE_URL));
}

if (isset($_SESSION['token'])) 
{
	$gClient->setAccessToken($_SESSION['token']);
}

if ($gClient->getAccessToken()) 
{
	//Get user profile data from google
	$gpUserProfile = $google_oauthV2->userinfo->get();
	
	//Initialize User class
	//createUserTable();

	$user = new User();
	
	//Insert or update user data to the database
    $gpUserData = array(
        'oauth_provider'=> 'google',
        'oauth_uid'     => $gpUserProfile['id'],
        'first_name'    => $gpUserProfile['given_name'],
        'last_name'     => $gpUserProfile['family_name'],
        'email'         => $gpUserProfile['email'],
    );

    $_SESSION['USERNAME']=$gpUserProfile['email'];
    $userData = $user->checkUser($gpUserData);
		
	//Render Google profile data
    //if(!empty($userData)){
    	
    //}
    if (isset($_SESSION['token']))
    {
        $output = '<h3 style="color:red">Some problem occurred, please try again.</h3>';
    }

}else{
    // Get login url
    $authUrl = $gClient->createAuthUrl();
    
    // Render google login button
    $output = '<a href="'.filter_var($authUrl, FILTER_SANITIZE_URL).'"><img src="img/coord1.png" alt=""/></a>';
}

/*
session_start();
//if user not already authenticated then ask for username and password

if (!isset($_SESSION['USERNAME']) && !isset($_SESSION['LOGINFLAG'])) 
{
	$_SESSION['LOGINFLAG'] = "TRUE";
    header('WWW-Authenticate: Basic realm="Campus Login"');
    header('HTTP/1.0 401 Unauthorized');
    echo "This page is unavailable without a valid login";
    exit;
} 
//Don't let user try and log on with a blank password as this can be seen as an unauthenticated connection attempt and get a positive response from LDAP server
if ($_SERVER['PHP_AUTH_PW'] == "")
{
	header('WWW-Authenticate: Basic realm="Campus Login"');
    header('HTTP/1.0 401 Unauthorized');
    echo "This page is unavailable without a valid login";
    exit;
}

//Initialise PHP session and store username



$password = $_SERVER['PHP_AUTH_PW'];
$smarty->assign('username',$_SERVER['PHP_AUTH_USER']);

//check if we have a guest username



$statement = $db->prepare("SELECT Username FROM Guests WHERE Username=?");
$statement->bindValue(1, $_SERVER['PHP_AUTH_USER']);	
$statement->execute();		
$row = $statement->Fetch();

//if it's not a guest account then check if it's a campus login
if(is_null($row[0]))
{	
	//TO DO add ldap to PHP, for now just authenticate anyone
	
	//try to bind to ldap server, as password is not blank this will only succeed if password is correct
	$ds=ldap_connect("ldap.ncl.ac.uk");  
	$try_bind =ldap_bind($ds,$_SERVER['PHP_AUTH_USER']."@campus.ncl.ac.uk",$_SERVER['PHP_AUTH_PW'] );
	
	//$try_bind = true;
	
		
	//if it is authenticated	
	if($try_bind)
	{
	
		//now see if there's already a user account on the system

		$statement = $db->prepare("SELECT Username FROM Usernames WHERE Username=?");
		$statement->bindValue(1, isset($_SESSION['user']) ? $_SESSION['user'] : null);	
		$statement->execute();		
		$row = $statement->Fetch();		

		
		//if there isn't then create one on our system
		if(is_null($row[0]))
		{
			$statement = $db->prepare("INSERT INTO Usernames VALUES(?)");
			$statement->bindValue(1, isset($_SESSION['user']) ? $_SESSION['user'] : null);	
			$statement->execute();	
		}
		
		//didn't authenticate so either the password's wrong or this is neither a guest account nor a campus login
		else
		{
			header('WWW-Authenticate: Basic realm="Campus Login"');
			header('HTTP/1.0 401 Unauthorized');
			echo "This page is unavailable without a valid login";
			exit;
		}
	}
	else 
	{
		//Check password
		$statement = $db->prepare("SELECT Password FROM Guests WHERE Username=?");
		$statement->bindValue(1, $_SERVER['PHP_AUTH_USER']);	
		$statement->execute();		
		$row = $statement->Fetch();		
		if($_SERVER['PHP_AUTH_PW'] != $row[0])
		{
			header('WWW-Authenticate: Basic realm="Incorrect password, please try again"');
			header('HTTP/1.0 401 Unauthorized');
			echo '<h1>401 You are not authorised to view this page</h1>';
			//echo($result1);
			exit;
			
		}
	}
	$_SESSION['USERNAME']=$_SERVER['PHP_AUTH_USER'];
	if (is_admin())
	{
		$_SESSION['is_admin']=true;
	}
}

function is_admin()
{
	global $db;

	$statement = $db->prepare("SELECT Username FROM Admins WHERE Username=?");
	$statement->bindValue(1, $_SERVER['PHP_AUTH_USER']);	
	$statement->execute();		
	$row = $statement->Fetch();
	return !(is_null($row[0]));
}
*/
?>
