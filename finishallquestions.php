 <?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

	require_once ('./classroominclude.php');
	require_once ('./auth2.php');
	$smarty->assign('highlighted','manageelections');
	if (isset($_SESSION['USERNAME']))
	{
		$smarty->assign('username',$_SESSION['USERNAME']);
	}
	if(isset($_SESSION['is_admin']))
	{
		$smarty->assign('admin','true');
	}	

	if ( isset($_POST['sessionid']))
	{
		$sessionID=$_POST['sessionid'];
		//check that the session belong to this user
		$statement = $db->prepare("SELECT COUNT(*) FROM Sessions WHERE sessionID= ? AND username = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $_SESSION['USERNAME']);	
		$statement->execute();	
		$row = $statement->Fetch();
		if ($row[0]==0)
		{
			$smarty->display('noauth.tpl');
			exit;
		}

		//use curl to request dofinishallquestion.php with a short time out for asynchronous ballot closing
				// post request to signature generator	
				$url = $_SERVER['REQUEST_SCHEME']."://". $_SERVER['SERVER_NAME'] . str_replace(basename($_SERVER['REQUEST_URI'], ".php"), "dofinishallquestions", $_SERVER['REQUEST_URI'])."?sessionid=".$sessionID;
                                //echo($url);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
				curl_setopt($ch, CURLOPT_URL,$url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_TIMEOUT, 1);

				$data = curl_exec($ch);
				curl_close($ch);
	//			$data = trim($data);
	//			$data = urldecode($data);
//test
	//			return $data;
	
		$smarty->display('finishedallquestions.tpl');
	}
	
			
	else
	{
		$smarty->display('finishallquestions.tpl');
	}
?>
