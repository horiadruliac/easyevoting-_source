<?php 
require_once ('./classroominclude.php');
require_once ('./gethash.php');
require_once ('./getclassroomsignature.php');
$postdata = file_get_contents("php://input");
$xmlData = simplexml_load_string($postdata);
//echo("XML DATA: ".$xmlData);
//Constants for fault tolerance testing
$GENERAL_ERROR = false;
$TYPE_THREE_ANSWERED = false;
$TYPE_FOUR_ANSWERED = false;
$TYPE_ONETWO_ANSWERED = false;
$CANCEL_ERROR = false;
$CONFIRM_ERROR = false;
	
function getTypeTag($xml)
{
	if ($xml->type)
	{
		return (string) $xml->type;
	}
	
	//return false if we get here as there was no type tag
	malformedRequest();
	exit;
}

function getSessionIDTag($xml)
{
	if ($xml->sessionid)
	{
		return (string) $xml->sessionid;
	}
	
	//return false if we get here as there was no sessionid tag
	malformedRequest();
	exit;	
}

function getPasscodeTag($xml)
{
	if ($xml->passcode)
	{
		return (string) $xml->passcode;
	}
	//return false if we get here as there was no passcode tag
	malformedRequest();
	exit;
}

function getChoiceTag($xml)
{
	if ($xml->choice)
	{
		return (string) $xml->choice;
	}
	//return false if we get here as there was no choice tag
	malformedRequest();
	exit;
}

function getUserIDTag($xml)
{
	if ($xml->userid)
	{
		return (string) $xml->userid;
	}
	
	malformedRequest();
	exit;	
}

function getQuestionTag($xml)
{
	if ($xml->questionid)
	{
		return (string) $xml->questionid;
	}
	
	malformedRequest();
	exit;	


}

//only for type 1, type 3 and type 4 questions
function getAnswerTag($xml)
{
	if ($xml->answer)
	{
		return (string) $xml->answer;

	}
	
	//return false if we get here as there was no userid tag
	malformedRequest();
	exit;


}

//only for type 2 questions
function getAnswerArray($xml)
{
	if ($xml->answerset)
	{
		$answers = "";
		$counter = 0;
		foreach ($xml->answerset->children() as $answer)
		{
			$answers[$counter] = (string) $answer;
			$counter++;
		}
		
		return $answers;

	}
	
	malformedRequest();
	exit;	


}

function ballotsRemaining ($sessionID, $questionNumber,$userID)
{
	global $db;
	$statement = $db->prepare("SELECT ballotsAllowed FROM Type12Status WHERE sessionID = ? AND questionID = ? AND userID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);	
	$statement->bindValue(3, $userID);
	$statement->execute();		
	$row = $statement->Fetch();
	if ( ((int)$row[0]) > 0)
	{
		return true;
	}	
	return false;
}


function receiveAnswer($sessionID,$userID,$questionNumber,$questionType,$xmlData)
{
	//fault tolerance testing variables
	global $TYPE_THREE_ANSWERED;
	global $TYPE_FOUR_ANSWERED;
	global $GENERAL_ERROR;
	global $TYPE_ONETWO_ANSWERED;
	
	//make sure we are using the smarty handler, not a local variable.
	global $smarty;
	//echo("hello!");
	if ($GENERAL_ERROR)
	{
		$smarty->display('generalerror.tpl');
		exit();
	}
	
	if (!sessionExists($sessionID))
	{
		//return xml for no such session
		$smarty->display('invalidsession.tpl');
	}
	//check if valid user id
	else if (!userExists($sessionID,$userID))
	{
		//return xml for no such user
		$smarty->assign('sessionID',$sessionID);
		$smarty->display('invaliduser.tpl');
	}
	//check if valid question number
	else if (!questionExists($sessionID,$questionNumber))
	{
		//return xml for no such user
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('invalidquestion.tpl');
	}
	else if ($questionType == "1")
	{
		if ($TYPE_ONETWO_ANSWERED)
		{
			$smarty->assign('sessionID',$sessionID);
			$smarty->assign('questionID',$questionNumber);
			$smarty->display('questionanswered.tpl');
			exit();
		}
		$answer = getAnswerTag($xmlData);
		receiveTypeOneAnswer($sessionID,$userID,$questionNumber,$answer);
	}
	else if ($questionType == "2")
	{
		if ($TYPE_ONETWO_ANSWERED)
		{
			$smarty->assign('sessionID',$sessionID);
			$smarty->assign('questionID',$questionNumber);
			$smarty->display('questionanswered.tpl');
			exit();
		}
		$answer = getAnswerArray($xmlData);
		receiveTypeTwoAnswer($sessionID,$userID,$questionNumber,$answer);
	}
	else if ($questionType == "3")
	{

		if ($TYPE_THREE_ANSWERED)
		{
			$smarty->assign('sessionID',$sessionID);
			$smarty->assign('questionID',$questionNumber);
			$smarty->display('questionanswered.tpl');
			exit();
		}
		else
		{
			$answer = getAnswerTag($xmlData);
			receiveTypeThreeAnswer($sessionID,$userID,$questionNumber,$answer);
		}
	}
	else if ($questionType == "4")
	{
		if ($TYPE_FOUR_ANSWERED)
		{
			$smarty->assign('sessionID',$sessionID);
			$smarty->assign('questionID',$questionNumber);
			$smarty->display('questionanswered.tpl');
			exit();
		}
		$answer = getAnswerTag($xmlData);
		receiveTypeFourAnswer($sessionID,$userID,$questionNumber,$answer);
	}
}

//check if we have run out of user ids to allocate
function usersExhausted($sessionID)
{
	global $db;
	$statement = $db->prepare("SELECT count(*) FROM Users WHERE sessionID = ? AND allocated = false");
	$statement->bindValue(1, $sessionID);
	$statement->execute();		
	$row = $statement->Fetch();
	if ($row[0] == "0")
	{
		return true;
	}
	return false;
}

function joinSession($sessionID,$passcode)
{
	//make sure we are using the smarty handler, not a local variable.
	global $smarty;
	global $db;
	if (!sessionExists($sessionID))
	{
		//return xml for no such session
		$smarty->display('invalidsession.tpl');
	}
	else if (!sessionGenerated($sessionID))
	{
		//return xml for no such session
		$smarty->display('sessiongenerating.tpl');
	}
	else if (sessionFinished($sessionID))
	{
		//return xml for no such session
		$smarty->assign('sessionID',$sessionID);
		$smarty->display('sessionfinished.tpl');
	}
	else if (usersExhausted($sessionID))
	{
		//return xml for no such session
		$smarty->assign('sessionID',$sessionID);
		$smarty->display('usersexhausted.tpl');
	}
	else if (sessionLocked($sessionID))
	{
		//return xml for no such session
		$smarty->assign('sessionID',$sessionID);
		$smarty->display('questionlocked.tpl');
	}

	else 
	{
		$publicKey = getPublicKey();
		$smarty->assign('publickey',$publicKey);
		$passcodeType = getPasscodeType($sessionID);
		if ($passcodeType == "individual")
		{
			if (!passcodeExists($sessionID,$passcode,$passcodeType))
			{
				//return xml for no such passcode
				$smarty->assign('sessionID',$sessionID);
				$smarty->display('invalidpasscode.tpl');
			}	
			else if (passcodeUsed($sessionID,$passcode))
			{
				//return xml for passcode already used
				$smarty->assign('sessionID',$sessionID);
				$smarty->display('usedpasscode.tpl');
			}
			else 
			{
				//get number of questions
				$questions = getQuestions($sessionID);
				//set userID as used
				$statement = $db->prepare("UPDATE Users SET allocated = true WHERE sessionID = ? AND userID = ?");
				$statement->bindValue(1, $sessionID);
				$statement->bindValue(2, $passcode);
				$statement->execute();		
				
				//return xml with userID
				//as this is an individual passcode we return the passcode as the user ID
				$smarty->assign('sessionID',$sessionID);
				$smarty->assign('userID',$passcode);				
				$smarty->assign('questions',$questions);
				$smarty->assign('receiptlength',getReceiptLength($sessionID));
				$smarty->assign('securitylevel',getSecurityLevel($sessionID));				
				$smarty->display('joinsession.tpl');
			}
		}
		else if ($passcodeType == "group")
		{
			if (!passcodeExists($sessionID,$passcode,$passcodeType))
			{
				//return xml for no such passcode
				$smarty->assign('sessionID',$sessionID);
				$smarty->display('invalidpasscode.tpl');
			}	
			else
			{
				//get number of questions
				$questions = getQuestions($sessionID);
				$smarty->assign('sessionID',$sessionID);
				$smarty->assign('userID',getNewUserID($sessionID,$passcodeType));				
				$smarty->assign('questions',$questions);
				$smarty->assign('receiptlength',getReceiptLength($sessionID));
				$smarty->assign('securitylevel',getSecurityLevel($sessionID));				
				$smarty->display('joinsession.tpl');
			}
		}
		else if ($passcodeType == "none")
		{
			//get number of questions
			$questions = getQuestions($sessionID);
			$smarty->assign('sessionID',$sessionID);
			$smarty->assign('userID',getNewUserID($sessionID,$passcodeType));				
			$smarty->assign('questions',$questions);	
			$smarty->assign('receiptlength',getReceiptLength($sessionID));
			$smarty->assign('securitylevel',getSecurityLevel($sessionID));
			$smarty->display('joinsession.tpl');
		}
	}

}

function sendQuestion($sessionID,$userID,$questionNumber)
{
	//make sure we are using the smarty handler, not a local variable.
	global $smarty;
	if (!sessionExists($sessionID))
	{
		//return xml for no such session
		$smarty->display('invalidsession.tpl');
	}
	else if (!userExists($sessionID,$userID))
	{
		//return xml for no such user
		$smarty->assign('sessionID',$sessionID);
		$smarty->display('invaliduser.tpl');
	}
	else if (!questionExists($sessionID,$questionNumber))
	{
		//return xml for no such user
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('invalidquestion.tpl');
	}
	else if (sessionLocked($sessionID))
	{
		//return xml for no such user
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('questionlocked.tpl');
	}
	else
	{
		$questionText = getQuestionText($sessionID,$questionNumber);
		$questionType = getQuestionType($sessionID,$questionNumber);
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->assign('questionText',$questionText);
		$smarty->assign('questionType',$questionType);
		if (questionFinished($sessionID,$questionNumber))
		{
			$smarty->assign('finished','yes');
		}
		else
		{
		
			$smarty->assign('finished','no');
		}
		if ($questionType == "1")
		{
			$answerArray = getAnswers($sessionID,$questionNumber);
			$smarty->assign('answers',$answerArray);			
			$smarty->display('questionresponse.tpl');		
		}
		else if ($questionType == "2")
		{
			$answerArray = getAnswers($sessionID,$questionNumber);
			$maxAnswers = getMaxAnswers($sessionID,$questionNumber);
			$smarty->assign('answers',$answerArray);	
			$smarty->assign('maxAnswers',$maxAnswers);			
			$smarty->display('questionresponse.tpl');			

		}
		else if ($questionType == "3" || $questionType="4")
		{			
			$smarty->display('questionresponse.tpl');	
		}
		
	}
	
}

function answerable($sessionID,$questionNumber,$userID)
{
	global $db;
	$statement = $db->prepare("SELECT status,ballotsAllowed FROM Type12Status WHERE sessionID = ? AND questionID= ? AND userID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->bindValue(3, $userID);
	$statement->execute();	

	$row = $statement->Fetch();

	//it's not been answered yet
	if ($row == false)
	{
		return true;
	}
	
	if ( $row[0] == "0" && ((int)$row[1]) > 0)
	{
		return true;
	}	
	
	return false;
	
}
function alreadyAnswered ($sessionID,$questionNumber,$userID)
{
	global $db;
	$statement = $db->prepare("SELECT COUNT(*) FROM Type3Answers WHERE sessionID = ? AND questionID= ? AND userID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->bindValue(3, $userID);
	$statement->execute();	

	$row = $statement->Fetch();
	if ($row[0] != "0")
	{
		return true;
	}
	$statement = $db->prepare("SELECT COUNT(*) FROM Type4Answers WHERE sessionID = ? AND questionID= ? AND userID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->bindValue(3, $userID);
	$statement->execute();	

	$row = $statement->Fetch();
	if ($row[0] != "0")
	{
		return true;
	}
	return false;
}


//check if this user is at stage one for this session and question
function stageOne($sessionID,$userID,$questionNumber)
{
	//return false if this question has already been answered or it is at stageTwo
	global $db;
	$statement = $db->prepare("SELECT status FROM Type12Status WHERE sessionID = ? AND questionID= ? AND userID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->bindValue(3, $userID);
	$statement->execute();
	
	$row = $statement->Fetch();
	if($row[0] == "0")
	{
		return true;
	}
	
	
	return false;

}

//check if this user is at stage two for this session and question
function stageTwo($sessionID,$userID,$questionNumber)
{
	//return false if this question has already been answered or it is at stageOne
	global $db;
        
	//echo("SELECT status FROM Type12Status WHERE sessionID = ".$sessionID." AND questionID= ".$questionNumber." AND userID = ".$userID);
        
	$statement = $db->prepare("SELECT status FROM Type12Status WHERE sessionID = ? AND questionID= ? AND userID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->bindValue(3, $userID);
	$statement->execute();
	
	$row = $statement->Fetch();

	if($row[0] == "1")
	{
		return true;
	}
	
	return false;
}

//check if this is a valid answer for this type 1 question
function validAnswer($sessionID,$questionNumber,$answer)
{	
	//return false if the answer isn't one on the list
	global $db;
	$statement = $db->prepare("SELECT count(*) FROM Type12Answers WHERE sessionID = ? AND questionID= ? AND answerNumber = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->bindValue(3, $answer);
	$statement->execute();
	
	$row = $statement->Fetch();

	if ($row[0] == 0)
	{
		return false;
	}
	
	return true;

}

//check if this is a valid set of answers for this type 2 question
function validAnswerSet($sessionID,$questionNumber,$answer)
{
	//get maxanswers then check if the number of answers given is larger than it
	global $db;
	$statement = $db->prepare("SELECT maxAnswers FROM Type12Questions WHERE sessionID = ? AND questionID= ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->execute();	

	$row=$statement->Fetch();

	if (count($answer) > $row[0])
	{
		return false;
	}
	
	//loop through list of answers and return false if any answer isn't on on the list
	foreach ($answer as $choice)
	{
		$statement = $db->prepare("SELECT count(*) FROM Type12Answers WHERE sessionID = ? AND questionID= ? AND answerNumber = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $questionNumber);
		$statement->bindValue(3, $choice);
		$statement->execute();	

		$row=$statement->Fetch();
		if ($row[0] == 0)
		{
			return false;
		}
	}
	
	return true;

}


function receiveTypeOneAnswer($sessionID,$userID,$questionNumber,$answer)
{


	//make sure we are using the smarty handler, not a local variable.
	global $smarty;
	
	//check if question locked
	if (sessionLocked($sessionID))
	{
//                echo "[[ control flow: sessionLocked ]]<br>"; die();
		//return xml for question locked
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('questionlocked.tpl');
	}
	//check if question already answered
	else if (stageTwo($sessionID,$userID,$questionNumber))
	{ 
//                echo "[[ control flow: stageTwo ]]<br>"; die();
		//return xml for no already answered
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('notstageone.tpl');
	}
	else if (questionAnswered($sessionID,$userID,$questionNumber))
	{
//                echo "[[ control flow: questionAnswered ]]<br>"; die();
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('questionanswered.tpl');
	}
	else if (questionFinished($sessionID,$questionNumber))
	{
//                echo "[[ control flow: questionFinished ]]<br>"; die();
		//return xml for already answered
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('questionfinished.tpl');
	}
	//check if valid answer (i.e. a number)
	else if (!validAnswer($sessionID,$questionNumber,$answer))
	{
//                echo "[[ control flow: !validAnswer ]]<br>"; die();
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('invalidanswer.tpl');
	}
	else if (!ballotsRemaining($sessionID,$questionNumber,$userID))
	{
//                echo "[[ control flow: !ballotsRemaining ]]<br>"; die();
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('ballotsexhausted.tpl');
	}
	else
	{

	
		//assign ballot
		//get next unallocated ballot from ballot table
		global $db;
		$statement = $db->prepare("SELECT ballotID FROM Ballots WHERE sessionID = ? AND questionID= ? AND status = 0");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $questionNumber);
		$statement->execute();	
		$row = $statement->Fetch();
		$ballotID = $row[0];
		//set that ballot as allocated at stageone
		$statement = $db->prepare("UPDATE Ballots SET status = 1 WHERE sessionID = ? AND questionID= ?  AND ballotID = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $questionNumber);
		$statement->bindValue(3, $ballotID);
		$statement->execute();	
		
	
		// store answer chosen (temporarily as not committed)
		//set answer as currentChoice in Type12Status table
		//set status in Type12Status Table
		//set  ballot in Type12Status table
		$statement = $db->prepare("UPDATE Type12Status SET currentChoice = ?, status = 1, currentBallot = ? WHERE sessionID = ? AND questionID = ? AND userID = ?");
		$statement->bindValue(1, $answer);
		$statement->bindValue(2, $ballotID);		
		$statement->bindValue(3, $sessionID);
		$statement->bindValue(4, $questionNumber);
		$statement->bindValue(5, $userID);
		$statement->execute();	

		
		//get stage one receipt for answer given
		$receipt="<ballotreceipt>";
		$receipt=$receipt."<sessionid>".$sessionID."</sessionid>";
		$receipt=$receipt."<questionid>".$questionNumber."</questionid>";
		$receipt=$receipt."<ballotid>".$ballotID."</ballotid>";
		$receipt=$receipt."<stage>1</stage>";	
		//get set of possible answers
		$statement = $db->prepare("SELECT answerNumber,publicKey,restructuredKey,yesValue,noValue FROM BallotXML WHERE sessionID = ? AND questionID = ? AND ballotID = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $questionNumber);		
		$statement->bindValue(3, $ballotID);
		$statement->execute();	
		// loop through answers
		while($row = $statement->Fetch())
		{
			$receipt=$receipt."<answer>";
			$receipt=$receipt."<answertext><![CDATA[";
			$statement2 = $db->prepare("SELECT answerText FROM Type12Answers WHERE sessionID = ? AND questionID = ? AND answernumber = ?");
			$statement2->bindValue(1, $sessionID);
			$statement2->bindValue(2, $questionNumber);		
			$statement2->bindValue(3, $row[0]);
			$statement2->execute();	
			$row2 = $statement2->Fetch();
			$receipt=$receipt.$row2[0];
			$receipt=$receipt."]]></answertext>";			
			$receipt=$receipt.$row[1];			
			$receipt=$receipt.$row[2];				
			//for each answer if this is the choice include yes XML else include no XML
			if ($row[0] == $answer)
			{
				$receipt=$receipt.$row[3];	
			}
			else
			{
				$receipt=$receipt.$row[4];	
			}
			$receipt=$receipt."</answer>";
		}
					
		// sign the receipt
		$statement = $db->prepare("SELECT value FROM Constants WHERE constant='privateKey'");
		$statement->execute();	
		$row = $statement->Fetch();
                
		$signature = getSignature($receipt."<key>".$row[0]."</key>"."</ballotreceipt>");
		$receipt = $receipt."<signature>".$signature."</signature></ballotreceipt>";
		

		// store this receipt in the receipt table
		$statement = $db->prepare("UPDATE Ballots SET stageOneReceipt = ? WHERE sessionID = ? AND questionID = ? AND ballotID = ?");
		$statement->bindValue(1, $receipt);
		$statement->bindValue(2, $sessionID);
		$statement->bindValue(3, $questionNumber);		
		$statement->bindValue(4, $ballotID);
		$statement->execute();
		

		//hash the receipt before displaying
		$temp = $receipt;
		$receipt = generateReceipt($receipt,0);
		//send xml response
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->assign('ballotID',$ballotID);
		$smarty->assign('receipt',$receipt);
		$smarty->assign('signature',$signature);
		$smarty->display('stageone.tpl');
	}
		
}


function receiveTypeTwoAnswer($sessionID,$userID,$questionNumber,$answer)
{

	//make sure we are using the smarty handler, not a local variable.
	global $smarty;
	//check if valid session id
	if (!sessionExists($sessionID))
	{
		//return xml for no such session
		$smarty->display('invalidsession.tpl');
	}
	//check if valid user id
	else if (!userExists($sessionID,$userID))
	{
		//return xml for no such user
		$smarty->assign('sessionID',$sessionID);
		$smarty->display('invaliduser.tpl');
	}
	//check if valid question number
	else if (!questionExists($sessionID,$questionNumber))
	{
		//return xml for no such user
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('invalidquestion.tpl');
	}
	//check if question locked
	else if (sessionLocked($sessionID))
	{
		//return xml for question locked
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('questionlocked.tpl');
	}
	//check if question already answered
	else if (stageTwo($sessionID,$userID,$questionNumber))
	{
		//return xml for no already answered
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('notstageone.tpl');
	}
	else if (questionAnswered($sessionID,$userID,$questionNumber))
	{
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('questionanswered.tpl');
	}
	else if (questionFinished($sessionID,$questionNumber))
	{
		//return xml for already answered
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('questionfinished.tpl');
	}
	//check if valid answer (i.e. a number)
	else if (!validAnswerSet($sessionID,$questionNumber,$answer))
	{
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('invalidanswer.tpl');
	}
	else if (!ballotsRemaining($sessionID,$questionNumber,$userID))
	{
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('ballotsexhausted.tpl');
	}
	else
	{	global $db;
		//get next unallocated ballot from ballot table
		$statement = $db->prepare("SELECT ballotID FROM Ballots WHERE sessionID = ? AND questionID = ? AND status =0");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $questionNumber);		
		$statement->execute();
		$row = $statement->Fetch();
		$ballotID = $row[0];
		//set that ballot as allocated at stageone
		$statement = $db->prepare("UPDATE Ballots SET status = 1 WHERE sessionID = ? AND questionID = ? AND ballotID = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $questionNumber);
		$statement->bindValue(3, $ballotID);		
		$statement->execute();
		
		//build string from answers chosen
		$tempAnswer = $answer[0];
		$counter =1;
		while ($counter<count($answer))
		{
			$tempAnswer = $tempAnswer.",".$answer[$counter];
			$counter++;
		}
		
		//set answer as currentChoice in Type12Status table
		//set status in Type12Status Table
		//set  ballot in Type12Status table
		$statement = $db->prepare("UPDATE Type12Status SET currentChoice = ?, status = 1, currentBallot = ? WHERE sessionID = ? AND questionID = ? AND userID = ?");
		$statement->bindValue(1, $tempAnswer);
		$statement->bindValue(2, $ballotID);		
		$statement->bindValue(3, $sessionID);
		$statement->bindValue(4, $questionNumber);
		$statement->bindValue(5, $userID);		
		$statement->execute();
		
		
		//get stage one receipt for answer given
		
		$receipt="<ballotreceipt>";
		$receipt=$receipt."<sessionid>".$sessionID."</sessionid>";
		$receipt=$receipt."<questionid>".$questionNumber."</questionid>";
		$receipt=$receipt."<stage>1</stage>";
		$receipt=$receipt."<ballotid>".$ballotID."</ballotid>";		
		//get set of possible answers
		$statement = $db->prepare("SELECT answerNumber,publicKey,restructuredKey,yesValue,noValue FROM BallotXML WHERE sessionID = ? AND questionID = ? AND ballotID = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $questionNumber);
		$statement->bindValue(3, $ballotID);		
		$statement->execute();
		// loop through answers
		while($row = $statement->Fetch())
		{
			$receipt=$receipt."<answer>";
			$receipt=$receipt."<answertext><![CDATA[";
			$statement2 = $db->prepare("SELECT answerText FROM Type12Answers WHERE sessionID = ? AND questionID = ? AND answerNumber = ?");
			$statement2->bindValue(1, $sessionID);
			$statement2->bindValue(2, $questionNumber);
			$statement2->bindValue(3, $row[0]);		
			$statement2->execute();

			$row2 = $statement2->Fetch();
			$receipt=$receipt.$row2[0];
			$receipt=$receipt."]]></answertext>";			
			$receipt=$receipt.$row[1];			
			$receipt=$receipt.$row[2];				
			//for each answer loop through choices and if this is a choice include yes XML else include no XML
			$chosen = false;
			for ($i=0;$i<count($answer);$i++)
			{
				if ($row[0] == $answer[$i])
				{
					$chosen = true;
				}
			
			}
			
			if ($chosen)
			{
				$receipt=$receipt.$row[3];	
			}
			else
			{
				$receipt=$receipt.$row[4];	
			}
			$receipt=$receipt."</answer>";
		}
		
		// sign the receipt
		$statement = $db->prepare("SELECT value FROM Constants WHERE constant='privateKey'");
		$statement->execute();	
		$row = $statement->Fetch();

																
		$signature = getSignature($receipt."<key>".$row[0]."</key>"."</ballotreceipt>");
		$receipt = $receipt."<signature>".$signature."</signature></ballotreceipt>";
		
		


		//store this receipt in the receipt table
		$statement = $db->prepare("UPDATE Ballots SET stageOneReceipt = ? WHERE sessionID = ? AND questionID = ? AND ballotID = ?");
		$statement->bindValue(1, $receipt);
		$statement->bindValue(2, $sessionID);
		$statement->bindValue(3, $questionNumber);		
		$statement->bindValue(4, $ballotID);
		$statement->execute();
		
		// hash the receipt before displaying
		$receipt = generateReceipt($receipt,0);
		//send xml response
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->assign('ballotID',$ballotID);
		$smarty->assign('signature',$signature);
		$smarty->assign('receipt',$receipt);
		$smarty->display('stageone.tpl');
	}


}



function receiveTypeThreeAnswer($sessionID,$userID,$questionNumber,$answer)
{

	//make sure we are using the smarty handler, not a local variable.
	global $smarty;
	global $db;
		//check if valid session id
	if (!sessionExists($sessionID))
	{
		//return xml for no such session
		$smarty->display('invalidsession.tpl');
	}
	//check if valid user id
	else if (!userExists($sessionID,$userID))
	{
		//return xml for no such user
		$smarty->assign('sessionID',$sessionID);
		$smarty->display('invaliduser.tpl');
	}
	//check if valid question number
	else if (!questionExists($sessionID,$questionNumber))
	{
		//return xml for no such user
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('invalidquestion.tpl');
	}
	//check if question locked
	else if (sessionLocked($sessionID))
	{
		//return xml for question locked
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('questionlocked.tpl');
	}
	//check if question already answered
	else if (questionAnswered($sessionID,$userID,$questionNumber))
	{
		//return xml for already answered
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('questionanswered.tpl');
	}
	else if (questionFinished($sessionID,$questionNumber))
	{
		//return xml for already answered
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('questionfinished.tpl');
	}
	//check if valid answer (i.e. a number)
	else if (!is_numeric($answer))
	{
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('invalidanswer.tpl');
	}
	
	else
	{
		//store answer
		$statement = $db->prepare("INSERT INTO Type3Answers VALUES (?,?,?,?)");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $questionNumber);
		$statement->bindValue(3, $userID);		
		$statement->bindValue(4, $answer);
		$statement->execute();
	
		//send xml response
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('answer.tpl');
	}


}

function receiveTypeFourAnswer($sessionID,$userID,$questionNumber,$answer)
{

	//make sure we are using the smarty handler, not a local variable.
	global $smarty;
	global $db;
		//check if valid session id
	if (!sessionExists($sessionID))
	{
		//return xml for no such session
		$smarty->display('invalidsession.tpl');
	}
	//check if valid user id
	else if (!userExists($sessionID,$userID))
	{
		//return xml for no such user
		$smarty->assign('sessionID',$sessionID);
		$smarty->display('invaliduser.tpl');
	}
	//check if valid question number
	else if (!questionExists($sessionID,$questionNumber))
	{
		//return xml for no such user
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('invalidquestion.tpl');
	}
	//check if question locked
	else if (sessionLocked($sessionID))
	{
		//return xml for question locked
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('questionlocked.tpl');
	}
	//check if question already answered
	else if (questionAnswered($sessionID,$userID,$questionNumber))
	{
		//return xml for already answered
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('questionanswered.tpl');
	}
	else if (questionFinished($sessionID,$questionNumber))
	{
		//return xml for already answered
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('questionfinished.tpl');
	}
	else
	{
		//store answer
		$statement = $db->prepare("INSERT INTO Type4Answers VALUES (?,?,?,?)");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $questionNumber);
		$statement->bindValue(3, $userID);		
		$statement->bindValue(4, $answer);
		$statement->execute();	
		//send xml response
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('answer.tpl');
	}


}


function sendResults($sessionID,$questionNumber)
{
//make sure we are using the smarty handler, not a local variable.
	global $smarty;
	if (!sessionExists($sessionID))
	{
		//return xml for no such session
		$smarty->display('invalidsession.tpl');
	}
	else if (!questionExists($sessionID,$questionNumber))
	{
		//return xml for no such user
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('invalidquestion.tpl');
	}
	else if (!questionFinished($sessionID,$questionNumber))
	{
		//return xml for no question not finished
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->assign('questiontext',getQuestionText($sessionID,$questionNumber));
		$smarty->display('questionnotavailable.tpl');
	}
	else
	{
		$questionType = getQuestionType($sessionID,$questionNumber);
		if ($questionType == "3")
		{
			$results = getType3Results($sessionID,$questionNumber);
			$smarty->assign('sessionID',$sessionID);
			$smarty->assign('questionID',$questionNumber);
			$smarty->assign('questiontext',getQuestionText($sessionID,$questionNumber));
			$smarty->assign('answers',$results);
			$smarty->display('type3results.tpl');
		}
		else if ($questionType == "4")
		{
			$results = getType4Results($sessionID,$questionNumber);
			$smarty->assign('sessionID',$sessionID);
			$smarty->assign('questionID',$questionNumber);
			$smarty->assign('questiontext',getQuestionText($sessionID,$questionNumber));
			$smarty->assign('answers',$results);
			$smarty->display('type4results.tpl');
		
		}
		else
		{
			$results = getType12Results($sessionID,$questionNumber);
			$smarty->assign('sessionID',$sessionID);
			$smarty->assign('questionID',$questionNumber);
			$smarty->assign('answers',$results);
			$smarty->assign('questiontext',getQuestionText($sessionID,$questionNumber));
			$smarty->assign('questionType',$questionType);
			$smarty->display('type12results.tpl');
		
		}
	
	}


}

function getType12Results($sessionID,$questionNumber)
{
	global $db;
	$statement = $db->prepare("SELECT answerText,votesCast FROM Type12Answers WHERE sessionID = ? AND questionID = ? ORDER BY answerNumber");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->execute();
	$results = "";
	$counter = 0;
	while ($row = $statement->Fetch())
	{
		$results[$counter]['answer'] =$row[0];
		$results[$counter]['votes'] =$row[1];		
		$counter++;
	}
	return ($results);

}


function getType3Results($sessionID,$questionNumber)
{
	global $db;
	$statement = $db->prepare("SELECT answer FROM Type3Answers WHERE sessionID = ? AND questionID = ? ORDER BY answer");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->execute();
	$results = "";
	$counter = 0;
	while ($row = $statement->Fetch())
	{
		$results[$counter] =$row[0];
		$counter++;
	}
	return ($results);

}

function getType4Results($sessionID,$questionNumber)
{
	global $db;
	$statement = $db->prepare("SELECT answer FROM Type4Answers WHERE sessionID = ? AND questionID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->execute();
	$results = "";
	$counter = 0;
	while ($row = $statement->Fetch())
	{
		$results[$counter] =($counter+1).". ".$row[0];
		$counter++;
	}
	return ($results);

}



function userExists($sessionID,$userID)
{
	//check in database if this user exists for this session
	//make sure we've already set this userID to used to prevent guessing and re-using
	global $db;
	$statement = $db->prepare("SELECT * FROM Users WHERE sessionID = ? AND userID = ? AND allocated=true");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $userID);
	$statement->execute();
	$row = $statement->Fetch();
	if (!$row)
	{
		return false;
	}
	return true;
}

function questionExists($sessionID,$questionNumber)
{
	//check in database if this question exists for this session
	//first check type 3 questions
	global $db;
	$statement = $db->prepare("SELECT * FROM Type3Questions WHERE sessionID = ? AND questionID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->execute();
	$row = $statement->Fetch();
	if ($row)
	{
		return true;
	}
	
	//check type 4 questions
	$statement = $db->prepare("SELECT * FROM Type4Questions WHERE sessionID = ? AND questionID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->execute();
	$row = $statement->Fetch();
	if ($row)
	{
		return true;
	}
	
	//check type1/2 table
	$statement = $db->prepare("SELECT * FROM Type12Questions WHERE sessionID = ? AND questionID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->execute();
	$row = $statement->Fetch();
	if ($row)
	{
		return true;
	}
	
	
	
	return false;

}

function sessionLocked($sessionID)
{
	//check in database if this question is locked and return false is it is
	global $db;
	$statement = $db->prepare("SELECT locked FROM Sessions WHERE sessionID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->execute();
	$row = $statement->Fetch();
	if ($row)
	{
		return $row[0];
	}
	
	return true;
}

function questionFinished($sessionID,$questionNumber)
{

	
	//first check type 3 table
	global $db;
	$statement = $db->prepare("SELECT finished FROM Type3Questions WHERE sessionID = ? AND questionID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->execute();
	$row = $statement->Fetch();
	if ($row)
	{
		return $row[0];
	}

	//first check type 4 table
	$statement = $db->prepare("SELECT finished FROM Type4Questions WHERE sessionID = ? AND questionID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->execute();
	$row = $statement->Fetch();
	if ($row)
	{
		return $row[0];
	}
	
	//check type 1/2 table
	$statement = $db->prepare("SELECT finished FROM Type12Questions WHERE sessionID = ? AND questionID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->execute();
	$row = $statement->Fetch();
	if ($row)
	{
		return $row[0];
	}

	
	//if the question doesn't exist then return  it as finished,
	//this case should never occur, but if it does due to error then this is the more logical response
	return true;

}

function sessionFinished($sessionID)
{

	
	//first check type 3 table
	global $db;
	$statement = $db->prepare("SELECT count(*) FROM Type3Questions WHERE sessionID = ? AND finished = false");
	$statement->bindValue(1, $sessionID);
	$statement->execute();
	$row = $statement->Fetch();
	if ($row[0] != 0)
	{
		return false;
	}

	//first check type 4 table
	$statement = $db->prepare("SELECT count(*) FROM Type4Questions WHERE sessionID = ? AND finished = false");
	$statement->bindValue(1, $sessionID);
	$statement->execute();
	$row = $statement->Fetch();
	if ($row[0] != 0)
	{
		return false;
	}
	
	//check type 1/2 table
	$statement = $db->prepare("SELECT count(*) FROM Type12Questions WHERE sessionID = ? AND finished = false");
	$statement->bindValue(1, $sessionID);
	$statement->execute();
	$row = $statement->Fetch();
	if ($row[0] != 0)
	{
		return false;
	}

	
	//if no unfinished question has been found
	return true;

}


function questionAnswered($sessionID,$userID,$questionNumber)
{
	//check in database if this question has already been answered by this user
	global $db;
	$statement = $db->prepare("SELECT * FROM Type3Answers WHERE sessionID = ? AND questionID = ? AND userID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->bindValue(3, $userID);
	$statement->execute();
	$row = $statement->Fetch();
	if (!$row)
	{
		$statement = $db->prepare("SELECT * FROM Type4Answers WHERE sessionID = ? AND questionID = ? AND userID = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $questionNumber);
		$statement->bindValue(3, $userID);
		$statement->execute();
		$row = $statement->Fetch();
		if (!$row)
		{
			$statement = $db->prepare("SELECT * FROM Type12Status WHERE sessionID = ? AND questionID = ? AND userID = ? AND status = 2");
			$statement->bindValue(1, $sessionID);
			$statement->bindValue(2, $questionNumber);
			$statement->bindValue(3, $userID);
			$statement->execute();
			$row = $statement->Fetch();
			if (!$row)
			{
				return false;
			}
		}
	
	}
		
	return true;
}


function getQuestionText($sessionID,$questionNumber)
{
	//get the question text for this sessionID, questionNumber pair from the database

	//first check if it's in the type 3 questions table
	global $db;
	$statement = $db->prepare("SELECT questionText FROM Type3Questions WHERE sessionID = ? AND questionID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->execute();
	$row = $statement->Fetch();
	if ($row)
	{
		return $row[0];
	}

	//check if it's in the type 4 questions table
	$statement = $db->prepare("SELECT questionText FROM Type4Questions WHERE sessionID = ? AND questionID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->execute();
	$row = $statement->Fetch();
	if ($row)
	{
		return $row[0];
	}
	
	//check if it's in the type 1 or 2 questions table
	$statement = $db->prepare("SELECT questionText FROM Type12Questions WHERE sessionID = ? AND questionID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->execute();
	$row = $statement->Fetch();
	if ($row)
	{
		return $row[0];
	}


	
	//return false if the question doesn't exist
	return false;

}


function getQuestionType($sessionID,$questionNumber)
{
	//get the question type for this sessionID, questionNumber pair from the database
	
	//first check if it's in the type 3 table
	global $db;
	$statement = $db->prepare("SELECT * FROM Type3Questions WHERE sessionID = ? AND questionID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->execute();
	$row = $statement->Fetch();
	if ($row)
	{
		return "3";
	}
	
	//next check if it's in the type 4 table
	$statement = $db->prepare("SELECT * FROM Type4Questions WHERE sessionID = ? AND questionID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->execute();
	$row = $statement->Fetch();
	if ($row)
	{
		return "4";
	}
	
	
	//check other table to see if it's type 1 or type 2
	$statement = $db->prepare("SELECT maxAnswers FROM Type12Questions WHERE sessionID = ? AND questionID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionNumber);
	$statement->execute();
	$row = $statement->Fetch();
	if ($row)
	{
		if ($row[0]=="1")
		{
			return "1";
		}
		else
		{
			return "2";
		}
	}
	
	//question doesn't exist	
	return false;
	
}


function getQuestions($sessionID)
{

	//get the number of questions from the database
	global $db;
	$statement = $db->prepare("SELECT COUNT(*) FROM Type3Questions WHERE sessionID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->execute();
	$row = $statement->Fetch();
	
	$questions = (int) $row[0];
	
	//add in type 4 questions
	$statement = $db->prepare("SELECT COUNT(*) FROM Type4Questions WHERE sessionID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->execute();
	$row = $statement->Fetch();
	$questions = $questions+ (int) $row[0];
	
	//add in type 1/2 questions
	$statement = $db->prepare("SELECT COUNT(*) FROM Type12Questions WHERE sessionID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->execute();
	$row = $statement->Fetch();
	$questions = $questions+ (int) $row[0];

	return $questions;

}

function getReceiptLength($sessionID)
{
	global $db;
	$statement = $db->prepare("SELECT receiptLength FROM Sessions WHERE sessionID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->execute();
	$row = $statement->Fetch();
	
	return $row[0];

}

function getSecurityLevel($sessionID)
{
	global $db;
	$statement = $db->prepare("SELECT securityLevel FROM Sessions WHERE sessionID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->execute();
	$row = $statement->Fetch();
	
	return $row[0];
}

function getAnswers($sessionID,$questionID)
{
	//get all possible answers for this session id, question id and return as an array with number, text fields
	global $db;
	$statement = $db->prepare("SELECT answerNumber,answerText FROM Type12Answers WHERE sessionID = ? AND questionID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionID);
	$statement->execute();
	$answers = null;
	$counter = 0;
	while ($row = $statement->Fetch())
	{
		$answers[$counter]['number'] = $row[0];
		$answers[$counter]['text'] = utf8_encode($row[1]);
		$counter ++;
	}
	
	
	return $answers;
}

function getMaxAnswers($sessionID,$questionID)
{
	//get the maximum number of answers for this session id, question id 
	global $db;
	$statement = $db->prepare("SELECT maxAnswers FROM Type12Questions WHERE sessionID = ? AND questionID = ?");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $questionID);
	$statement->execute();
	if ($row = $statement->Fetch())
	{
		return $row[0];
	}
	else
	{
		return 1;
	}
}

function passcodeExists($sessionID,$passcode,$passcodeType)
{
	global $db;
	if ($passcodeType == "individual")
	{
		//query the passcode table in the database for a value with this sessionID, passcode pair

		$statement = $db->prepare("SELECT * FROM Users WHERE sessionID = ? AND userID = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $passcode);
		$statement->execute();
		$row = $statement->Fetch();
		if (!$row)
		{
			return false;
		}
		return true;
		
	}
	else if ($passcodeType == "group");
	{
		//query the sesion table for this sessionid and check if passcode matches
		$statement = $db->prepare("SELECT * FROM Sessions WHERE sessionID = ? AND passcode = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $passcode);
		$statement->execute();
		$row = $statement->Fetch();
		if (!$row)
		{
			return false;
		}
		return true;
	}
}


function getPublicKey()
{
	global $db;
	$statement = $db->prepare("SELECT value FROM Constants WHERE constant='privateKey'");
	$statement->execute();	
	$row = $statement->Fetch();
	return $row[0];

}

function passcodeUsed($sessionID,$passcode)
{
	//query the user table in the database for a value with this sessionID, passcode pair
	//return true if that table has the passcode set as already used
	global $db;
	$statement = $db->prepare("SELECT * FROM Users WHERE sessionID = ? AND userID = ? AND allocated=false");
	$statement->bindValue(1, $sessionID);
	$statement->bindValue(2, $passcode);
	$statement->execute();
	$row = $statement->Fetch();
	if ($row)
	{
		return false;
	}
	return true;

}



function getPasscodeType($sessionID)
{
	//query database for this session ID and return the passcode type
	global $db;
	$statement = $db->prepare("SELECT passcodetype FROM Sessions WHERE sessionID = ?");
	$statement->bindValue(1, $sessionID);

	$statement->execute();
	$row = $statement->Fetch();
	if (!$row)
	{
		return false;
	}
	return $row[0];

}


function sessionExists($sessionID)
{
	//query database for this session ID and return true if it exists
	if (!is_numeric($sessionID))
	{
		return false;
	}
	global $db;
	$statement = $db->prepare("SELECT * FROM Sessions WHERE sessionID = ? AND hidden = false");
	$statement->bindValue(1, $sessionID);

	$statement->execute();
	$row = $statement->Fetch();

	if (!$row)
	{
		return false;
	}
	return true;
	
}

function sessionGenerated($sessionID)
{
	//query database for this session ID and return true if it exists
	global $db;
	$statement = $db->prepare("SELECT * FROM Sessions WHERE sessionID = ? AND created = true");
	$statement->bindValue(1, $sessionID);

	$statement->execute();
	$row = $statement->Fetch();

	if (!$row)
	{
		return false;
	}
	return true;
	
}

//only used for no passcode and group passcode sessions
function getNewUserID($sessionID,$passcodeType)
{
	//get an unused userID from the table
	global $db;
	$statement = $db->prepare("SELECT userID FROM Users WHERE sessionID = ? AND allocated=false");
	$statement->bindValue(1, $sessionID);

	$statement->execute();
	$row = $statement->Fetch();
	if (!$row)
	{
		return false;
	}
	else
	{
		$statement = $db->prepare("UPDATE Users SET allocated = true WHERE sessionID = ? AND userID = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $row[0]);
		$statement->execute();
		return $row[0];
	}

}

//18 SQL statements
function receiveStageTwo($sessionID,$userID,$questionNumber,$questionType,$choice)
{
	global $CANCEL_ERROR;	
	global $CONFIRM_ERROR;	
	//make sure we are using the smarty handler, not a local variable.
	global $smarty;
	global $db;
		//check if valid session id
	if (!sessionExists($sessionID))
	{
		//return xml for no such session
		$smarty->display('invalidsession.tpl');
	}
	//check if valid user id
	else if (!userExists($sessionID,$userID))
	{
		//return xml for no such user
		$smarty->assign('sessionID',$sessionID);
		$smarty->display('invaliduser.tpl');
	}
	//check if valid question number
	else if (!questionExists($sessionID,$questionNumber))
	{
		//return xml for no such user
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('invalidquestion.tpl');
	}
	//check if question locked
	else if (sessionLocked($sessionID))
	{
		//return xml for question locked
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('questionlocked.tpl');
	}
	//check if question already answered
	else if (stageOne($sessionID,$userID,$questionNumber))
	{
		//return xml for no already answered
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('notstagetwo.tpl');
	}
	else if (questionAnswered($sessionID,$userID,$questionNumber))
	{
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('questionanswered.tpl');
	}
	else if (questionFinished($sessionID,$questionNumber))
	{
		//return xml for already answered
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionNumber);
		$smarty->display('questionanswered.tpl');
	}
	else if ($choice == "cancel")
	{
		if ($CANCEL_ERROR == true)
		{
			exit;
		}
	
	
		//get ballot number
		$statement = $db->prepare("SELECT currentBallot FROM Type12Status WHERE sessionID = ? AND questionID = ? AND userID = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $questionNumber);
		$statement->bindValue(3, $userID);
		$statement->execute();
		$row = $statement->Fetch();
		$questionType = "12";
		
		$ballotID = $row[0];
	
		//generate stage two xml
		$receipt="<ballotreceipt>";
		$receipt=$receipt."<sessionid>".$sessionID."</sessionid>";
		$receipt=$receipt."<questionid>".$questionNumber."</questionid>";
		$receipt=$receipt."<ballotid>".$ballotID."</ballotid>";
		$receipt=$receipt."<stage>2</stage>";	
		$receipt=$receipt."<type>audit</type>";
		$statement = $db->prepare("SELECT currentChoice FROM Type12Status WHERE sessionID = ? AND questionID = ? AND userID = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $questionNumber);
		$statement->bindValue(3, $userID);
		$statement->execute();
		$row = $statement->Fetch();	
		$answer = preg_split("%,%",$row[0]);
		
		//get actual answers that were chosen
		$cancelString ="";

		for ($i=0;$i<count($answer)-1;$i++)
		{
			$statement = $db->prepare("SELECT answerText FROM Type12Answers WHERE sessionID = ? AND questionID = ? AND answerNumber = ?");
			$statement->bindValue(1, $sessionID);
			$statement->bindValue(2, $questionNumber);
			$statement->bindValue(3, $answer[$i]);
			$statement->execute();
			$row = $statement->Fetch();
			$cancelString = $cancelString.$row[0].",";
		}
		
		if (count($answer) >0)
		{
			$statement = $db->prepare("SELECT answerText FROM Type12Answers WHERE sessionID = ? AND questionID = ? AND answerNumber = ?");
			$statement->bindValue(1, $sessionID);
			$statement->bindValue(2, $questionNumber);
			$statement->bindValue(3, $answer[(count($answer)-1)]);
			$statement->execute();
			$row = $statement->Fetch();
			$cancelString = $cancelString.$row[0];
		}

		
		//loop through all possible answers and add the opposite XML to what was given at stage one
		$statement = $db->prepare("SELECT answerNumber,publicKey,restructuredKey,yesValue,noValue FROM BallotXML WHERE sessionID = ? AND questionID = ? AND ballotID = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $questionNumber);
		$statement->bindValue(3, $ballotID);
		$statement->execute();
		// loop through answers
		while($row = $statement->Fetch())
		{
			$receipt=$receipt."<answer>";
			$receipt=$receipt."<answertext><![CDATA[";
			$statement2 = $db->prepare("SELECT answerText FROM Type12Answers WHERE sessionID = ? AND questionID = ? AND answerNumber = ?");
			$statement2->bindValue(1, $sessionID);
			$statement2->bindValue(2, $questionNumber);
			$statement2->bindValue(3, $row[0]);
			$statement2->execute();

			$row2 = $statement2->Fetch();
			$receipt=$receipt.$row2[0];
			

			$receipt=$receipt."]]></answertext>";			
			$receipt=$receipt.$row[1];			
			$receipt=$receipt.$row[2];				
			//for each answer loop through choices and if this is a choice include yes XML else include no XML
			
			$chosen = false;
			for ($i=0;$i<count($answer);$i++)
			{
				if ($row[0] == $answer[$i])
				{
					$chosen = true;
				}
			
			}
			//get the opposite value to the one for stage one
			if ($chosen)
			{
				$receipt=$receipt.$row[4];	
			}
			else
			{
				//change this so that it says yesvalue
				$receiptValue=str_replace("<value>","<yesvalue>",$row[3]);
				$receiptValue=str_replace("<proof>","<yesproof>",$receiptValue);
				$receiptValue = str_replace("</value>","</yesvalue>",$receiptValue);
				$receiptValue = str_replace("</proof>","</yesproof>",$receiptValue);				
				$receipt=$receipt.$receiptValue;	
			}
			$receipt=$receipt."</answer>";
		}
		
		
		
		//add in the stage one signature
		//get stage one receipt
		$statement = $db->prepare("SELECT stageOneReceipt FROM Ballots WHERE sessionID = ? AND questionID = ? AND ballotID = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $questionNumber);
		$statement->bindValue(3, $ballotID);
		$statement->execute();
		$row = $statement->Fetch();
		//cut up to get signature
		$stageOneSignature = preg_split("%<signature>%",$row[0]);
		$stageOneSignature = $stageOneSignature[1];
		$stageOneSignature = preg_split("%</signature>%",$stageOneSignature);
		$stageOneSignature = $stageOneSignature[0];
		$receipt = $receipt."<stageonesignature>".$stageOneSignature."</stageonesignature>";
		$smarty->assign('stageone',generateReceipt($row[0],0));
		// sign the receipt
		$statement = $db->prepare("SELECT value FROM Constants WHERE constant='privateKey'");
		$statement->execute();	
		$row = $statement->Fetch();		

																
		$signature = getSignature($receipt."<key>".$row[0]."</key>"."</ballotreceipt>");
		
		$receipt = $receipt."<signature>".$signature."</signature></ballotreceipt>";
		
		
		//clear temporary choices and decrement the amount of ballots allowed
		$statement = $db->prepare("SELECT ballotsAllowed,currentChoice FROM Type12Status WHERE sessionID = ? AND questionID = ? AND userID = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $questionNumber);
		$statement->bindValue(3, $userID);
		$statement->execute();
		$row = $statement->Fetch();
		$ballotsAllowed = ((int) $row[0]) - 1;
		$cancelChoice = $row[1];
		$statement = $db->prepare("UPDATE Type12Status SET currentBallot = null, currentChoice=null, ballotsAllowed = ?, status = 0 WHERE sessionID = ? AND questionID = ? AND userID = ?");
		$statement->bindValue(1, $ballotsAllowed);
		$statement->bindValue(2, $sessionID);
		$statement->bindValue(3, $questionNumber);
		$statement->bindValue(4, $userID);
		$statement->execute();
		
		//store receipt in ballots table
		$statement = $db->prepare("UPDATE Ballots SET stageTwoReceipt = ?, status = 3, cancelledFor = ? WHERE sessionID = ? AND questionID = ? AND ballotID = ?");
		$statement->bindValue(1, $receipt);
		$statement->bindValue(2, $cancelChoice);
		$statement->bindValue(3, $sessionID);
		$statement->bindValue(4, $questionNumber);
		$statement->bindValue(5, $ballotID);
		$statement->execute();
		
		//hash the receipt before displaying
		$receipt = generateReceipt($receipt,0);
		
		//return xml	
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('userID',$userID);				
		$smarty->assign('questionID',$questionNumber);
		$smarty->assign('receipt',$receipt);		
		$smarty->assign('signature',$signature);		
		$smarty->assign('ballotID',$ballotID);	
		$smarty->assign('ballotsAllowed',$ballotsAllowed);	
		$smarty->assign('cancelstring',$cancelString);
		$smarty->display('classroomaudit.tpl');
	}
	else if ($choice == "confirm")
	{
		if ($CONFIRM_ERROR == true)
		{
			exit;
		}
		//get ballot number
		$statement = $db->prepare("SELECT currentBallot FROM Type12Status WHERE sessionID = ? AND questionID = ? AND userID = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $questionNumber);
		$statement->bindValue(3, $userID);
		$statement->execute();
		$row = $statement->Fetch();
		
		$questionType = "12";
		
		$ballotID = $row[0];
	
	
		//generate and store stage two xml
				
		$receipt="<ballotreceipt>";
		$receipt=$receipt."<sessionid>".$sessionID."</sessionid>";
		$receipt=$receipt."<questionid>".$questionNumber."</questionid>";
		$receipt=$receipt."<ballotid>".$ballotID."</ballotid>";
		$receipt=$receipt."<stage>2</stage>";	
		$receipt=$receipt."<type>confirm</type>";			
		
		$statement2 = $db->prepare("SELECT answerText FROM Type12Answers WHERE sessionID = ? AND questionID = ?");
		$statement2->bindValue(1, $sessionID);
		$statement2->bindValue(2, $questionNumber);
		$statement2->execute();


		while ($row2 = $statement2->Fetch())
		{
			$receipt=$receipt."<answer>";
			$receipt=$receipt."<answertext><![CDATA[";
			$receipt=$receipt.$row2[0];
			$receipt=$receipt."]]></answertext>";	
			$receipt=$receipt."</answer>";
		}
		
		
		//add in the stage one signature
		//get stage one receipt
		$statement = $db->prepare("SELECT stageOneReceipt FROM Ballots WHERE sessionID = ? AND questionID = ? AND ballotID = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $questionNumber);
		$statement->bindValue(3, $ballotID);
		$statement->execute();
		$row = $statement->Fetch();

		//cut up to get signature
		$stageOneSignature = preg_split("%<signature>%",$row[0]);
		$stageOneSignature = $stageOneSignature[1];
		$stageOneSignature = preg_split("%</signature>%",$stageOneSignature);
		$stageOneSignature = $stageOneSignature[0];
		$receipt = $receipt."<stageonesignature>".$stageOneSignature."</stageonesignature>";
		$smarty->assign('stageone',generateReceipt($row[0],0));
		//sign the receipt
		$statement = $db->prepare("SELECT value FROM Constants WHERE constant='privateKey'");
		$statement->execute();	
		$row = $statement->Fetch();		
	
		$signature = getSignature($receipt."<key>".$row[0]."</key>"."</ballotreceipt>");
		
		$receipt= $receipt."<signature>".$signature."</signature></ballotreceipt>";
		
		

		
		//increment vote counters
		$statement = $db->prepare("SELECT currentChoice FROM Type12Status WHERE sessionID = ? AND questionID = ? AND userID = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $questionNumber);
		$statement->bindValue(3, $userID);
		$statement->execute();
		$row = $statement->Fetch();

		$currentChoice = $row[0];
		$choiceArray = preg_split("%,%",$currentChoice);
		for ($i =0; $i<count($choiceArray);$i++)
		{
			//increment the current choice

			$statement = $db->prepare("SELECT votesCast FROM Type12Answers WHERE sessionID = ? AND questionID = ? AND answerNumber = ?");
			$statement->bindValue(1, $sessionID);
			$statement->bindValue(2, $questionNumber);
			$statement->bindValue(3, $choiceArray[$i]);
			$statement->execute();
			$row = $statement->Fetch();

			$temp = ((int)$row[0]) + 1;

			$statement = $db->prepare("UPDATE Type12Answers SET votesCast = ? WHERE sessionID = ? AND questionID = ? AND answerNumber = ?");
			$statement->bindValue(1, $temp);			
			$statement->bindValue(2, $sessionID);
			$statement->bindValue(3, $questionNumber);
			$statement->bindValue(4, $choiceArray[$i]);
			$statement->execute();
			
		}	
		
		//clear temporary choices
		//set question to answered
		$statement = $db->prepare("UPDATE Type12Status SET currentBallot = null, currentChoice=null, status = 2 WHERE sessionID = ? AND questionID = ? AND userID = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $questionNumber);
		$statement->bindValue(3, $userID);
		$statement->execute();
	
		//store receipt in ballots table
		$statement = $db->prepare("UPDATE Ballots SET stageTwoReceipt = ?, status = 2 WHERE sessionID = ? AND questionID = ? AND ballotID = ?");	
		$statement->bindValue(1, $receipt);		
		$statement->bindValue(2, $sessionID);

		$statement->bindValue(3, $questionNumber);
		$statement->bindValue(4, $ballotID);
		$statement->execute();		
		//hash the receipt before displaying
		$receipt = generateReceipt($receipt,0);
		
		//return xml
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('userID',$userID);				
		$smarty->assign('questionID',$questionNumber);
		$smarty->assign('receipt',$receipt);
		$smarty->assign('signature',$signature);
		$smarty->assign('ballotID',$ballotID);	
		$smarty->display('classroomconfirm.tpl');
		
	}


}


function malformedRequest()
{
	header("HTTP/1.1 400 Bad Request");
	exit;
}


//main method

	if(!$xmlData)
	{
		//echo("no Data");
		malformedRequest();

	}
	else
	{
		
		if(!$xmlData->getName()=="classroomvoting")
		{
			//echo("classroomvoting");
			malformedRequest();
		}
		else
		{			
			$type = getTypeTag($xmlData);
			$sessionID = getSessionIDTag($xmlData);

			
			//handle join request		
			if ($type == "join")
			{
				//echo("join");
				$passcode = getPasscodeTag($xmlData);
				joinSession($sessionID,$passcode);
				exit;
			}
			else if ($type == "getresults")
			{
				//echo("getresults");
				$questionNumber = getQuestionTag($xmlData);

				sendResults($sessionID,$questionNumber);
			}
			else
			{
				
				$userID = getUserIDTag($xmlData);

				if ($type == "getquestion")
				{
					//echo("getquestion");
					$questionNumber = getQuestionTag($xmlData);
					sendQuestion($sessionID,$userID,$questionNumber);
				
				}
				else if ($type=="answer")
				{
					//echo("answer");
					$questionNumber = getQuestionTag($xmlData);

					$questionType = getQuestionType($sessionID,$questionNumber);

                                        receiveAnswer($sessionID,$userID,$questionNumber,$questionType,$xmlData);
				}
				else if ($type=="stagetwo")
				{
					//echo("stagetwo");

					$questionNumber = getQuestionTag($xmlData);

					$questionType = getQuestionType($sessionID,$questionNumber);
					$choice = getChoiceTag($xmlData);

					receiveStageTwo($sessionID,$userID,$questionNumber,$questionType,$choice);
				
				}

			}
		}
	}
	



?>
