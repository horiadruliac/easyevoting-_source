<?php 
ini_set('display_errors', 1);
error_reporting(E_ALL);

	require_once ('./classroominclude.php');
	
	

	
	$smarty->assign('highlighted','viewelections');

	session_start();
	if (isset($_SESSION['USERNAME']))
	{
		$smarty->assign('username',$_SESSION['USERNAME']);
	}
	if(isset($_SESSION['is_admin']))
	{
		$smarty->assign('admin','true');
	}	
	
	//echo $_SESSION['USERNAME']	;
	//echo $_GET['sessionid'];
	//echo "THIS IS TEST MESSAGE";
	
	if ( isset($_GET['sessionid']))
  	{
		//echo "SHOW ELECETIONS";
		$sessionID = $_GET['sessionid'];
		$statement = null;
		if(isset($_SESSION['is_admin']))
		{
			$statement = $db->prepare("SELECT locked,username FROM Sessions WHERE sessionID = ?");
		}
		else
		{
			$statement = $db->prepare("SELECT locked,username FROM Sessions WHERE sessionID = ? AND hidden = false");
		}
		$statement->bindValue(1, $sessionID);
		$statement->execute();		
		$row = $statement->Fetch();		

		
		if ($row[0])
		{
			$smarty->assign('is_locked',"Yes");
		}
		else
		{
			$smarty->assign('is_locked',"No");
		}
	
		//check if the user is logged in
		if (isset($_SESSION['USERNAME']))
		{
			//check if the user is the creator of this election
			if( $row[1]==$_SESSION['USERNAME'])
			{
				$smarty->assign('electionowner','true');
			}
		}
		if (isset($_GET['questionid']))
		{
			$questionID = $_GET['questionid'];		
			$smarty->assign('sessionID',$sessionID);
			$smarty->assign('questionID',$questionID);			
			$statement = $db->prepare("SELECT questionText FROM Type3Questions WHERE sessionID = ? AND questionID = ?");
			$statement->bindValue(1, $sessionID);
			$statement->bindValue(2, $questionID);	
			$statement->execute();		

			if ($row = $statement->Fetch())
			{
				$smarty->assign('questiontype',"3");
				$smarty->assign('questionid',$_GET['questionid']);
				$smarty->assign('questiontext',$row[0]);
				$smarty->display('sessionlist/questiondetails.tpl');
			}
			else
			{
				$statement = $db->prepare("SELECT questionText FROM Type4Questions WHERE sessionID = ? AND questionID = ?");
				$statement->bindValue(1, $sessionID);
				$statement->bindValue(2, $questionID);	
				$statement->execute();	
				if ($row = $statement->Fetch())
				{
					$smarty->assign('questiontype',"4");
					$smarty->assign('questionid',$_GET['questionid']);
					$smarty->assign('questiontext',$row[0]);
	


					$smarty->display('sessionlist/questiondetails.tpl');
				}
				else
				{
					$statement = $db->prepare("SELECT questionText FROM Type12Questions WHERE sessionID = ? AND questionID = ?");
					$statement->bindValue(1, $sessionID);
					$statement->bindValue(2, $questionID);	
					$statement->execute();	

					if ($row = $statement->Fetch())
					{
						if (isset($row[1]) > 1 && $row[1] >1)
						{
							$smarty->assign('questiontype',"2");
							$smarty->assign("maxanswers",$row[1]);
						}
						else
						{
							$smarty->assign('questiontype',"1");
						}
						$smarty->assign('questionid',$_GET['questionid']);
						$smarty->assign('questiontext',$row[0]);
						$statement = $db->prepare("SELECT answerNumber, answerText FROM Type12Answers WHERE sessionID = ? AND questionID = ?");
						$statement->bindValue(1, $sessionID);
						$statement->bindValue(2, $questionID);	
						$statement->execute();
						$answers = null;
						$counter = 0;
						while ($row = $statement->Fetch())
						{
							$answers[$counter]['number'] = $row[0];
							$answers[$counter]['text'] = $row[1];
							$counter++;
						}
						$smarty->assign('answers',$answers);
						$smarty->display('sessionlist/questiondetails.tpl');
					}
					else
					{
						$statement = $db->prepare("SELECT sessionID, created FROM Sessions order by sessionID");
						$statement->execute();
						$counter = 0;
						$sessions = null;
						while ($row = $statement->Fetch())
						{
							$sessions[$counter]['id']=$row[0];
							$sessions[$counter]['finished']=$row[1];	
							$counter++;					
						}
						$smarty->assign('sessions',$sessions);
						$smarty->display('sessionlist/sessionlist.tpl');
					}
				}	
			}
		}
		else
		{	
			//get all stage 3 questions and add them to $questions
			$statement = $db->prepare("SELECT questionID,questionText,finished FROM Type3Questions WHERE sessionID = ?");
			$statement->bindValue(1, $sessionID);	
			$statement->execute();
			$is_finished = null;
			$questions = null;
			
			
			while ($row = $statement->Fetch())
			{
				$questions[$row[0]]['text']=$row[1];
				$questions[$row[0]]['id']=$row[0];	

				if($row[2] == 0) $is_finished = "Open";
				else $is_finished = "Closed";
			}
			
			//get all stage 4 questions and add them to $questions
			$statement = $db->prepare("SELECT questionID,questionText,finished FROM Type4Questions WHERE sessionID = ?");
			$statement->bindValue(1, $sessionID);	
			$statement->execute();	
			while ($row = $statement->Fetch())
			{
				$questions[$row[0]]['text']=$row[1];
				$questions[$row[0]]['id']=$row[0];	


                                if($row[2] == 0) $is_finished = "Open";
                                else $is_finished = "Closed";
			}
			
			//get all stage 1/2 questions
			$statement = $db->prepare("SELECT questionID,questionText,finished FROM Type12Questions WHERE sessionID = ?");
			$statement->bindValue(1, $sessionID);	
			$statement->execute();
			while ($row = $statement->Fetch())
			{
				$questions[$row[0]]['text']=$row[1];
				$questions[$row[0]]['id']=$row[0];

                                if($row[2] == 0) $is_finished = "Open";
                                else $is_finished = "Closed";
			}
			if (!is_null($questions))
			{
				ksort($questions);
			}


			$is_created = null;
			$statement = $db->prepare("SELECT created FROM Sessions WHERE sessionID = ?");
			$statement->bindValue(1, $sessionID);	
			$statement->execute();
	                while($row = $statement->Fetch())
			{
                	        $is_created = $row[0];
        	        }
	
			if($is_created == 1){
				$is_created = "Ready";
			}else $is_created = "Still generating";

			$smarty->assign('is_created',$is_created);
			$smarty->assign('is_finished',$is_finished);
			$smarty->assign('sessionid',$_GET['sessionid']);
			$smarty->assign('questions',$questions);
			$smarty->display('sessionlist/sessiondetails.tpl');
		}
	}
	else
	{
//echo("Got here");
		$showElections = 20;
		if (isset($_GET['showelections']))
		{
			if (is_numeric ($_GET['showelections']))
			{
				if ($_GET['showelections']>=0)
				{
					$showElections = $_GET['showelections'];
				}
			}
		}
		$statement = $db->prepare("SELECT COUNT(sessionID)  FROM Sessions");	
		//$statement = $db->prepare("SELECT *  FROM Sessions");	
		$statement->execute();
		$row = $statement->Fetch();
		//echo $statement->Fetch()[0];
		//echo $row[0];
		$moreElections = false;
		if ($row[0]>$showElections)
		{
			$moreElections = true;
		}
		if(isset($_SESSION['is_admin']))
		{
			$statement = $db->prepare("SELECT sessionID, created,passcodeType,passcode,username,title,locked,hidden FROM Sessions order by sessionID DESC limit ?");
		}
		else
		{			
	
			$statement = $db->prepare("SELECT sessionID, created,passcodeType,passcode,username,title,locked,hidden FROM Sessions WHERE hidden = false order by sessionID DESC limit ?");
		}
		$statement->bindValue(1, (int) $showElections, PDO::PARAM_INT);
		$statement->execute();

		$counter = 0;
		$sessions = null;
		//echo $statement->Fetch()[0];
		//echo $row[0];
		while ($row = $statement->Fetch())
		{
		

			$sessions[$counter]['id']=$row[0];
			$sessions[$counter]['generated']=$row[1];	
			$sessions[$counter]['passcodetype']=$row[2];
			if ($row[2] =="none")
			{
				$sessions[$counter]['passcode']="";
			}
			else if ($row[2] == "group")
			{
				$sessions[$counter]['passcode']=$row[3];
			}
			else
			{
				$sessions[$counter]['passcode']="<a href='./getpasscodes.php?sessionid=".$row[0]."'>Click Here</a>";
			}

			//Store username that produced that session
			$sessions[$counter]['username']=$row[4];
			$sessions[$counter]['title']=$row[5];
			$sessions[$counter]['locked']=$row[6];	
			$sessions[$counter]['hidden']=$row[7];
			$statement2 = $db->prepare("SELECT COUNT(*) FROM (SELECT questionText FROM Type12Questions WHERE sessionID = ? AND finished = false UNION SELECT questionText FROM Type3Questions WHERE finished = false AND sessionID = ? UNION SELECT questionText FROM Type4Questions WHERE sessionID = ? AND finished = false) as questions");
			$statement2->bindValue(1, $row[0]);
			$statement2->bindValue(2, $row[0]);
			$statement2->bindValue(3, $row[0]);
			$statement2->execute();
	
			$row2 = $statement2->Fetch();

			if ($row2[0]=="0")
			{
				$sessions[$counter]['finished']=true;
			}
			else
			{
				$sessions[$counter]['finished']=false;
			}
			
			$counter++;					
		}
		$smarty->assign('moreelections',$moreElections);
		$smarty->assign('showelections',$showElections);
		$smarty->assign('sessions',$sessions);
		$smarty->display('sessionlist/sessionlist.tpl');
	}
?>
