<classroomvoting>
	<type>results</type>
	<sessionid>{$sessionID}</sessionid>
	<questionid>{$questionID}</questionid>
	<questiontype>{$questionType}</questiontype>
	<questiontext><![CDATA[{$questiontext}]]></questiontext>	
	<resultset>
{foreach from=$answers item=answer}
<result>
<answer><![CDATA[{$answer.answer}]]></answer>
<votes>{$answer.votes}</votes>
</result>
{/foreach}
	</resultset>
</classroomvoting>