<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Classroom Voting</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="./js/html5shim.js"></script>
<link rel="stylesheet" type="text/css" href="../css/ie8.css">	  
    <![endif]-->
</head>
<body>
{include file="./navbar.tpl"}
<div class="container-narrow">
<div class="jumbotron">
<h1>Missing Answer</h1>
</div>
<div class="pagination-centered">
<h2>This question is a multiple choice question and does not have any answers specified.</h2>
<div class="row">
<form name="myform2" action="login.php" method="post">
<input class="btn btn-large btn-primary" type="submit" value="Back" />
<input type="hidden" name="sessionID" value="{$sessionID}">
<input type="hidden" name="userID" value="{$userID}">
<input type="hidden" name="questions" value="{$questions}">
<input type="hidden" name="publicKey" value="{$publicKey}">
<input type="hidden" name="nextQuestion" value="{$questionID}">
<input type="hidden" name="receiptLength" value="{$receiptLength}">
<input type="hidden" name="previousreceipts" value="{$previousreceipts}">
</form>
</div>
</div>
</div>
</body>
</html>
