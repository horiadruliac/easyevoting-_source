<classroomvoting>
	<type>results</type>
	<sessionid>{$sessionID}</sessionid>
	<questionid>{$questionID}</questionid>
	<questiontype>4</questiontype>
	<questiontext><![CDATA[{$questiontext}]]></questiontext>
	<resultset>
{foreach from=$answers item=answer}
		<answer><![CDATA[{$answer}]]></answer>
{/foreach}
	</resultset>
</classroomvoting>