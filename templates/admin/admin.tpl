<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta name="description" content="">
	<title>Newcastle University E-Voting: Admin</title>
	<link href="./css/bootstrap.css" rel="stylesheet" />
	<link href="./css/bootstrap-responsive.php" rel="stylesheet" />
	<link href="./css/admin.css" rel="stylesheet" />
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="./js/html5shim.js"></script>
		<link rel="stylesheet" type="text/css" href="../css/ie8.css">
	<![endif]-->
</head>
<body>
	{include file="../navbar.tpl"}


	 <div class="container-narrow">
		<div class="jumbotron">
		        <h1>Admin</h1>
				<p>Free disk space: {$freespace}</p>
		        <p class="lead">Use the form below to add new guest and administrator accounts</p>
		</div>

		<div class="alert alert-block alert-success hide">
			<!--<button type="button" class="close" data-dismiss="alert">&times;</button>-->
			<strong>Success!</strong> You have added a new user.</p>
			<p>New user details:</p>
			<p class="ajax_data"></p>
		</div>

		<form class="admin-form form-horizontal" method="POST">	
		<div class="control-group">
	        <label class="control-label">Username</label>
			<div class="controls">
			    <div class="input-prepend">
				<span class="add-on"><i class="icon-user"></i></span>
					<input type="text" class="span3 input-xlarge" name="username" placeholder="Username">
				</div>
			</div>
		</div>
		<div class="control-group">
	        <label class="control-label">Email</label>
			<div class="controls">
			    <div class="input-prepend">
				<span class="add-on"><i class="icon-envelope"></i></span>
					<input type="text" class="span3 input-xlarge" name="email" placeholder="Email (only required for new admins)">
				</div>
			</div>	
		</div>
		<div class="control-group">
	        <label class="control-label">New Account Type</label>
			<div class="controls">
			    <div class="input-prepend">
				<span class="add-on"><i class="icon-tag"></i></span>
					<select name="account_type" id="subject" class="input-xlarge" role="select" aria-required="true">
						<option>Guest</option>
						<option>Administrator</option>
					</select>
			    </div>
			</div>
		</div>
		<div class="control-group">
			<div class="misc-button">
				<button type="submit" class="btn btn-large btn-primary">Submit</button>
	      		</div>	
		</div>
	  	</form>

		<div class="footer">
		</div>
	</div>

	<!-- Javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="./js/jquery-1.9.1.min.js"></script>
	<script src="./js/bootstrap.min.js"></script>
	<script type="text/javascript" src="./js/jquery.validate.js"></script>
	<script type="text/javascript" src="./js/admin-validation.js"></script>
</body>
</html>
