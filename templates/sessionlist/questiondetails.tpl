<!DOCTYPE html> 
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Newcastle E-voting: Questions List</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="./css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap-responsive.php" />
<link rel="stylesheet" type="text/css" href="./css/extra-css.css" />
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="./js/html5shim.js"></script>
<link rel="stylesheet" type="text/css" href="./css/ie8.css" />	  
    <![endif]-->
</head>
<body>
{include file="../navbar.tpl"}
<div class="container-narrow">
<div class="jumbotron">
<h1>Question Details - Session {$sessionID} Question {$questionID} </h1>
</div>
<div>
{if isset($electionowner) || $is_locked=="No"}
<p> Note, if this session is not fully generated the list may not show all answers for Type 1 or 2 questions.</p>

<ul>
<li> Question Type - {$questiontype}</li>
<li> Question Text - {$questiontext}</li>
<li> Session Locked - {$is_locked}</li>
</ul>

{if $questiontype == "1" || $questiontype == "2"}
<h2 style="font-size: 22px">Answers</h2>
<div>
<table class="table table-condensed table-striped">
<thead>
<tr>
<th> Answer Number </th> <th>Answer Text</th>
</tr>
</thead>
<tbody>
{foreach from=$answers item = answer}
<tr> <td> {$answer.number} </td> <td> {$answer.text} </td>
</tr>
{/foreach} 
</tbody>
</table>
</div>
{/if}
{if $questiontype == "2"}
<p> Maximum number of answers choosable - {$maxanswers}</p>
{/if}
{else}
<p> Question not viewable as the session is locked and you are not the session owner </p>
{/if}
</div>
</div>
<script src="./js/jquery-1.9.1.min.js"></script>
<script src="./js/bootstrap.min.js"></script>

</body>
</html>
