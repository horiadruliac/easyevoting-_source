<!DOCTYPE html> 
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    	<meta name="description" content="">
	<title>Newcastle University E-Voting: Question List</title>
	<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="./css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap-responsive.php" />
<link rel="stylesheet" type="text/css" href="./css/extra-css.css" />
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="./js/html5shim.js"></script>
<link rel="stylesheet" type="text/css" href="./css/ie8.css" />	  
    <![endif]-->
</head>
<body>
{include file="../navbar.tpl"}
<div class="container-narrow">
<div class="jumbotron">
<h1>Election Details</h1>
<p class="lead">View the details of each question by clicking its question number</p>
</div>
<div>
	<ul>
		<li>Locked status:  <strong>{$is_locked}</strong></li>
		<li>Election status: <strong>{$is_finished}</strong></li>
		<li>Generating status: <strong>{$is_created}</strong></li>
	</ul>
</div>
{if isset($electionowner) || $is_locked=="No"}
<div class="pagination-centered">
	<p>Note: if the election is not fully generated, the list will only include questions that have already been generated.</p>
</div>
{if isset($questions)}
<h2>Questions</h2>
<div>
<table class="table table-condensed table-striped">
<thead>
<tr>
<th> Question Number </th> <th>Question Text</th>{if isset($electionowner)}<th>Action</th>{/if}
</tr>
</thead>
<tbody>
{foreach from=$questions item = question}
<tr><td><a class="btn btn-primary" href="sessionlist.php?sessionid={$sessionid}&amp;questionid={$question.id}">{$question.id}</a></td><td>{$question.text}</td>
{if isset($electionowner)}

<td>
{if $is_finished=="Open"}
<select title="Change question {$question.id}" onchange="coordaction(this)">
	<option value="select">Select Action</option>
	<option value="confirmfinish({$sessionid},{$question.id})">Finish Question</option>
</select>
{else}
None
{/if}
</td>
{/if}
</tr>
{/foreach} 
</tbody>
</table>
</div>
{else}
<p> There are currently no questions generated for this session.</p>
{/if}
</div>
{else}
<div>
<p> Questions not viewable as the session is locked and you are not the session owner </p>
</div>
{/if}

<script src="./js/jquery-1.9.1.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/sessiondetails.js"></script>
</body>
</html>
