<!DOCTYPE html> 
<html lang="en">
<head>
	<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
	<title>Newcastle University E-Voting: Passcode List</title>
	<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="./js/html5shim.js"></script>
        <![endif]-->
	<style>.table td { text-align: center; }</style>
</head>
<body>
{include file="./navbar.tpl"}
<div class="container-narrow">
<div class="jumbotron">
	<h1>Individual Passcode List</h1>
	<p class="lead">Distribute these to voters to allow them to vote in your election</p>
</div>
<table class="table table-condensed table-striped">
<tbody>
<tr>
{foreach from=$passcodes item = passcode name=list}
{if $smarty.foreach.list.index % 3 == 2}
{if $passcode@last}
<td>{$passcode}</td></tr>
{else}
<td>{$passcode}</td></tr><tr>
{/if}
{else}
<td>{$passcode}</td>
{/if}
{/foreach}
</tbody>
</table>
<div class="pagination-centered">
	<a class="btn btn-danger" href="./passcodes_pdf.php?sessionid={$sessionid}">Download Passcodes As PDF</a>
	<a class="btn btn-primary" href="./individualpasscodes.xml?sessionid={$sessionid}&type=xml">Download Passcodes As XML</a>
</div>
</div>
</body>
</html>
