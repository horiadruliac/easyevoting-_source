<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Election Administration Page</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
</head>
<body>
<div id = "page">
<div id = "masthead">
<h1>Submission Page</h1>
</div>
<div id = "maintext">
<p> Papers Submitted:</p>


<table border='1'>
<tr>
<th>ID </th> <th> Paper Title </th> <th> View Submission </th> <th> View Paper </th> <th> Delete Submission </th>
</tr>
{foreach from=$submissions item=submission}
<tr>
<td>{$submission.id} </td> <td> {$submission.title} </td> 
<td> <a href="paperadmin.php?submissionset={$submissionset}&id={$submission.id}"> Click Here </a> </td> 
<td><a href="viewpaper.php?submissionset={$submissionset}&id={$submission.id}"> View Paper </a></td>
<td> <a href="paperadmin.php?option=delete&submissionset={$submissionset}&id={$submission.id}"> Click Here </a> </td>
</tr>

{/foreach}
</table>
<p> <a href="coordinator.php">Return to co-ordinator page</a></p>
</div>
</div>
</body>
</html>
