<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Submit Paper Page</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
</head>
<body>
<div id = "page">
<div id = "masthead">
<h1>Election Administration Page</h1>
</div>
<div id = "maintext">
<p> Paper details:</p>

<form name="myform" action="submitted.php" method="post" enctype="multipart/form-data">
<div>
<div class = "row">
<span class = "label"> Paper Title:</span> <span class="input"> <input type = "text" name = "title" /> </span>
</div>
<input type="hidden" name="MAX_FILE_SIZE" value="20000000">
<div class = "row">
<span class = "label">Paper (pdf):</span> <span class = "input"><input name="uploadedpdf" type="file" id="uploadedpdf"></span>
</div>
<div class = "row">
<span class = "label">Bibliographic information (authors, name of journal/conference, date published or accepted):</span> <span class = "input"><input type = "text" class = "longinput" name = "biblio" /> </span>
</div>
<div class = "row">
<span class="label">Impact Statement (up to 100 words explaining the merits of the paper):</span> <span class="input"><textarea name = "testimony" class = "bigtextarea"></textarea> </span>
</div>

<input type = "hidden" name="submitter" value="{$submitter}" />
<input type = "hidden" name="submission" value="{$submission}" />
<div class = "row">
<span class="label"></span><span class = "input"><input type="submit" value="Submit" /></span>
</div>
</form>

</div>
</div>
</body>
</html>
