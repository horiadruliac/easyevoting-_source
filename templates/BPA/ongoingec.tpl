<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Election Page</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
</head>
<body>
<div id="page">
<div id="masthead">
<h1>{$election}</h1>
</div>
<div id = "maintext">
{$description}
<hr />
<p> To vote in this election click <a href = "papervote.php?election={$electionnumber}">here</a>. </p> 
<p> To view the bulletin board for this election click <a href = "bulletinboard.php?election={$electionnumber}">here</a>. </p>
<p>If you would like to know more about the election process then please click <a href="additionalinfo.html">here</a> .</p>
<hr />
</div>
</div>
</body>
</html>
