<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Election Administration Page</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
</head>
<body>
<div id = "page">
<div id = "masthead">
<h1>Submission Page</h1>
</div>
<div id = "maintext">
<h2> Confirm End Submission Period</h2>
<p><strong>Are you sure you wish to end the submission period for submission set {$submission}?<p>
<p><strong><a href="submissionadmin.php?option=end&submission={$submission}&sure=yes">Yes</a><p>
<p><strong><a href="submissionadmin.php">No</a><p>
</div>
</div>
</body>
</html>
