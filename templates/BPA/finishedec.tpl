<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Election Page</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
</head>
<body>
<div id="page">
<div id="masthead">
<h1>{$election}</h1>
</div>
<div id = "maintext">
{if $finishdate=="voter"}
<p>All votes have been cast in this election.</p>
{else}
<p>This election finished on {$finishdate}.</p>
{/if}
<p> To view the bulletin board for this election click <a href = "bulletinboard.php?election={$electionnumber}">here</a>. </p> 
<p> To view the papers submitted for this election click <a href = "viewsubmission.php?submissionset={$submissionset}">here</a>. </p> 
</div>
</div>
</body>
</html>
