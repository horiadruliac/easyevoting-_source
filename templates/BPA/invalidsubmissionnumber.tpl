<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Best Paper Election Page</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
</head>
<body>
<div id="page">
<div id="masthead">
<h1>Welcome to the Best Paper Election Page</h1>
</div>
<div id = "maintext">
<p>Unfortunately you have supplied an invalid submission number. Please check the URL you entered, and if this does not resolve the problem then contact the election co-ordinator.</p>
</div>
</div>
</body>
</html>
