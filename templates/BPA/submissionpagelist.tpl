<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Election Administration Page</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
</head>
<body>
<div id = "page">
<div id = "masthead">
<h1>Submission Set Administration Page</h1>
</div>
<div id = "maintext">
<p> Submission pages:</p>


<table border='1'>
<tr>
<th>ID </th> <th> Submission Page Title </th> <th> Submission Page Start Date </th> <th> Submission Page End Date </th> <th> Submission Page URL </th> <th> View Submissions URL </th> <th> End Submission Page </th> <th> Delete Submission Page </th>
</tr>
{foreach from=$submissionpages item=submission}
<tr>
<td><a href="paperadmin.php?submissionset={$submission.id}">{$submission.id}</a> </td> <td> {$submission.name} </td> 
<td> {$submission.startdate}  </td> 
<td> {$submission.enddate}  </td> 
<td> <a href ="submitpaper.php?id={$submission.id}">Submit Paper</a></td> 
<td> <a href="{$submission.viewurl}"> View Submissions (as user) </a> </td>
<td>
{if ($submission.enddate == "Finished")}
Ended
{else}
<a href = "submissionadmin.php?option=end&submission={$submission.id}">End</a>
{/if}
</td>
{if $submission.delete}
<td> <a href = "submissionadmin.php?option=delete&submission={$submission.id}">Delete</a> </td>
{else}
<td> N/A </td>
{/if}
</tr>
{/foreach}
</table>
<p> <a href = "coordinator.php">Return to co-ordinator page</a></p>
</div>
</div>
</body>
</html>
