<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Election Generator Page</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
</head>
<body>
<div id="page">
<div id="masthead">
<h1>Submission Page Generator</h1>
</div>
<div id = "maintext">
<p> Create a new submission page.</p>


<form name="myform" action="createsubmissions.php" method="post">
<div class = "row">
<span class="sublabel">Election Title:</span><span class = "subinput"> <input type = "text" name = "title" class="longinput" /></span>
</div>
<div class = "row">
<span class="sublabel">Start Date and Time:</span><span class = "subinput"><input type = "text" name = "startdate" /> yyyy-mm-dd hh:mm:ss </span>
</div>
<div class = "row">
<span class="sublabel">End Date and Time:</span><span class = "subinput"><input type = "text" name = "enddate" /> yyyy-mm-dd hh:mm:ss </span>
</div>
<input type = "hidden" name="creator" value="{$username}" />
<div class = "row">
<span class="sublabel"></span><span class="subinput"><input type="submit" value="Create" /></span>
</div>
</form>
<p> <a href="coordinator.php">Return to co-ordinator page</a></p>
</div>
</div>
</body>
</html>
