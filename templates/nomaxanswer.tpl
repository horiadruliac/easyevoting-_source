<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Newcastle E-voting: No Max. Answers</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
</head>
<body>
<div class="container-narrow">
<div class="jumbotrom">
<h1>Missing Maximum Answers Value</h1>
</div>
<div class="pagination-centered">
<h2>Your session contains a type 2 question that does not have a maximum number of answers specified.</h2>
<a class="btn btn-large btn-primary" href="./coordinator.php">Back to Coordinator Page</a>
</div>
</div>
</body>
</html>
