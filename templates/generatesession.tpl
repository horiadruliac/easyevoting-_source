<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Session Generator Page</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
<script type="text/javascript">

function deleteQuestion(questionNumber)
{

	eleToRemove = document.getElementsByName(questionNumber)[0];	
	//alert(eleToRemove);	
	 eleToRemove.parentNode.removeChild(eleToRemove);

}

function doQuestionType(question)
{

	eleToChange = document.getElementById(question+"span");
	mySelect = document.getElementById(question);
	
	mySelect.parentNode.removeChild(mySelect);
		
	myValue = mySelect.value;

	var newSpan = document.createElement("span");

	newSpan.className="subinput";
	newSpan.setAttribute("id",question+"span");
	newSpan.appendChild(mySelect);
	var explanatoryText = null;
	if (myValue == 1)
	{
		explanatoryText = document.createTextNode("Type 1 questions are multiple choice questions where one answer can be selected");
	}
	else if (myValue == 2)
	{
		explanatoryText = document.createTextNode("Type 2 questions are multiple choice questions where multiple answers can be selected");
	}
	else if (myValue == 3)
	{
		explanatoryText = document.createTextNode("Type 3 questions are questions where any number can be input as an answer");
	}
	else if (myValue == 4)
	{
		explanatoryText = document.createTextNode("Type 4 questions are questions where any text can be input as an answer");
	}
	var textSeparator = document.createTextNode(" - ");
	newSpan.appendChild(textSeparator);
	var strongElement = document.createElement("strong");
	strongElement.appendChild(explanatoryText);
	newSpan.appendChild(strongElement);
	eleToChange.parentNode.appendChild(newSpan);
	eleToChange.parentNode.removeChild(eleToChange);

	
}

function deleteAnswer(answerNumber)
{

	eleToRemove = document.getElementsByName(answerNumber)[0];	
	//alert(eleToRemove);	
	 eleToRemove.parentNode.removeChild(eleToRemove);

}

function addAnswer(questionNumber)
{


	eleToRemove = document.getElementById(questionNumber+'addanswerblock');
	addAnswerBlock = eleToRemove;
	questionElement=eleToRemove.parentNode;
	
	eleToRemove.parentNode.removeChild(eleToRemove);
	eleToRemove = document.getElementById('delete'+questionNumber+"block");	
	deleteElement = eleToRemove; 
	eleToRemove.parentNode.removeChild(eleToRemove);

	//increment answers
	var answers = parseInt(document.getElementsByName(questionNumber+"answers")[0].value) + 1;

	document.getElementsByName(questionNumber+"answers")[0].value = answers;
	
	//add in new answer block
	var mainElement = document.createElement("fieldset");

 
	var legendElement = document.createElement("legend");
	var legendText = document.createTextNode("Answer");
	legendElement.appendChild(legendText);
	mainElement.appendChild(legendElement);


	//generate answertext element
	
	var div = document.createElement("div");
	div.className="row";
	var span = document.createElement("span");
	span.className="label2";
	
	var labelElement = document.createElement("label");
	labelElement.setAttribute("for", questionNumber+"answer"+answers+"text");
	var labelText = document.createTextNode("Answer Text:");
	labelElement.appendChild(labelText);
	span.appendChild(labelElement);
	div.appendChild(span);
	
	var span = document.createElement("span");
	span.className="subinput";
	
	var textElement = document.createElement("input");
	textElement.setAttribute("type", "text");
	textElement.setAttribute("name", questionNumber+"answer"+answers+"text");
   
   
	span.appendChild(textElement);
	div.appendChild(span);
	mainElement.appendChild(div);
	
	var div = document.createElement("div");
	div.className="row";
	div.setAttribute("name", "delete"+questionNumber+"answer"+answers);
	
	var deleteButton = document.createElement("input");
	deleteButton.setAttribute("value", "Delete Answer");

	deleteButton.setAttribute("type","button");
	deleteButton.setAttribute("onclick","deleteAnswer('"+questionNumber+'answer'+answers+"')");
	div.appendChild(deleteButton);
	mainElement.appendChild(div);
	
	var mainDiv = document.createElement("div");
	mainDiv.setAttribute("name", questionNumber+"answer"+answers);
	mainDiv.className="row";
	
	mainDiv.appendChild(mainElement);
	questionElement.appendChild(mainDiv);

	//add back in addanswerblock and delete
	questionElement.appendChild(addAnswerBlock);
	questionElement.appendChild(deleteElement);	
}

function addQuestion()
{

	//alert("Added a question");
	document.forms['myform'].elements['questions'].value = parseInt(document.forms['myform'].elements['questions'].value)+1;

	var questions = parseInt(document.forms['myform'].elements['questions'].value);
	eleToRemove = document.forms['myform'].elements['sendbutton'];	 
	eleToRemove.parentNode.removeChild(eleToRemove);
	eleToRemove = document.getElementById('addquestionblock');	 
    var mainForm = eleToRemove.parentNode;
	eleToRemove.parentNode.removeChild(eleToRemove);
	//eleToRemove = document.forms['myform'].elements['br1'];
//alert(eleToRemove);
	//	eleToRemove.parentNode.removeChild(eleToRemove);	
	var mainElement = document.createElement("fieldset");
 
    //Assign different attributes to the element.
    var legendElement = document.createElement("legend");
	var legendText = document.createTextNode("Question");
	legendElement.appendChild(legendText);
	mainElement.appendChild(legendElement);

	var link = document.createElement("a");
	var linkText = document.createTextNode("Get help");
	link.appendChild(linkText);
	link.setAttribute("href","./help.php?page=generator&section=question");
	var p = document.createElement("p");	
	p.appendChild(link)
	mainElement.appendChild(p);	

   //add subelements
   	var div = document.createElement("div");
	div.className="row";
	var span = document.createElement("span");
	span.className="label2";
	
	var labelElement = document.createElement("label");
	labelElement.setAttribute("for", "questiontype"+questions);
	var labelText = document.createTextNode("Question Type:");
	labelElement.appendChild(labelText);
	span.appendChild(labelElement);
	div.appendChild(span);
	
	var span = document.createElement("span");
	span.className="subinput";
	span.setAttribute("id","questiontype"+questions+"span");
	var selectElement = document.createElement("select");

	selectElement.setAttribute("name", "questiontype"+questions);
	selectElement.setAttribute("id", "questiontype"+questions);
	selectElement.setAttribute("onchange", "doQuestionType('questiontype"+questions+"')");

	var optionElement = document.createElement("option");
	optionElement.setAttribute("value","1");
	var optionText = document.createTextNode("1");
	optionElement.appendChild(optionText);
	selectElement.appendChild(optionElement);

	var optionElement = document.createElement("option");
	optionElement.setAttribute("value","2");
	var optionText = document.createTextNode("2");
	optionElement.appendChild(optionText);
	selectElement.appendChild(optionElement);
	
	var optionElement = document.createElement("option");
	optionElement.setAttribute("value","3");
	var optionText = document.createTextNode("3");
	optionElement.appendChild(optionText);
	selectElement.appendChild(optionElement);

	var optionElement = document.createElement("option");
	optionElement.setAttribute("value","4");
	var optionText = document.createTextNode("4");
	optionElement.appendChild(optionText);
	selectElement.appendChild(optionElement);
	span.appendChild(selectElement);	
	
	var strongElement = document.createElement("strong");
	var explanatoryText = document.createTextNode("Type 1 questions are multiple choice questions where one answer can be selected");
	strongElement.appendChild(explanatoryText);
	var textSeparator = document.createTextNode(" - ");
	span.appendChild(textSeparator);
	span.appendChild(strongElement);
	
	div.appendChild(span);
   
	mainElement.appendChild(div);
	
   	var div = document.createElement("div");
	div.className="row";
	var span = document.createElement("span");
	span.className="label2";


	var labelElement = document.createElement("label");
	labelElement.setAttribute("for", "questionlocked"+questions);
	var labelText = document.createTextNode("Question Locked:");
	labelElement.appendChild(labelText);
	span.appendChild(labelElement);
	div.appendChild(span);

	var span = document.createElement("span");
	span.className="subinput";
	
	var selectElement = document.createElement("select");

	selectElement.setAttribute("name", "questionlocked"+questions);
	
	var optionElement = document.createElement("option");
	optionElement.setAttribute("value","no");
	var optionText = document.createTextNode("No");
	optionElement.appendChild(optionText);
	selectElement.appendChild(optionElement);

	var optionElement = document.createElement("option");
	optionElement.setAttribute("value","yes");
	var optionText = document.createTextNode("Yes");
	optionElement.appendChild(optionText);
	selectElement.appendChild(optionElement);
	
	span.appendChild(selectElement);	

	
	var extraText = document.createTextNode("-");

	span.appendChild(extraText);
	var extraText = document.createTextNode("Locked questions cannot be answered until you unlock them");
	var strongElement = document.createElement("strong");
	strongElement.appendChild(extraText);
	span.appendChild(strongElement);

	div.appendChild(span);
	mainElement.appendChild(div);
	
	
   	var div = document.createElement("div");
	div.className="row";
	var span = document.createElement("span");
	span.className="label2";

	var labelElement = document.createElement("label");
	labelElement.setAttribute("for", "questiontext"+questions);
	var labelText = document.createTextNode("Question Text:");
	labelElement.appendChild(labelText);
	span.appendChild(labelElement);
	div.appendChild(span);
	
	var span = document.createElement("span");
	span.className="subinput";
	
	var textElement = document.createElement("input");
	textElement.setAttribute("type", "text");
	textElement.setAttribute("name", "questiontext"+questions);
	span.appendChild(textElement);	
   	div.appendChild(span);
   	mainElement.appendChild(div);
	
	
	
   	var div = document.createElement("div");
	div.className="row";
	var span = document.createElement("span");
	span.className="label2";

	
	var labelElement = document.createElement("label");
	labelElement.setAttribute("for", "maxanswers"+questions);
	var labelText = document.createTextNode("Max Answers:");
	labelElement.appendChild(labelText);
	span.appendChild(labelElement);
	div.appendChild(span);
	
	var span = document.createElement("span");
	span.className="subinput";
	
	var textElement = document.createElement("input");
	textElement.setAttribute("type", "text");
	textElement.setAttribute("name", "maxanswers"+questions);
	span.appendChild(textElement);
		var labelText = document.createTextNode(" - ");

	span.appendChild(labelText);
	var labelText = document.createTextNode("Only needed for type 2 questions");
	var strongElement = document.createElement("strong");
	strongElement.appendChild(labelText);
	span.appendChild(strongElement);
	
	div.appendChild(span);

	mainElement.appendChild(div);
	
	var div = document.createElement("div");
	div.className="row";
	var strongElement = document.createElement("strong");
	var noteText = document.createTextNode("Note: answers only need to be given for type 1 and type 2 questions.");
	strongElement.appendChild(noteText);
	div.appendChild(strongElement);
		mainElement.appendChild(div);
	
   var hiddenElement = document.createElement("input");
   hiddenElement.setAttribute("name","question"+questions+"answers");
   hiddenElement.setAttribute("value","0");
   hiddenElement.setAttribute("type","hidden");
   
	mainElement.appendChild(hiddenElement);

   
   
	var div = document.createElement("div");
	div.className="row";
	div.setAttribute("id","question"+questions+"addanswerblock");

	var answerButton = document.createElement("input");
	answerButton.setAttribute("value","Add Answer");
	answerButton.setAttribute("name","addanswerq"+questions);
	answerButton.setAttribute("type","button");
	answerButton.setAttribute("onclick","addAnswer('question"+questions+"')");	
	
	div.appendChild(answerButton);
	
	mainElement.appendChild(div);
	var div = document.createElement("div");
	div.className="row";
	div.setAttribute("id","deletequestion"+questions+"block");
	var deleteButton = document.createElement("input");
	deleteButton.setAttribute("value", "Delete Question");
    deleteButton.setAttribute("name", "deletequestion"+questions);
	deleteButton.setAttribute("type","button");
	deleteButton.setAttribute("onclick","deleteQuestion('question"+questions+"')");

	
	div.appendChild(deleteButton);
	mainElement.appendChild(div);   
   
   
   //add to form
   var maindiv = document.createElement("div");
   maindiv.className="row";
   
   maindiv.setAttribute("name", "question"+questions);
   maindiv.appendChild(mainElement);
    mainForm.appendChild(maindiv);


	
	var div = document.createElement("div");
	div.setAttribute("id","addquestionblock");
		div.className="row";
		var addButton = document.createElement("input");
	addButton.setAttribute("value", "Add Question");
    addButton.setAttribute("name", "addquestion");
	addButton.setAttribute("type","button");
	addButton.setAttribute("onclick","addQuestion()");
	div.appendChild(addButton);
	
	mainForm.appendChild(div);
		var div = document.createElement("div");
			div.className="row";
	var submitButton = document.createElement("input");
	submitButton.setAttribute("value", "Create Session");
    submitButton.setAttribute("name", "sendbutton");
	submitButton.setAttribute("type","submit");
		div.appendChild(submitButton);
	    mainForm.appendChild(div);
		
		addAnswer("question"+questions);
		addAnswer("question"+questions);

}
</script>
</head>
<body>

<div id="page">
<div id="masthead">
<h1>Session Generator Page</h1>
</div>
<div id = "maintext">

<form name="myform" action="./generatesession.php" method="post" accept-charset="UTF-8">

<div class="row">
<span class="label2">Number of users:</span><span class="subinput"><input type="text" name="users" /></span>
</div>
<div class="row">
<span class="label2">PasscodeType:</span><span class="subinput">
<select name = "passcodetype">
  <option value="none">None</option>
  <option value="group">Group</option>
  <option value="individual">Individual</option>
</select> </span>
</div>
<div class="row">
<span class="label2">Passcode:</span><span class="subinput"> <input type="text" name="passcode" /> - <strong>Only needed for group passcode type</strong></span>
</div>
<div class ="row">
<p> <a href ="./help.php?page=generator&section=general" target="_blank">Get help</a> </p>
<fieldset>
<legend>
Advanced Options
</legend>

<div class="row">
<strong> Most users will not need to change any of the advanced options. They can be safely left at their standard values unless you have a particular reason to change them.</strong></p>
</div>
<p>
<a href ="./help.php?page=generator&section=advanced" target="_blank">Get help</a>
</p>
<div class="row">
<span class="label2">Security Level:</span><span class="subinput">
<select name = "securitylevel">
  <option value="128">128</option>
</select> </span>
</div>
<div class="row">
<span class="label2">Multiplier:</span><span class="subinput"> <input type="text" name="multiplier" value="5"> - <strong>The number of attempts a user has to answer a multiple choice question.</strong> </span>
</div>
<div class="row">
<span class="label2">Receipt Length:</span><span class="subinput"> <input type="text" name="receiptlength" value="5"></span>
</div>
</fieldset>
</div>
<div class="row" name="question1">
<fieldset>
<legend>
Question
</legend>
<p> <a href ="./help.php?page=generator&section=question" target="_blank">Get help</a> </p>
<div class= "row" >
<span class="label2">Question Type:</span><span class="subinput" id="questiontype1span"> 
<select id="questiontype1" name = "questiontype1" onchange="doQuestionType('questiontype1')">
  <option value="1">1</option>
  <option value="2">2</option>
  <option value="3">3</option>
  <option value="4">4</option>
</select> - <strong>Type 1 questions are multiple choice questions where one answer can be selected</strong></span>
</div>
<div class = "row">
<span class="label2">Question Locked:</span><span class="subinput"> 
<select name = "questionlocked1">
  <option value="no">No</option>
  <option value="yes">Yes</option>
</select> 
- <strong>Locked questions cannot be answered until you unlock them</strong> 
</span>
</div>
<div class= "row">
<span class="label2">Question Text:</span><span class="subinput"><input type="text" name="questiontext1" /></span>
</div>
<div class= "row">
<span class="label2">Max Answers:</span><span class="subinput"> <input type="text" name="maxanswers1" /> - <strong>Only needed for type 2 questions</strong></span>
</div>
<div class ="row">
<strong>Note: answers only need to be given for type 1 and type 2 questions.</strong>
</div>
<div class= "row" name="question1answer1">
<fieldset >
<legend>
Answer
</legend>
<div class= "row">
<span class="label2">Answer Text:</span><span class="subinput"><input type = "text" name = "question1answer1text"></span>
</div>
<div class= "row">
<input type = "button" value="Delete Answer" name="question1answer1deleteanswer" onclick="deleteAnswer('question1answer1')"/> <br/>
</div>
</fieldset>
</div>

<div class= "row" name="question1answer2">
<fieldset>
<legend>
Answer
</legend>
<div class= "row">
<span class="label2">Answer Text:</span><span class="subinput"><input type = "text" name = "question1answer2text"></span>
</div>
<div class = "row">
<input type = "button" value="Delete Answer" name="question1answer2deleteanswer" onclick="deleteAnswer('question1answer2')"/> <br/>
</div>
</fieldset>
</div>




<input type="hidden" name="question1answers" value="2">

<div class="row" id="question1addanswerblock">
<input type = "button" value="Add Answer" name="addanswerq1" onclick="addAnswer('question1')"/> 
</div>
<div class="row" id="deletequestion1block">
<input type = "button" value="Delete Question" name="deletequestion1" onclick="deleteQuestion('question1')"/> 
<div>
</fieldset>

</div>
<input type="hidden" name="questions" value="1">

<div class="row" id="addquestionblock">
<input type = "button" value="Add Question" name="addquestion" onclick="addQuestion()"/> 
</div>
<div class="row">
<input type="submit" value="Create Session" name="sendbutton"/>
</div>
</form>

</div>
</div>
</body>
</html>
