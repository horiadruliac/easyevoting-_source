<?xml version="1.0"?>
<classroomvoting>
<type>question</type>
<sessionid>{$sessionID}</sessionid>
	<questionid>{$questionID} </questionid>
	<questiontext><![CDATA[{$questionText}]]></questiontext>
	<finished>{$finished}</finished>
	<questiontype>
		<typenumber>{$questionType}</typenumber>
{if $questionType == "2"}
		<maxanswers>{$maxAnswers}</maxanswers>
{/if}
	</questiontype>
{if $questionType == "1" || $questionType == "2" || $questionType=="5"}
	<answers>
{foreach from=$answers item = answer}
		<answer>
			<number>{$answer.number}</number>
			<text><![CDATA[{$answer.text}]]></text>			
		</answer>
{/foreach}
	</answers>
{/if}	
</classroomvoting>