<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    	<meta name="description" content="">
	<title>Newcastle University E-Voting: Contact</title>
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.php" rel="stylesheet">
	<link href="css/contactstyle.css" rel="stylesheet">
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="./js/html5shim.js"></script>
<link rel="stylesheet" type="text/css" href="./css/ie8.css" />	  
	<![endif]-->
</head>
<body>
	{include file="../navbar.tpl"}

	 <div class="container-narrow">
		<div class="jumbotron">
		        <h1>Contact</h1>
		        <p class="lead">Use the below form to report any issues or enquire about the system</p>
		</div>

		<div class="alert alert-block alert-success hide">
			<!--<button type="button" class="close" data-dismiss="alert">&times;</button>-->
			<p><strong>Thank you!</strong> Your enquiry has been sent to the administrators.</p>
		</div>

		<form class="contact-us form-horizontal" action="process_contact.php" method="GET">	
		<div class="control-group">
	        <label class="control-label" for="name">Name</label>
			<div class="controls">
			    <div class="input-prepend">
				<span class="add-on"><i class="icon-user"></i></span>
					<input type="text" class="span3 input-xlarge" id="name" name="name" placeholder="Name">
				</div>
			</div>
		</div>
		<div class="control-group">
	        <label for="email" class="control-label">Email</label>
			<div class="controls">
			    <div class="input-prepend">
				<span class="add-on"><i class="icon-envelope"></i></span>
					<input type="text" class="span3 input-xlarge" id="email" name="email" placeholder="Email">
				</div>
			</div>	
		</div>
		<div class="control-group">
	        <label class="control-label" for="subject">Subject</label>
			<div class="controls">
			    <div class="input-prepend">
				<span class="add-on"><i class="icon-tag"></i></span>
					<select name="subject" id="subject" class="input-xlarge">
						<option>General</option>
						<option>Bug Report</option>
						<option>Other</option>
					</select>
			    </div>
			</div>
		</div>
		<div class="control-group">
	        <label class="control-label" for="comment">Comment (500 Characters Maximum)</label>
			<div class="controls">
			    <div class="input-prepend">
				<span class="add-on"><i class="icon-edit"></i></span>
					<textarea name="comment" id="comment" class="span4" rows="4" cols="80" placeholder="Enter your queries here..."></textarea>
				</div>
			</div>
		</div>
		<div class="control-group">
		<div class="misc-button">
			<button type="submit" class="btn btn-large btn-primary">Submit</button>
	      	</div>	
		</div>
	  	</form>
		

		

		<div class="footer">
		</div>
	</div>

	<!-- Javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="./js/jquery-1.9.1.min.js"></script>
	<script src="./js/bootstrap.min.js"></script>
	<script type="text/javascript" src="./js/jquery.validate.js"></script>
	<script type="text/javascript" src="./js/validation.js"></script>
</body>
</html>

