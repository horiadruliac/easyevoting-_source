<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Newcastle E-voting: Results Page</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="./css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap-responsive.php" />
<link rel="stylesheet" type="text/css" href="./css/extra-css.css" />
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="./js/html5shim.js"></script>
<link rel="stylesheet" type="text/css" href="./css/ie8.css" />	  
    <![endif]-->
</head>
<body>
{include file="../navbar.tpl"}
<div class="container-narrow">
<div class="jumbotron">
<h1>Results Page: Session {$sessionID} Question {$questionID} </h1>
</div>
<div class="pagination-centered">
<div id="resultsholder">
<h2> {$questiontext} </h2>
<h3>Results</h3>

{if $finished == false}
<h4>Voting has not yet finished!</h4>
{/if}
<div>
<table class="table table-striped">
<thead>
<tr>
<th class="votingoptions"> Answer </th> <th class="votingoptions"> Votes </th>
</tr>
</thead>
<tbody>
{foreach from=$results item = result}
<tr> <td class="votingoptions"> {$result.answer} </td> <td class="votingoptions"> {$result.votes} </td> </tr>
{/foreach} 
</tbody>
</table>
</div>
<img src="generategraph.php?sessionid={$sessionID}&questionid={$questionID}" alt="Graph of results"/>
<p><em>Note: Type 3 questions do not have ballots as they accept free numerical input.</em></p>
</div>
</div>
</div>
<script src="./js/jquery-1.9.1.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
</body>
</html>
