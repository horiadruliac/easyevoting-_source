<!DOCTYPE html> 
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Newcastle University E-Voting: Bulletin Board</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="./css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap-responsive.php" />
<link rel="stylesheet" type="text/css" href="./css/extra-css.css" />
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="./js/html5shim.js"></script>
<link rel="stylesheet" type="text/css" href="./css/ie8.css" />	  
    <![endif]-->
<style>
.button-area { text-align: center; }
.cushion { padding: 8px; } 
</style>
</head>
<body>
{include file="../navbar.tpl"}


<div class="container-narrow">
<div class="jumbotron">
<h1>Bulletin Board</h1>
<p class="lead">Enter a Session ID and Question ID to verify your ballot</p>
</div>
<!--New Form-->
<form name="myform" action="classroombulletinboard.php" method="get" class="form-horizontal">
<fieldset>
<div>
<!-- Election ID input-->
<div class="control-group cushion">
  <label for="sessionid" class="control-label">Session ID</label>
  <div class="controls">
 	<div class="input-prepend"> 
	<span class="add-on"><i class="icon-list"></i></span>
    		<input id="sessionid" name="sessionid" required="" placeholder="Enter the Session ID here..." type="text" role="textbox">
	</div>
  </div>
</div>
<!-- Question ID input-->
<div class="control-group cushion">
  <label for="questionid" class="control-label">Question ID</label>
  <div class="controls">
	<div class="input-prepend">
	<span class="add-on"><i class="icon-question-sign"></i></span>
    	<input id="questionid" name="questionid" placeholder="Enter the Question ID here..." class="input-xlarge" type="text" role="textbox">
	</div>
  </div>
</div>
</div>
</fieldset>
<div class="button-area">
	<button class="btn btn-large btn-primary" type="submit">Submit</button>
</div>
</form>
<!--New form-->
</div>
<script src="./js/jquery-1.9.1.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
</body>
</html>
