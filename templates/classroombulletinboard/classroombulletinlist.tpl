<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Newcastle E-voting: Bulletin Board Page</title>

<style>

.results-btn { width: 140px; }
h2 { font-size: 18px; }
</style>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap-responsive.php" />
<link rel="stylesheet" type="text/css" href="./css/extra-css.css" />
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="./js/html5shim.js"></script>
<link rel="stylesheet" type="text/css" href="./css/ie8.css" />	  
    <![endif]-->
</head>
<body>
{include file="../navbar.tpl"}

<div class="container-narrow">
<div class="jumbotron">
<h1 style="font-size: 48px">Bulletin Board</h1>
<p class="lead">Session ID: {$sessionID}, Question: {$questionID}</p>
</div>
<div id = "maintext">
<p> <strong>Question:</strong> {$questiontext} </p>
{if isset($results)}
<h2>Results</h2>


<div>
<table class="table table-condensed table-striped">
<thead>
<tr>
<th>Answer Number</th><th>Text</th><th>Votes</th>
</tr>
</thead>
{$counter=1}
{foreach from=$results item = result}
<tr>
<td>{$counter++} </td> <td> {$result.answer} </td> <td> {$result.votes} </td>
</tr>

{/foreach}
</table>
</div>
<p><img src="./generategraph.php?sessionid={$sessionID}&questionid={$questionID}" alt="Graph of results"/></p>
<br/>
<div>
<table class="table table-condensed table-striped">
<thead>
<tr>
<th>Ballot Type</th><th>Number</th>
</tr>
</thead>
<tbody>
<tr><td>Confirmed</td><td>{$confirmed}</td></tr>
<tr><td>Cancelled</td><td>{$cancelled}</td></tr>
<tr><td>Unused</td><td>{$unused}</td></tr>
</tbody>
</table>
</div>


<p> Right click to save the <a href = "receipt.xml?sessionid={$sessionID}&questionid={$questionID}&machine=ballotreceipts">receipt XML</a> file for verification </p>
{/if}
<p> Right click to save the <a href = "receipt.xml?sessionid={$sessionID}&questionid={$questionID}&machine=publickeys">public key XML</a> file for verification </p>
<p> Right click to download the  <a href = "ClassroomChecker.jar">verification tool</a></p>
<p> Right click to download the  <a href = "ClassroomChecker Source.zip">verification tool source code</a></p>
<table class="table table-condensed table-striped">
<thead>
<tr>
<th>Ballot Number</th>
<th>Link</th>
</tr>
</thead>
<tbody>
{foreach from = $ballots item = ballot}
<tr>
<td> {$ballot.number} </td> 
<td> 

{if $ballot.link == -1}
Not yet cast.
{else}
<a class="btn" href="./classroombulletinboard.php?sessionid={$sessionID}&questionid={$questionID}&ballot={$ballot.link}">View Ballot Receipt</a></td>
{/if}
</tr>
{/foreach}
</tbody>
</table>
<p class="pagination-centered"><a class="btn btn-primary" href="./classroombulletinboard.php">View The Bulletin Board For Another Question</a></p>
<hr/>
</div>
</div>
<script src="./js/jquery-1.9.1.min.js"></script>
<script src="./js/bootstrap.min.js"></script>

</body>
</html>
