<!DOCTYPE html> 
<html lang="en">
<head>
	<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="">
        <title>Newcastle University E-Voting: Session Generated</title>
	<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
	<style>
		h2{ font-size:24px }
		#note { padding: 20px}
	</style>
</head>
<body>
{include file="./navbar.tpl"}

<div class="container-narrow">
<div class="jumbotron">
<h1>Session Generated</h1>
</div>
<div class="pagination-centered">
	<h2>Your Election ID is {$sessionID}</h2>
</div>
<div class="pagination-centered" id="note">Note that sometimes no Election ID may be displayed if server load is heavy, this does not mean that your session has not been generated.</div>
<div class="pagination-centered">
<p><a class="btn btn-primary" href="./manage.php">Manage Your Elections</a></p>
<hr />
<p><a class="btn" href="./coordinator.php">Create Another Election</a></p>
</div>
</div>
<script src="./js/jquery-1.9.1.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
</body>
</html>
