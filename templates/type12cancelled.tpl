 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Newcastle E-voting: Cancelled Vote</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap-responsive.php" />
<link rel="stylesheet" type="text/css" href="./css/extra-css.css" />
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="./js/html5shim.js"></script>
<link rel="stylesheet" type="text/css" href="./css/ie8.css" />	  
    <![endif]-->
</head>
<body>
{include file="./navbar.tpl"}
<div class="container-narrow">
<div class="jumbotron">
<h1>Vote Status</h1>
</div>
<div class="pagination-centered">
<div id="receiptholder">
<h2>Cancelled Ballot</h2>
<p><strong>Answer:</strong> {$answertext}</p>
<p><strong>Session ID: {$sessionID}</strong></p>
<p><strong>Question ID: {$questionID}</strong></p>
<p><strong>Ballot ID: {$ballotnumber}</strong></p>
<p>Receipt: <strong id='receipt'>{$receipt}</strong></p>
<a class="btn" href="./classroombulletinboard.php?sessionid={$sessionID}&questionid={$questionID}&ballot={$ballotnumber}" target="_blank">Verify Receipt</a>

<h3>You have {$ballots} attempts remaining.</p>
<table style="margin: 0 auto !important; float: none !important;">
<tr>
<td>
{if $ballots>0}
<form name="myform2" action="login.php" method="post">
<input style="margin:10px;" class="btn btn-large btn-primary" type="submit" value="Answer" />
<input type="hidden" name="sessionID" value="{$sessionID}">
<input type="hidden" name="userID" value="{$userID}">
<input type="hidden" name="questions" value="{$questions}">
<input type="hidden" name="publicKey" value="{$publicKey}">
<input type="hidden" name="nextQuestion" value="{$questionID}">
<input type="hidden" name="receiptLength" value="{$receiptLength}">
<input type="hidden" name="previousreceipts" value="{$previousreceipts}">
</form>
</td>
{/if}
<td>
<form name="myform" action="login.php" method="post">

<input class="btn btn-large btn-danger" type="submit" value="Skip" style="margin:10px;padding: 11px 30px;"></input>
<input type="hidden" name="sessionID" value="{$sessionID}">
<input type="hidden" name="userID" value="{$userID}">
<input type="hidden" name="questions" value="{$questions}">
<input type="hidden" name="publicKey" value="{$publicKey}">
<input type="hidden" name="nextQuestion" value="{$questionID+1}">
<input type="hidden" name="receiptLength" value="{$receiptLength}">
<input type="hidden" name="previousreceipts" value="{$previousreceipts}">
</form>
</td>
</tr>
</table>
</div>
</div>
</div>
<script src="./js/jquery-1.9.1.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/splitter.js"></script>
</body>
</html>
