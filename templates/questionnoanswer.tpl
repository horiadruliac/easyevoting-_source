<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Classroom Voting</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
</head>
<body>
<div class="container-narrow">
<div class="jumbotron">
<h1>Missing Answer</h1>
</div>
<div class="pagination-centered">
<h2>A multiple choice question does not have any answers specified</h2>
<a style="margin:10px;" class="btn btn-large btn-primary" href="./demo/coordinator.php">Back to Coordinator Page</a>
</div>
</div>
</body>
</html>
