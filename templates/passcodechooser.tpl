<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>View Passcodes</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
</head>
<body>
<div id="page">
<div id="masthead">
<h1>View Passcodes</h1>
</div>
<div id = "maintext">
<h2>View passcodes for a session with individual passcodes.</h2>

<div id="formholder">
<form name="myform" action="getpasscodes.php" method="get">
<div class="row">
<span class="loginlabel">Election Session ID:</span><span class="subinput">  <input type = "text" name = "sessionid" /> </span>
</div>
<div class="row">
<input type="submit" value="View Passcodes" />
</div>
</form>
</div>
<input type="submit" value="Coordinator Page" onClick="location.href='coordinator.php'" />
</div>
</div>
</body>
</html>
