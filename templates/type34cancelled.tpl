<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Newcastle University E-Voting</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="./js/html5shim.js"></script>
<link rel="stylesheet" type="text/css" href="./css/ie8.css" />	  
    <![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
{include file="./navbar.tpl"}
<div class="container-narrow">
<div class="jumbotron">
<h1>Vote Status</h1>
</div>
<div class="pagination-centered">
<div id="receiptholder">
<h2>{$questionText}</h2>
<p><strong>Answer: {$answerstring}</strong></p>
<h3>Cancelled</h3>
<hr/>
<table style="margin: 0 auto !important; float: none !important;">
<tr>
<td>
<form name="myform2" action="login.php" method="post">
<input style="margin: 10px;" class="btn btn-large btn-primary" type="submit" value="Answer" />
<input type="hidden" name="sessionID" value="{$sessionID}">
<input type="hidden" name="userID" value="{$userID}">
<input type="hidden" name="questions" value="{$questions}">
<input type="hidden" name="publicKey" value="{$publicKey}">
<input type="hidden" name="nextQuestion" value="{$questionID}">
<input type="hidden" name="receiptLength" value="{$receiptLength}">
<input type="hidden" name="previousreceipts" value="{$previousreceipts}">
</form>
</td>
<td>

<form name="myform" action="login.php" method="post">
<input style="margin: 10px;padding: 11px 30px;" class="btn btn-large btn-danger" type="submit" value="Skip"  />
<input type="hidden" name="sessionID" value="{$sessionID}">
<input type="hidden" name="userID" value="{$userID}">
<input type="hidden" name="questions" value="{$questions}">
<input type="hidden" name="publicKey" value="{$publicKey}">
<input type="hidden" name="nextQuestion" value="{$questionID+1}">
<input type="hidden" name="receiptLength" value="{$receiptLength}">
<input type="hidden" name="previousreceipts" value="{$previousreceipts}">
</form>

</td></tr>
</table>
</div>
</div>
</div>
<script src="./js/jquery.js"></script>
<script src="./js/bootstrap.min.js"></script>
</body>
</html>
