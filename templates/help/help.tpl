<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Newcastle University E-Voting: Help</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<link href="./css/bootstrap.css" rel="stylesheet" />
        <link href="./css/bootstrap-responsive.php" rel="stylesheet" />
	<link href="./css/extra-css.css" rel="stylesheet" />
	<link href="./css/helpstyle.css" rel="stylesheet" />
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    	<!--[if lt IE 9]>
      		<script src="./js/html5shim.js"></script>
			<link rel="stylesheet" type="text/css" href="./css/ie8.css" />
      	<![endif]-->

</head>
<body>
    {include file="../navbar.tpl"}

    <div class="container-narrow">
	<div class="jumbotron">
		<h1>Help</h1>
		<p class="lead">View help and assistance on this page</p>
	</div>
    
	<h2>Important Information</h2>
	<p> To create or manage elections you must have a Newcastle University Campus Login </p>
	<hr />
	<h2>Your First Election</h2>
	<p>To create your first election, proceed with the following:</p>
	<ul>
		<li>Click "Create Election" from the top navigation bar</li>
		<li>Complete the appropriate fields, using the help pointers as guidance</li>
		<li>Choose to lock an election - this prevents voters from accessing the election before you have unlocked it via the <a href="./manage.php">Manage Page</a></li>
		<li>Add any additional questions or answers using the "Add Answer" and "Add Question" buttons.  Delete any extraneous questions and answers with the appropriate buttons</li>
		<li>Click "Create Session" to create your election</li>
		<li>Election creation may take a while depending on the number of specified voters, number of questions and number of answers.  Don't worry, that doesn't mean your election hasn't or won't be created</li>
		<li>Distribute the returned Election ID and the passcode you specified to voters.  They can now vote in your new election using their mobile devices or through the web interface</li>
	</ul><hr/>
	<h2>Mobile Applications</h2>
	<ul>
		<li>Users may vote in your newly created election by using the voting interface available for iOS and Android devices</li>
		<li>
		<ul>
			<li><a href="https://play.google.com/store/apps/details?id=uk.ac.ncl.evoting&amp;hl=en" target="_blank">Android Application</a></li>
			<li><a href="https://itunes.apple.com/gb/app/id565080670?mt=8&amp;affId=1744357" target="_blank">iOS Application</a></li>
		</ul>
		</li>
		<li>Alternatively, voters may use the web interface - accessible at <a href="./index.php">the home page </a>- to conduct voting</li>	
	</ul><hr/>
	<h2>Managing Elections</h2>
	<p>To manage elections, use the <a href="./manage.php">Manage Page</a> and proceed with the following:</p>
	<ul>
		<li><strong>Unlocking an election: </strong>Select "Unlock Election" to unlock a previous locked election - voters may now commence voting in your election</li>
		<li><strong>Finish an election: </strong>Select "Finish Election" from the "Actions" dropdown to finish an election - this prevents further voters from accessing your election and computes the results</li>
		<li><strong>Viewing results of an election: </strong>Select "View Results" from the "Actions" dropdown - you should automatically be directed to the results page</li>
	</ul><hr/>
	<h2>Verifying Votes</h2>
	<p>To verify your voting receipt to check if your vote was casted and counted correctly, proceed with the following steps:</p>
	<ul>
		<li>Access the <a href="./classroombulletinboard.php">Bulletin Board</a> from the home page or the top navigation bar</li>
		<li>Enter the Election ID and Question ID provided to you</li>
		<li>Find the ballot corresponding to the ID given to you during the voting process</li>
		<li>Click the link to view the receipt that was computed by the server</li>
		<li>If both receipts match, your vote was filed correctly; if not, contact an administrator immediately via the <a href="./contact.php">Contact Page</a></li>
	</ul><hr/>
	<h2>Contact an Administrator</h2>
	<p>Want further help or wish to report a bug?  Access the <a href="./contact.php">Contact Page</a> and complete the form.  An administrator will respond to your query as soon as possible</p>
    </div>

    <script src="./js/jquery.js"></script>
    <script src="./js/bootstrap.js"></script>
    <script src="./js/bootstrap.min.js"></script>
</body>
</html>
