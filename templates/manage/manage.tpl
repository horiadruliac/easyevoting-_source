
<!DOCTYPE html>
<html lang="en">
<head>
<title>Newcastle University E-Voting: Manage Elections</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap-responsive.php" />
<link rel="stylesheet" type="text/css" href="./css/extra-css.css" />
<link rel="stylesheet" type="text/css" href="./css/managestyle.css" />
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="./js/html5shim.js"></script>
<link rel="stylesheet" type="text/css" href="./css/ie8.css" />	  
    <![endif]-->

<script src="./js/jquery-1.9.1.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/manage.js"></script>
	

<!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="../assets/ico/favicon.png">
</head>
<body>
	
{include file="../navbar.tpl"}


<div class="container-narrow">
<div class="jumbotron">
<h1>Manage Elections</h1>
<p class="lead">View and administrate your elections</p>
</div>
<div id = "maintext">
	{if isset($sessions)}
	<div>
		<table class="table table-condensed table-striped">
		<thead>
		<tr>
			<th>ID</th><th>Status</th><th>Passcode</th><th>Title</th><th>Action</th>
		</tr>	
		</thead>
		<tbody>
		{foreach from=$sessions item = election}
		<tr> 
			<td><a class="btn btn-primary" href="./sessionlist.php?sessionid={$election[0]}">{$election[0]}</a></td> 
			<td> 
				{if ($election[1] == "Generating")} 
					Generating
				{else}
					{if ($election[5])}
						Locked
					{elseif ($election[6])}
						Finished
					{else}
						Ready
					{/if}
				{/if}

			</td> 
			<td id="passcode{$election[0]}">
				{$election[3]}
			</td>
			<td>
				{$election[4]}	
			</td>
			<td>
				<select title = "Change election {$election[0]}" onchange="coordaction(this)">
					<option value="select">Select Action</option>
					{if ($election[5])}
					<option value="unlocksession({$election[0]})" >Unlock Election</option>
					{/if}
					{if (!$election[6])}					
					<option value="confirmfinish({$election[0]})">Finish Election</option>
					{/if}
					<option value="viewresults({$election[0]})">View Results</option>
					{if ($election[1] != "Generating") }
					<option value="duplicateelection({$election[0]})">Duplicate Election</option>
					<option value="viewpdf({$election[0]})">View as PDF</option>					
					{/if}
				</select>
			</td>
		</tr>
		{/foreach}
		</tbody>
		</table>
		<a id="electionend"> </a>
		{if $moreelections}
			<div><a href="./manage.php?showelections={($showelections+20)}#electionend">Show More Elections</a></div>
		{/if}
	</div>
	{else}
		<div class="jumbotron">
		<h3>You haven't created any elections!</h3>
		</div>
	{/if}
</div>
</div>


</body>
</html>
