<classroomvoting>
	<type>results</type>
	<sessionid>{$sessionID}</sessionid>
	<questionid>{$questionID}</questionid>
	<questiontype>3</questiontype>
	<questiontext><![CDATA[{$questiontext}]]></questiontext>
	<resultset>
{foreach from=$answers item=answer}
		<answer>{$answer}</answer>
{/foreach}
	</resultset>
</classroomvoting>