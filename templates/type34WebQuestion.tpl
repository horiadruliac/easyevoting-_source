 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Classroom Voting</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" type="text/css" href="./css/Type4_files/mainstyle.css" />
<style>
        .option{ padding:10px; font-size: 18px; }
        .row { padding: 10px; }
</style>
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="./js/html5shim.js"></script>
<link rel="stylesheet" type="text/css" href="./css/ie8.css" />	  
    <![endif]-->
<body>
{include file="./navbar.tpl"}
<div class="container-narrow">
<!-- To be added -->
<!-- End to be added -->
<div class="jumbotron">
<h1>Question {$questionID}/{$questions}</h1>
</div>
<div class="pagination-centered">
<h2>{$questionText}</h2> 
<form id="theform" name="myform" action="login.php" method="post">
<div id="answerholder">
{if $questionType == "3"}
<div class="row">
<span class="label2"><strong>Answer (numbers only):</strong></span><span class="subinput">  <input id="type3-input" type = "text" name = "answerstring" /> </span>
</div>
</div>
{else}
<div class="row">
<span class="label2"><strong>Answer:</strong></span><span class="subinput">  <textarea id="type4-input" type = "text" name = "answerstring" /></textarea> </span>
</div>
</div>
{/if}
<hr/>
</div>
<input type="hidden" name="sessionID" value="{$sessionID}">
<input type="hidden" name="userID" value="{$userID}">
<input type="hidden" name="questions" value="{$questions}">
<input type="hidden" name="publicKey" value="{$publicKey}">
<input type="hidden" name="nextQuestion" value="{$questionID}">
<input type="hidden" name="questionType" value="{$questionType}">
<input type="hidden" name="questionText" value="{$questionPass}">
<input type="hidden" name="receiptLength" value="{$receiptLength}">
<input type="hidden" name="previousreceipts" value="{$previousreceipts}">
<div class="pagination-centered">
<input type="submit">
</div>
</form>



</div>
<script>
$(document).ready(function() {
	$("#submitButton").click(function() {
		var questiontype = {$questionType};
		var intregex = /^\d+$/;

		if(questiontype == 3){
			//Get field information
			var field = $("#type3-input").val();
		
			//Test against regex
			if(intregex.test(field)){
				$("#theform").submit();
			}else{
				alert("You entered an invalid value!");
			}
		}	
	});

});

</script>
<script src="./js/jquery.js"></script>
<script src="./js/bootstrap.min.js"></script>
</body>
</html>
