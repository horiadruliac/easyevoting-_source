<?php 
	require_once ('./include.php');
	require_once ('./auth.php');
	
	//echo("This page is currently running in test mode and is not secure");
	//we know the username from using authentication in include.php we just need to know if this user is an election administrator
	$statement = $db->prepare("SELECT * FROM BPA_Coordinators WHERE ID =?");
	$statement->bindValue(1, $_SERVER['PHP_AUTH_USER']);	
	$statement->execute();		
	$row = $statement->Fetch();
	if ($row == false)
	{
 		//not an administrator so display polite page saying that the user is currently not an administrator for any elections
		$smarty->display('BPA/noelections.tpl');
	}
	else
	{

		if (isset($_GET['submissionset']))
		{
			$submissionset = $_GET['submissionset'];
			$statement = $db->prepare("SELECT * FROM BPA_Submissions WHERE ID =?");
			$statement->bindValue(1, $submissionset);	
			$statement->execute();		
			$row = $statement->Fetch();
			if ($row == false)
			{
		 		//not a valid submission set
				$smarty->display('BPA/noelections.tpl');
			}
			else
			{
				$submissionset = $row[0];
				$smarty->assign('submissionset',$submissionset);
				if (time() < strtotime($row[1]))
				{
					//submission period is not open yet - check if this should just display a blank list, or if list should not be viewable until the end of submissions
					$smarty->display('BPA/submissionnotstarted.tpl');
				}
				else
				{
					if (isset($_GET['option']) && $_GET['option'] == "delete" && isset($_GET['id']))
					{		
						$id = $_GET['id'];
						if(isset($_GET['sure']) && $_GET['sure'] == "yes" && ctype_digit($submissionset))
						{
							$statement = $db->prepare("INSERT INTO BPA_Deletions VALUES (?,?,?)");
							$statement->bindValue(1, $_SERVER['PHP_AUTH_USER']);							
							$statement->bindValue(2, $submissionset);
							$statement->bindValue(3, $id);			
							$statement->execute();

							$statement = $db->prepare("DELETE FROM BPA_Papers_".$submissionset." WHERE ID = ?");
							$statement->bindValue(1, $id);			
							$statement->execute();

						}
						else
						{
							$smarty->assign('submissionset',$submissionset);
							$smarty->assign('submission',$id);
							$smarty->display('BPA/confirmdeletepaper.tpl');
							exit;
						}
					}
					
					if ((!isset($_GET['id']) || isset($_GET['option']))&&ctype_digit($submissionset)) //second condition causes list to still be displayed when a delete has happened
					{

						//get entire list of submissions and display
						$counter = 0;
						$submissions = null;
						$statement = $db->prepare("SELECT * FROM BPA_Papers_".$submissionset);
						$statement->execute();

						while ($row = $statement->Fetch())
						{
							$submissions[$counter]['id'] = $row[0];
							$submissions[$counter]['title'] = $row[2];	
							$submissions[$counter]['biblio'] = $row[5];
							$submissions[$counter]['testimony'] = $row[6];	

							$counter ++;					
						}
						$smarty->assign('submissions',$submissions);
						$smarty->display('BPA/viewsubmissionsadmin.tpl');
					}
					else
					{
						$id = $_GET['id'];
						//display this submission
						if (ctype_digit($submissionset))
						{
							$statement = $db->prepare("SELECT * FROM BPA_Papers_".$submissionset." WHERE ID = ?");
							$statement->bindValue(1, $id);
							$statement->execute();

							if($row = $statement->Fetch())
							{
								$submission['id'] = $row[0];
								$submission['title'] = $row[2];	
								$submission['biblio'] = $row[5];
								$submission['testimony'] = $row[6];	
								$temp = preg_split('% %',$row[6]);
								$submission['testimonylength']=sizeof($temp);
								$smarty->assign('submission',$submission);
								$smarty->display('BPA/viewsubmissionadmin.tpl');										
							}
							else
							{
								echo("No submission");
							}
						}

					}
						
					
						
				}
					
						
			}
		}
		else
		{
			 		//not a valid submission set
				$smarty->display('noelections.tpl');
		}
	}

			
?>