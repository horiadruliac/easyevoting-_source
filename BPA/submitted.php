<?php 
	require_once ('./include.php');
	require_once ('./auth.php');
	//echo("This page is operating in test mode and is not secure");
	
		//Original: $userID = mysql_real_escape_string($_SERVER['PHP_AUTH_USER']);
		$userID = $_SERVER['PHP_AUTH_USER'];
		
		//set some stuff here to show title
		
		if (!isset($_POST['submission']))
		{
			//change this to a custom page showing that no submission id has been given
						
			$smarty->display('submissionfailed.tpl');
		}
		else
		{
				//Originl: $submission = mysql_real_escape_string($_POST['submission']);
				$submission = $_POST['submission'];
				$statement = $db->prepare("SELECT * FROM BPA_Submissions WHERE ID =?");
				$statement->bindValue(1, $submission);	
				$statement->execute();
				$row = $statement->Fetch();
				if (!$row)
				{
					
					$smarty->display('BPA/submissionfailed.tpl');
					
				
				}
				else
				{
					//check if this is a new paper or an overwrite
					
					$submissionid = -1;
					if (isset($_POST['id']))
					{
						//Original: $id = mysql_real_escape_string($_POST['id']);						
						if (ctype_digit($submission))
						{
							$id = $_POST['id'];
							$statement = $db->prepare("SELECT * FROM BPA_Papers_".$submission." WHERE ID = ?");
							$statement->bindValue(1, $id);	
							$statement->execute();
							if($row = $statement->Fetch())	
							{
								$submissionid = $row[0];
							}
						}
						else
						{
							echo("Some Error!");
						}
					}
					
					//this is a new paper
					if ($submissionid==-1)
					{
						//generate id number
						$submissionid = 1;
						$statement = $db->prepare("SELECT ID FROM BPA_Papers_".$submission." ORDER BY ID DESC");
						$statement->execute();
						if($row = $statement->Fetch())	
						{
							$submissionid = ($row[0] + 1); 
						}
						
						if ($_FILES['uploadedpdf']['error']==0)
						{
							$fp      = fopen($_FILES['uploadedpdf']['tmp_name'], 'r');
							$content = fread($fp, filesize($_FILES['uploadedpdf']['tmp_name']));
							$content = addslashes($content);
							fclose($fp);
							$statement = $db->prepare("INSERT INTO BPA_Papers_".$submission." VALUES (?,?,?,?,'',?,?,?)");

							$statement->bindValue(1, $submissionid);
							$statement->bindValue(2, $userID);
							//Original: $statement->bindValue(3, mysql_real_escape_string($_POST['title']));
							$statement->bindValue(3, $_POST['title']);
							$statement->bindValue(4, $content,PDO::PARAM_LOB);
							//Original: $statement->bindValue(5, mysql_real_escape_string($_POST['biblio']));
							// Original: $statement->bindValue(6, mysql_real_escape_string($_POST['testimony']));	
							$statement->bindValue(5, $_POST['biblio']);
							$statement->bindValue(6, $_POST['testimony']);				
							$statement->bindValue(7, $_FILES['uploadedpdf']['type']);
							$statement->execute();
						}
						else
						{
							$smarty->display('BPA/nofile.tpl');
							exit;
						}
					}
					else
					{
					
						$submissionid = $row[0];
						if ($_FILES['uploadedpdf']['error']==0)
						{

							$fp      = fopen($_FILES['uploadedpdf']['tmp_name'], 'r');
							$content = fread($fp, filesize($_FILES['uploadedpdf']['tmp_name']));
							$content = addslashes($content);
							fclose($fp);
							$statement = $db->prepare("UPDATE BPA_Papers_".$submission." SET Paper = ?, PaperFileType = ? WHERE ID =?");
							$statement->bindValue(1, $content);
							$statement->bindValue(2, $_FILES['uploadedpdf']['type']);
							$statement->bindValue(3, $submissionid);
							$statement->execute();	
						
						}
						$statement = $db->prepare("UPDATE BPA_Papers_".$submission." SET Title = ?, BibDetails = ?, Testimony = ? WHERE ID = ?");
						//Original: $statement->bindValue(1, mysql_real_escape_string($_POST['title']));
						//Original: $statement->bindValue(2, mysql_real_escape_string($_POST['biblio']));
						//Original: $statement->bindValue(3, mysql_real_escape_string($_POST['testimony']));

						$statement->bindValue(1, $_POST['title']);
						$statement->bindValue(2, $_POST['biblio']);
						$statement->bindValue(3, $_POST['testimony']);					
						$statement->bindValue(4, $submissionid);
						$statement->execute();
					
					}
					$smarty->assign('title',$_POST['title']);
					$smarty->assign('biblio',$_POST['biblio']);
					$smarty->assign('testimony',$_POST['testimony']);					
					$smarty->assign('submissionset',$submission);
					$smarty->assign('id',$submissionid);					
					$smarty->display("BPA/papersubmitted.tpl");
				
				}
				
		}
			
						
?>