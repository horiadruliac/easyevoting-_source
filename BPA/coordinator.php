 <?php 
ini_set('display_errors', 1);
error_reporting(E_ALL);

	require_once ('./include.php');
	require_once ('./auth.php');
//echo("This page is running in test mode and is not currently secure");
	$statement = $db->prepare("SELECT * FROM BPA_Coordinators WHERE ID = ?");
	//echo(mysqli_real_escape_string($_SERVER['PHP_AUTH_USER']));
	$statement->bindValue(1, $_SERVER['PHP_AUTH_USER']);	
	$statement->execute();		
	$row = $statement->Fetch();

	if ($row == false)
	{
 		//not an administrator so display polite no page.
		$smarty->display('BPA/notcoordinator.tpl');
	}
	else
	{
		$smarty->display('templates/BPA/coordinator.tpl');		
	}	
?>