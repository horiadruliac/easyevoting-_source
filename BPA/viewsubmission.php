<?php 
ini_set('display_errors', 1);
error_reporting(E_ALL);

	require_once ('./include.php');
	

	if (isset($_GET['submissionset']) && ctype_digit($_GET['submissionset']))
	{
		$submissionset = ($_GET['submissionset']);
		$statement = $db->prepare("SELECT * FROM BPA_Submissions WHERE ID = ?");
		$statement->bindValue(1, $submissionset);	
		$statement->execute();		
		$row = $statement->Fetch();

		if ($row == false)
		{
	 		//not a valid submission set
			$smarty->display('BPA/invalidsubmissionnumber.tpl');
		}
		else
		{
			
			$smarty->assign('submissionset',$submissionset);
			if (time() < strtotime($row[1]))
			{
				//submission period is not open yet - check if this should just display a blank list, or if list should not be viewable until the end of submissions
				$smarty->display('BPA/submissionnotstarted.tpl');
			}
			else
			{
				if (!isset($_GET['id']))
				{

					//get entire list of submissions and display
					$counter = 0;
					$submissions = null;
					$statement = $db->prepare("SELECT * FROM BPA_Papers_".$submissionset);	
					$statement->execute();		
					while ($row = $statement->Fetch())
					{
						$submissions[$counter]['id'] = $row[0];
						$submissions[$counter]['title'] = $row[2];	
						$submissions[$counter]['biblio'] = $row[5];
						$submissions[$counter]['testimony'] = $row[6];	
						$counter ++;					
					}
					$smarty->assign('submissions',$submissions);
					$smarty->display('BPA/viewsubmissions.tpl');
				}
				else
				{
					//$id = mysql_real_escape_string($_GET['id']);
					//display this submission
					if (ctype_digit(submissionset))
					{
						$statement = $db->prepare("SELECT * FROM Papers_".$submissionset." WHERE ID = ?");
						$statement->bindValue(1, $_GET['id']);	
						$statement->execute();
						if($row = $statement->Fetch())
						{
							$submission['id'] = $row[0];
							$submission['title'] = $row[2];	
							$submission['biblio'] = $row[5];
							$submission['testimony'] = $row[6];	
							$smarty->assign('submission',$submission);
							$smarty->display('BPA/viewsubmission.tpl');										
						}
						else
						{
							echo("No submission");
						}
					}
				}
					
				
					
			}
				
					
		}
	}
	else
	{
		 		//not a valid submission set
			$smarty->display('BPA/noelections.tpl');
	}
	

			
?>