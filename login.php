<?php 
ini_set('display_errors', 1);
error_reporting(E_ALL);

	require_once ('./classroominclude.php');
	$smarty->assign('highlighted','none');
	
	if ( isset($_POST['sessionID']))
	{
		//echo($_POST['sessionID']);
		//echo($_POST['questions']);
		if (isset($_POST['questions']))
		{
			//do stuff based on what stage we're at
			//echo($_POST['questions']);
			if (isset($_POST['actionstring']))
			{
				//echo($_POST['actionstring']);
				//echo($_POST['questionType']);

				if ($_POST['questionType'] == 1 || $_POST['questionType'] == 2)
				{
					
					if ($_POST['actionstring'] == "confirm")
					{
						// send confirm XML
						$xmlstring ="<?xml version='1.0'?><classroomvoting><type>stagetwo</type><sessionid>".$_POST['sessionID']."</sessionid><userid>".$_POST['userID']."</userid><questionid>".$_POST['nextQuestion']."</questionid><choice>confirm</choice></classroomvoting>";							
						//echo($xmlstring);
						$xml = simplexml_load_string(contactServer($xmlstring));

						//echo("this is login page 1:".$xml);
						//if there is a question problem then flag
						if (!$xml)
						{
							$smarty->assign('questionID',$_POST['nextQuestion']);
							$smarty->assign('sessionID',$_POST['sessionID']);
							$smarty->assign('userID',$_POST['userID']);
							$smarty->assign('publicKey',$_POST['publicKey']);		
							$smarty->assign('questions',$_POST['questions']);
							$smarty->assign('receiptLength',$_POST['receiptLength']);
							$smarty->assign('previousreceipts',$_POST['previousreceipts']);	
							$smarty->assign('stageonereceipt',urldecode($_POST['stageonereceipt']));
							$smarty->assign('questionType',$_POST['questionType']);
							$smarty->display('confirmerror.tpl');
							exit;
						}

						
						if (!$xml->type|| (((string) $xml->type) == "error"))
						{
							if (((string) $xml->errortype) == "questionfinished")
							{
				
								$smarty->assign('questionID',$_POST['nextQuestion']);
								$smarty->assign('sessionID',$_POST['sessionID']);
								$smarty->assign('userID',$_POST['userID']);
								$smarty->assign('publicKey',$_POST['publicKey']);		
								$smarty->assign('questions',$_POST['questions']);
								$smarty->assign('receiptLength',$_POST['receiptLength']);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);	
								$smarty->display('webquestionfinished.tpl');

							}
							else if (((string) $xml->errortype) == "notstagetwo")
							{
								$smarty->assign('questionID',$_POST['nextQuestion']);
								$smarty->assign('sessionID',$_POST['sessionID']);
								$smarty->assign('userID',$_POST['userID']);
								$smarty->assign('publicKey',$_POST['publicKey']);		
								$smarty->assign('questions',$_POST['questions']);
								$smarty->assign('receiptLength',$_POST['receiptLength']);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);
								$smarty->display('webalreadyanswered.tpl');

							
							}
							else if (((string) $xml->errortype) == "questionalreadyanswered")
							{
								$smarty->assign('questionID',$_POST['nextQuestion']);
								$smarty->assign('sessionID',$_POST['sessionID']);
								$smarty->assign('userID',$_POST['userID']);
								$smarty->assign('publicKey',$_POST['publicKey']);		
								$smarty->assign('questions',$_POST['questions']);
								$smarty->assign('receiptLength',$_POST['receiptLength']);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);
								$smarty->display('webalreadyanswered.tpl');
								
							
							}							
							else
							{
								$smarty->assign('questionID',$_POST['nextQuestion']);
								$smarty->assign('sessionID',$_POST['sessionID']);
								$smarty->assign('userID',$_POST['userID']);
								$smarty->assign('publicKey',$_POST['publicKey']);		
								$smarty->assign('questions',$_POST['questions']);
								$smarty->assign('receiptLength',$_POST['receiptLength']);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);	
								$smarty->assign('stageonereceipt',urldecode($_POST['stageonereceipt']));
								$smarty->assign('questionType',$_POST['questionType']);
								$smarty->display('confirmerror.tpl');

							}
							exit;
						}	
						


						$smarty->assign('questionType',urldecode($_POST['questionType']));
						$smarty->assign('questionID',$_POST['nextQuestion']);
						$smarty->assign('sessionID',$_POST['sessionID']);
						$smarty->assign('userID',$_POST['userID']);
						$smarty->assign('publicKey',$_POST['publicKey']);		
						$smarty->assign('questions',$_POST['questions']);
						$smarty->assign('receiptLength',$_POST['receiptLength']);
						$smarty->assign('ballotnumber',(string) $xml->ballotnumber);	
						$smarty->assign('receipt',urldecode($_POST['stageonereceipt']));
						$smarty->assign('previousreceipts',$_POST['previousreceipts']."|".$_POST['nextQuestion']."@".(string) $xml->ballotnumber."@".urldecode($_POST['stageonereceipt']));
						if ($_POST['nextQuestion']<$_POST['questions'])
						{
							doQuestion($_POST['sessionID'],$_POST['questions'],$_POST['userID'],$_POST['publicKey'],$_POST['nextQuestion']+1,$_POST['receiptLength'],$_POST['previousreceipts']."|".$_POST['nextQuestion']."@".(string) $xml->ballotnumber."@".urldecode($_POST['stageonereceipt'])." ".substr((string) $xml->receipt,0,(int) $_POST['receiptLength']));	
						}
						else
						{
							$previousReceipts = $_POST['previousreceipts']."|".$_POST['nextQuestion']."@".(string) $xml->ballotnumber."@".urldecode($_POST['stageonereceipt']);
							$receipts = array();
							//split this and assign to array
							$tempReceipts = preg_split("/\|/",$previousReceipts);

							foreach ($tempReceipts as $i)
							{
								//throw away empty receipts
								if ($i!="")
								{
									$temp = preg_split("/\@/",$i);
									if ($temp[1]!="")
									{
										$receipt = array("value"=>$temp[2],"questionID"=>$temp[0],"ballotnumber"=>$temp[1]);
										array_push($receipts,$receipt);
									}
								}
							}
							$smarty->assign('receipts',$receipts);						
							$smarty->display('type12receipts.tpl');
							
						}
					}
					else if ($_POST['actionstring'] == "cancel")
					{
						// send cancel XML
						$xmlstring ="<?xml version='1.0'?><classroomvoting><type>stagetwo</type><sessionid>".$_POST['sessionID']."</sessionid><userid>".$_POST['userID']."</userid><questionid>".$_POST['nextQuestion']."</questionid><choice>cancel</choice></classroomvoting>";
//                                                echo("[[ post data: ".htmlentities($xmlstring)."]]<br>");
                                                $response = contactServer($xmlstring);
//                                                echo("[[ server response: ".htmlentities($response)."]]<br>");
                                                $xml = simplexml_load_string($response);
                                                
						//if there is a question problem then flag
						if (!$xml)
						{
								$smarty->assign('questionID',$_POST['nextQuestion']);
								$smarty->assign('questionType',$_POST['questionType']);
								$smarty->assign('sessionID',$_POST['sessionID']);
								$smarty->assign('userID',$_POST['userID']);
								$smarty->assign('publicKey',$_POST['publicKey']);		
								$smarty->assign('questions',$_POST['questions']);
								$smarty->assign('receiptLength',$_POST['receiptLength']);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);
								$smarty->assign('stageonereceipt',urldecode($_POST['stageonereceipt']));
								$smarty->display('cancelerror.tpl');
								exit;
						}
						
						if (!$xml->type|| (((string) $xml->type) == "error"))
						{
							if (((string) $xml->errortype) == "questionfinished")
							{
				
								$smarty->assign('questionID',$nextQuestion);
								$smarty->assign('sessionID',$sessionID);
								$smarty->assign('userID',$userID);
								$smarty->assign('publicKey',$publicKey);		
								$smarty->assign('questions',$questions);
								$smarty->assign('receiptLength',$receiptLength);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);						
								$smarty->display('webquestionfinished.tpl');

							}
							else if (((string) $xml->errortype) == "questionalreadyanswered")
							{
								$smarty->assign('questionID',$_POST['nextQuestion']);
								$smarty->assign('sessionID',$_POST['sessionID']);
								$smarty->assign('userID',$_POST['userID']);
								$smarty->assign('publicKey',$_POST['publicKey']);		
								$smarty->assign('questions',$_POST['questions']);
								$smarty->assign('receiptLength',$_POST['receiptLength']);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);
								$smarty->display('webalreadyanswered.tpl');
							
							
							}

							else if (((string) $xml->errortype) == "notstagetwo")
							{
								$smarty->assign('questionID',$_POST['nextQuestion']);
								$smarty->assign('sessionID',$_POST['sessionID']);
								$smarty->assign('userID',$_POST['userID']);
								$smarty->assign('publicKey',$_POST['publicKey']);		
								$smarty->assign('questions',$_POST['questions']);
								$smarty->assign('receiptLength',$_POST['receiptLength']);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);
								$smarty->display('webalreadyanswered.tpl');
							
							
							}
							else
							{
								$smarty->assign('questionID',$_POST['nextQuestion']);
								$smarty->assign('questionType',$_POST['questionType']);
								$smarty->assign('sessionID',$_POST['sessionID']);
								$smarty->assign('userID',$_POST['userID']);
								$smarty->assign('publicKey',$_POST['publicKey']);		
								$smarty->assign('questions',$_POST['questions']);
								$smarty->assign('receiptLength',$_POST['receiptLength']);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);
								$smarty->assign('stageonereceipt',urldecode($_POST['stageonereceipt']));
								$smarty->display('cancelerror.tpl');
			
							}
							exit;
						}

						
						
						
						// display receipt
						$smarty->assign('questionType',$_POST['questionType']);
						$smarty->assign('questionID',$_POST['nextQuestion']);
						$smarty->assign('sessionID',$_POST['sessionID']);
						$smarty->assign('userID',$_POST['userID']);
						$smarty->assign('publicKey',$_POST['publicKey']);		
						$smarty->assign('questions',$_POST['questions']);
						$smarty->assign('receiptLength',$_POST['receiptLength']);
						$smarty->assign('receipt',urldecode($_POST['stageonereceipt']));
						$smarty->assign('ballots',(string) $xml->ballotsremaining);
						$smarty->assign('ballotnumber',(string) $xml->ballotnumber);
						$smarty->assign('previousreceipts',$_POST['previousreceipts']);
						$smarty->assign('answertext',$xml->answers);
						$smarty->display('type12cancelled.tpl');
					
					}
					
				}
				else
				{
					//it's type 3 or 4 so go to confirm page and then send if it's confirmed
					if ($_POST['actionstring'] == "confirm")
					{
						$xmlstring ="<?xml version='1.0'?><classroomvoting><type>answer</type><sessionid>".$_POST['sessionID']."</sessionid><userid>".$_POST['userID']."</userid><questionid>".$_POST['nextQuestion']."</questionid><answer>".$_POST['answerstring']."</answer></classroomvoting>";
						$xml = simplexml_load_string(contactServer($xmlstring));
						//if there is a question problem then flag
						if(!$xml)
						{
								$smarty->assign('questionID',$_POST['nextQuestion']);
								$smarty->assign('sessionID',$_POST['sessionID']);
								$smarty->assign('userID',$_POST['userID']);
								$smarty->assign('publicKey',$_POST['publicKey']);		
								$smarty->assign('questions',$_POST['questions']);
								$smarty->assign('receiptLength',$_POST['receiptLength']);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);
								$smarty->assign('answerstring',$_POST['answerstring']);
								$smarty->assign('questionType',$_POST['questionType']);
								$smarty->display('type34confirmerror.tpl');
								exit;
						}
						
						if (!$xml->type|| (((string) $xml->type) == "error"))
						{
											
					
							if (((string) $xml->errortype) == "questionfinished")
							{
				
								$smarty->assign('questionID',$_POST['nextQuestion']);
								$smarty->assign('sessionID',$_POST['sessionID']);
								$smarty->assign('userID',$_POST['userID']);
								$smarty->assign('publicKey',$_POST['publicKey']);		
								$smarty->assign('questions',$_POST['questions']);
								$smarty->assign('receiptLength',$_POST['receiptLength']);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);
								$smarty->display('webquestionfinished.tpl');

							}
							else if (((string) $xml->errortype) == "questionalreadyanswered")
							{
								$smarty->assign('questionID',$_POST['nextQuestion']);
								$smarty->assign('sessionID',$_POST['sessionID']);
								$smarty->assign('userID',$_POST['userID']);
								$smarty->assign('publicKey',$_POST['publicKey']);		
								$smarty->assign('questions',$_POST['questions']);
								$smarty->assign('receiptLength',$_POST['receiptLength']);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);
								$smarty->display('webalreadyanswered.tpl');
							
							
							}
							else if (((string) $xml->errortype) == "invalidanswer")
							{
								$smarty->assign('questionID',$_POST['nextQuestion']);
								$smarty->assign('sessionID',$_POST['sessionID']);
								$smarty->assign('userID',$_POST['userID']);
								$smarty->assign('publicKey',$_POST['publicKey']);		
								$smarty->assign('questions',$_POST['questions']);
								$smarty->assign('receiptLength',$_POST['receiptLength']);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);
								$smarty->display('webinvalidanswer.tpl');
							}
							else
							{
								$smarty->assign('questionID',$_POST['nextQuestion']);
								$smarty->assign('sessionID',$_POST['sessionID']);
								$smarty->assign('userID',$_POST['userID']);
								$smarty->assign('publicKey',$_POST['publicKey']);		
								$smarty->assign('questions',$_POST['questions']);
								$smarty->assign('receiptLength',$_POST['receiptLength']);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);
								$smarty->assign('answerstring',$_POST['answerstring']);
								$smarty->assign('questionType',$_POST['questionType']);
								$smarty->display('type34confirmerror.tpl');
							
							}
							exit;
						}						
						// display confirm page
						$smarty->assign('questionText',urldecode($_POST['questionText']));
						$smarty->assign('questionType',$_POST['questionType']);
						$smarty->assign('questionID',$_POST['nextQuestion']);
						$smarty->assign('sessionID',$_POST['sessionID']);
						$smarty->assign('userID',$_POST['userID']);
						$smarty->assign('publicKey',$_POST['publicKey']);		
						$smarty->assign('questions',$_POST['questions']);
						$smarty->assign('answerstring',$_POST['answerstring']);
						$smarty->assign('receiptLength',$_POST['receiptLength']);
						$smarty->assign('previousreceipts',$_POST['previousreceipts']);
						if ($_POST['nextQuestion']<$_POST['questions'])
						{
							doQuestion($_POST['sessionID'],$_POST['questions'],$_POST['userID'],$_POST['publicKey'],$_POST['nextQuestion']+1,$_POST['receiptLength'],$_POST['previousreceipts']."|".$_POST['nextQuestion']."@".(string) $xml->ballotnumber."@".urldecode($_POST['stageonereceipt'])." ".substr((string) $xml->receipt,0,(int) $_POST['receiptLength']));	
						}
						else
						{
							$previousReceipts = $_POST['previousreceipts'];
							$receipts = array();
							//split this and assign to array
							$tempReceipts = preg_split("/\|/",$previousReceipts);

							foreach ($tempReceipts as $i)
							{
								//throw away empty receipts
								if ($i!="")
								{
									$temp = preg_split("/\@/",$i);
									if ($temp[1]!="")
									{
										$receipt = array("value"=>$temp[2],"questionID"=>$temp[0],"ballotnumber"=>$temp[1]);
										array_push($receipts,$receipt);
									}
								}
							}
							$smarty->assign('receipts',$receipts);						
							$smarty->display('type12receipts.tpl');
						}
					}
					else if ($_POST['actionstring'] == "cancel")
					{
						$smarty->assign('questionText',urldecode($_POST['questionText']));
						$smarty->assign('questionType',$_POST['questionType']);
						$smarty->assign('questionID',$_POST['nextQuestion']);
						$smarty->assign('sessionID',$_POST['sessionID']);
						$smarty->assign('userID',$_POST['userID']);
						$smarty->assign('publicKey',$_POST['publicKey']);		
						$smarty->assign('questions',$_POST['questions']);
						$smarty->assign('answerstring',$_POST['answerstring']);
						$smarty->assign('receiptLength',$_POST['receiptLength']);
						$smarty->assign('previousreceipts',$_POST['previousreceipts']);
						$smarty->display('type34cancelled.tpl');					
					}
				}

			}
			else if (isset($_POST['answerstring']))
			{
				//echo("Answer String: ".$_POST['answerstring']);
				//echo("Question Type: ".$_POST['questionType']);
				//echo("Answer1: ".$_POST['answer1']);

				if ($_POST['questionType'] == 1)
				{

					if (!isset($_POST['answer1']))
					{
						$smarty->assign('questionID',$_POST['nextQuestion']);
						$smarty->assign('sessionID',$_POST['sessionID']);
						$smarty->assign('userID',$_POST['userID']);
						$smarty->assign('publicKey',$_POST['publicKey']);		
						$smarty->assign('questions',$_POST['questions']);
						$smarty->assign('receiptLength',$_POST['receiptLength']);
						$smarty->assign('previousreceipts',$_POST['previousreceipts']);
						$smarty->display("webnoanswer.tpl");
						exit;
					}

					$xmlstring = "<?xml version='1.0'?><classroomvoting>	<type>answer</type><sessionid>".$_POST['sessionID']."</sessionid><questionid>".$_POST['nextQuestion']."</questionid><userid>".$_POST['userID']."</userid>";
					
					//get answer from answer1 attribute
					//echo($_POST['answer1']);

					$xmlstring = $xmlstring."<answer>".$_POST['answer1']."</answer></classroomvoting>";
					//send XML
//					echo("[[ post data: ".htmlentities($xmlstring)."]]<br>");
                                        $response = contactServer($xmlstring);
//					echo("[[ server response: ".htmlentities($response)."]]<br>");
					$xml = simplexml_load_string($response);
                                        
					//echo($xml);
                                        
					if (!$xml)
					{
						$smarty->assign('questionID',$_POST['nextQuestion']);
						$smarty->assign('sessionID',$_POST['sessionID']);
						$smarty->assign('userID',$_POST['userID']);
						$smarty->assign('publicKey',$_POST['publicKey']);		
						$smarty->assign('questions',$_POST['questions']);
						$smarty->assign('receiptLength',$_POST['receiptLength']);
						$smarty->assign('previousreceipts',$_POST['previousreceipts']);
						$smarty->assign('answer1',$_POST['answer1']);
						$smarty->assign('questionType',$_POST['questionType']);
						$smarty->assign('questionText',$_POST['questionText']);
						$smarty->assign('answertext',$_POST['answertext'.$_POST['answer1']]);
						$smarty->display('type1answererror.tpl');
						exit;
					}
	
						//if there is a question problem then flag
						if (!$xml->type|| (((string) $xml->type) == "error"))
						{
							if (((string) $xml->errortype) == "questionfinished")
							{
				
								$smarty->assign('questionID',$_POST['nextQuestion']);
								$smarty->assign('sessionID',$_POST['sessionID']);
								$smarty->assign('userID',$_POST['userID']);
								$smarty->assign('publicKey',$_POST['publicKey']);		
								$smarty->assign('questions',$_POST['questions']);
								$smarty->assign('receiptLength',$_POST['receiptLength']);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);
								$smarty->display('webquestionfinished.tpl');

							}
							else if (((string) $xml->errortype) == "notstageone")
							{
				
								$smarty->assign('questionID',$_POST['nextQuestion']);
								$smarty->assign('sessionID',$_POST['sessionID']);
								$smarty->assign('userID',$_POST['userID']);
								$smarty->assign('publicKey',$_POST['publicKey']);		
								$smarty->assign('questions',$_POST['questions']);
								$smarty->assign('receiptLength',$_POST['receiptLength']);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);
								$smarty->display('webalreadyanswered.tpl');

							}
							else if (((string) $xml->errortype) == "invalidanswer")
							{
								$smarty->assign('questionID',$_POST['nextQuestion']);
								$smarty->assign('sessionID',$_POST['sessionID']);
								$smarty->assign('userID',$_POST['userID']);
								$smarty->assign('publicKey',$_POST['publicKey']);		
								$smarty->assign('questions',$_POST['questions']);
								$smarty->assign('receiptLength',$_POST['receiptLength']);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);
								$smarty->display('webinvalidanswer1.tpl');
							}
							else
							{
								$smarty->assign('questionID',$_POST['nextQuestion']);
								$smarty->assign('sessionID',$_POST['sessionID']);
								$smarty->assign('userID',$_POST['userID']);
								$smarty->assign('publicKey',$_POST['publicKey']);		
								$smarty->assign('questions',$_POST['questions']);
								$smarty->assign('receiptLength',$_POST['receiptLength']);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);
								$smarty->assign('answer1',$_POST['answer1']);
								$smarty->assign('questionType',$_POST['questionType']);
								$smarty->assign('questionText',$_POST['questionText']);
								$smarty->assign('answertext',$_POST['answertext'.$_POST['answer1']]);
								$smarty->display('type1answererror.tpl');
								
							}
							exit;
						}						
						// display confirm page
						$smarty->assign('questionText',urldecode($_POST['questionText']));
						$smarty->assign('questionPass',$_POST['questionText']);
						$smarty->assign('questionType',$_POST['questionType']);
						$smarty->assign('questionID',$_POST['nextQuestion']);
						$smarty->assign('sessionID',$_POST['sessionID']);
						$smarty->assign('userID',$_POST['userID']);
						$smarty->assign('publicKey',$_POST['publicKey']);		
						$smarty->assign('questions',$_POST['questions']);
						$answers[0]=urldecode($_POST['answertext'.$_POST['answer1']]);
						$smarty->assign('answers',$answers);
						$smarty->assign('receiptLength',$_POST['receiptLength']);
						$smarty->assign('ballotnumber',(string) $xml->ballotid);
						$smarty->assign('receipt',substr((string) $xml->receipt,0,(int) $_POST['receiptLength']));
						$smarty->assign('stageone',urlencode(substr((string) $xml->receipt,0,(int) $_POST['receiptLength'])));
						$smarty->assign('previousreceipts',$_POST['previousreceipts']);
						$smarty->display('type12confirm.tpl');
						
				}
				else if( $_POST['questionType'] == 2)
				{
					//send answer then go to confirm page

					
					$xmlstring = "<classroomvoting>	<type>answer</type><sessionid>".$_POST['sessionID']."</sessionid><questionid>".$_POST['nextQuestion']."</questionid><userid>".$_POST['userID']."</userid><answerset>";
					$answerstring=null;
					$counter = 0;
					for ($i=1;$i<=$_POST['possibleanswers'];$i++)
					{
						if (isset($_POST['answer'.$i]))
						{
							$xmlstring = $xmlstring."<answer>".$_POST['answer'.$i]."</answer>";
							$answers[$counter] = urldecode($_POST['answertext'.$i]);
							$counter++;
						}
					}
					if ($counter == 0)
					{
						$smarty->assign('questionID',$_POST['nextQuestion']);
						$smarty->assign('sessionID',$_POST['sessionID']);
						$smarty->assign('userID',$_POST['userID']);
						$smarty->assign('publicKey',$_POST['publicKey']);		
						$smarty->assign('questions',$_POST['questions']);
						$smarty->assign('receiptLength',$_POST['receiptLength']);
						$smarty->assign('previousreceipts',$_POST['previousreceipts']);
						$smarty->display("webnoanswer.tpl");
						exit;
					}
					$answerstring = substr($answerstring,strlen($answerstring)-1);
					$xmlstring = $xmlstring."</answerset></classroomvoting>";
					//send XML
					$xml = simplexml_load_string(contactServer($xmlstring));
					//if there is a question problem then flag
					if (!$xml)
					{
						$smarty->assign('questionID',$_POST['nextQuestion']);
						$smarty->assign('sessionID',$_POST['sessionID']);
						$smarty->assign('userID',$_POST['userID']);
						$smarty->assign('publicKey',$_POST['publicKey']);		
						$smarty->assign('questions',$_POST['questions']);
						$smarty->assign('receiptLength',$_POST['receiptLength']);
						$smarty->assign('previousreceipts',$_POST['previousreceipts']);
						$smarty->assign('possibleanswers',$_POST['possibleanswers']);
						$smarty->assign('questionText',urldecode($_POST['questionText']));
						$smarty->assign('questionType',$_POST['questionType']);
						$answerValues = null;								
						for ($i=1;$i<=$_POST['possibleanswers'];$i++)
						{
							if (isset($_POST['answer'.$i]))
							{
								$answerValues[$counter]['text'] = urldecode($_POST['answertext'.$i]);
								$answerValues[$counter]['number'] = $i;
								$counter++;
							}
						}
						$smarty->assign('answers',$answerValues);
						$smarty->display('type2answererror.tpl');								
						exit;
					}
					
					if (!$xml->type|| (((string) $xml->type) == "error"))
					{
						if (((string) $xml->errortype) == "questionfinished")
							{
				
								$smarty->assign('questionID',$_POST['nextQuestion']);
								$smarty->assign('sessionID',$_POST['sessionID']);
								$smarty->assign('userID',$_POST['userID']);
								$smarty->assign('publicKey',$_POST['publicKey']);		
								$smarty->assign('questions',$_POST['questions']);
								$smarty->assign('receiptLength',$_POST['receiptLength']);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);
								$smarty->display('webquestionfinished.tpl');
							}
						else if (((string) $xml->errortype) == "notstageone")								
						{
								$smarty->assign('questionID',$_POST['nextQuestion']);
								$smarty->assign('sessionID',$_POST['sessionID']);
								$smarty->assign('userID',$_POST['userID']);
								$smarty->assign('publicKey',$_POST['publicKey']);		
								$smarty->assign('questions',$_POST['questions']);
								$smarty->assign('receiptLength',$_POST['receiptLength']);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);
								$smarty->display('webalreadyanswered.tpl');
							}
							else if (((string) $xml->errortype) == "invalidanswer")								
							{
								$smarty->assign('questionID',$_POST['nextQuestion']);
								$smarty->assign('sessionID',$_POST['sessionID']);
								$smarty->assign('userID',$_POST['userID']);
								$smarty->assign('publicKey',$_POST['publicKey']);		
								$smarty->assign('questions',$_POST['questions']);
								$smarty->assign('receiptLength',$_POST['receiptLength']);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);
								$smarty->display('webinvalidanswer2.tpl');
							}
							else
							{
								$smarty->assign('questionID',$_POST['nextQuestion']);
								$smarty->assign('sessionID',$_POST['sessionID']);
								$smarty->assign('userID',$_POST['userID']);
								$smarty->assign('publicKey',$_POST['publicKey']);		
								$smarty->assign('questions',$_POST['questions']);
								$smarty->assign('receiptLength',$_POST['receiptLength']);
								$smarty->assign('previousreceipts',$_POST['previousreceipts']);
								$smarty->assign('possibleanswers',$_POST['possibleanswers']);
								$smarty->assign('questionText',urldecode($_POST['questionText']));
								$smarty->assign('questionType',$_POST['questionType']);
								$answerValues = null;								
								for ($i=1;$i<=$_POST['possibleanswers'];$i++)
								{
									if (isset($_POST['answer'.$i]))
									{
										$answerValues[$counter]['text'] = urldecode($_POST['answertext'.$i]);
										$answerValues[$counter]['number'] = $i;
										$counter++;
									}
								}
								$smarty->assign('answers',$answerValues);
								$smarty->display('type2answererror.tpl');
								
							}
							exit;
					}	
					//
					
					
					//display confirm page
					$smarty->assign('questionText',urldecode($_POST['questionText']));
					$smarty->assign('questionPass',$_POST['questionText']);
					$smarty->assign('questionType',$_POST['questionType']);
					$smarty->assign('questionID',$_POST['nextQuestion']);
					$smarty->assign('sessionID',$_POST['sessionID']);
					$smarty->assign('userID',$_POST['userID']);
					$smarty->assign('publicKey',$_POST['publicKey']);		
					$smarty->assign('questions',$_POST['questions']);
					$smarty->assign('answers',$answers);
					$smarty->assign('ballotnumber',(string) $xml->ballotid);
					$smarty->assign('receiptLength',$_POST['receiptLength']);
					$smarty->assign('receipt',substr((string) $xml->receipt,0,(int) $_POST['receiptLength']));
											$smarty->assign('stageone',urlencode(substr((string) $xml->receipt,0,(int) $_POST['receiptLength'])));
					$smarty->assign('previousreceipts',$_POST['previousreceipts']);
					$smarty->display('type12confirm.tpl');
					
					
				}
				else
				{
					//it's type 3 or 4 so go to confirm page and then send it if it's confirmed
					$smarty->assign('questionText',urldecode($_POST['questionText']));
					$smarty->assign('questionPass',$_POST['questionText']);
					$smarty->assign('questionType',$_POST['questionType']);
					$smarty->assign('questionID',$_POST['nextQuestion']);
					$smarty->assign('sessionID',$_POST['sessionID']);
					$smarty->assign('userID',$_POST['userID']);
					$smarty->assign('publicKey',$_POST['publicKey']);		
					$smarty->assign('questions',$_POST['questions']);
					$smarty->assign('receiptLength',$_POST['receiptLength']);
					$smarty->assign('answerstring',$_POST['answerstring']);
					$smarty->assign('previousreceipts',$_POST['previousreceipts']);
					$smarty->display('type34confirm.tpl');
				}

			}
			else if ($_POST['nextQuestion']>$_POST['questions'])
			{
				$smarty->assign('sessionID',$_POST['sessionID']);
				$previousReceipts = $_POST['previousreceipts'];
				$receipts = array();
				//split this and assign to array
				$tempReceipts = preg_split("/\|/",$previousReceipts);

				foreach ($tempReceipts as $i)
				{
					//throw away empty receipts
					if ($i!="")
					{
						$temp = preg_split("/\@/",$i);
						if ($temp[1]!="")
						{
							$receipt = array("value"=>$temp[2],"questionID"=>$temp[0],"ballotnumber"=>$temp[1]);
							array_push($receipts,$receipt);
						}
					}
				}
				$smarty->assign('receipts',$receipts);						
				$smarty->display('type12receipts.tpl');
			}
			else
			{
			
				doQuestion($_POST['sessionID'],$_POST['questions'],$_POST['userID'],$_POST['publicKey'],$_POST['nextQuestion'],$_POST['receiptLength'],$_POST['previousreceipts']);
			}
		
		}
		else if (isset($_POST['passcode']))
		{
			//echo($_POST['passcode']);
			//contact the server and see if we can login
                        
//			echo("[[ session: ".$_POST['sessionID']."; passcode: ".$_POST['passcode']. "]]<br>");
                    
			$xmlstring ="<?xml version='1.0'?><classroomvoting><type>join</type><sessionid>".$_POST['sessionID']."</sessionid><passcode>".$_POST['passcode']."</passcode></classroomvoting>";
			
			$result = contactServer($xmlstring);
			$xml = simplexml_load_string($result);
			
                        $questions = null;
			$userID = null;
			$publicKey = null;
			$nextQuestion = null;
			if (!$xml || !$xml->type || (((string) $xml->type) == "error"))
			{
				if (!$xml || !$xml->errortype)
				{
					$smarty->display('webnoresponse.tpl');
					exit;
				}
				else if ($xml->errortype == "sessiongenerating")
				{
					$smarty->display('websessiongenerating.tpl');
					exit;
				}
				else if ($xml->errortype == "invalidpasscode")
				{
					$smarty->display('webinvalidpasscode.tpl');
					exit;
				}
				else if ($xml->errortype == "usedpasscode")
				{
					$smarty->display('webusedpasscode.tpl');
					exit;
				}				
				else if ($xml->errortype == "usersexhausted")
				{
					$smarty->display('webmaximumusers.tpl');
					exit;
				}
				else if ($xml->errortype == "invalidsession")
				{
					$smarty->display('webinvalidsession.tpl');
					exit;
				}
				else if ($xml->errortype == "lockedsession")
				{
					$smarty->display('websessionlocked.tpl');
					exit;
				}
				else if ($xml->errortype == "sessionfinished")
				{
					$smarty->display('websessionfinished.tpl');
					exit;
				}

				
			}
			else
			{
				$questions = (string) $xml->questions;
				$userID = (string) $xml->userID;
				$publicKey = (string) $xml->publickey;
				$receiptLength = (string) $xml->receiptlength;
				$nextQuestion = 1;
				doQuestion($_POST['sessionID'],$questions,$userID,$publicKey,$nextQuestion,$receiptLength,"");
			}
				
				
		}
		else
		{
			$smarty->display('login.tpl');
		}
			
				
			
	}
	else
	{
		$smarty->display('login.tpl');
	}
	
	
	function contactServer ($xmlstring)
	{
		//echo("<br/><br/>");
		$url = $_SERVER['REQUEST_SCHEME']."://". $_SERVER['SERVER_NAME'] .str_replace(basename($_SERVER['REQUEST_URI'], ".php"), "classroomvoting", $_SERVER['REQUEST_URI']);

               //echo($url);
        
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlstring);
                
		$data = curl_exec($ch);
//                echo "<p>".htmlentities("[[ curl call: ".$url."; post fields: ".$xmlstring."; result: ".$data."]]")."</p>";
                
		curl_close($ch);
		return $data;
	}
	
	function doQuestion ($sessionID,$questions,$userID,$publicKey,$nextQuestion,$receiptLength,$previousReceipts)
	{
		global $smarty;
		//contact the server
		$xmlstring ="<?xml version='1.0'?><classroomvoting><type>getquestion</type><sessionid>".$sessionID."</sessionid><userid>".$userID."</userid><questionid>".$nextQuestion."</questionid></classroomvoting>";
		//echo($xmlstring);
		$temp = contactServer($xmlstring);
		//echo($temp);
		$xml = simplexml_load_string($temp);
		//if there is a question problem then flag
		if (!$xml->type|| (((string) $xml->type) == "error"))
		{
			if (((string) $xml->errortype) == "lockedquestion")
			{
					$smarty->assign('questionID',$nextQuestion);
			$smarty->assign('sessionID',$sessionID);
			$smarty->assign('userID',$userID);
			$smarty->assign('publicKey',$publicKey);		
			$smarty->assign('questions',$questions);
			$smarty->assign('receiptLength',$receiptLength);
			$smarty->assign('previousreceipts',$previousReceipts);
			$smarty->display('webquestionlocked.tpl');
				exit;
			}
			else
			{
				$smarty->assign('questionID',$nextQuestion);
				$smarty->assign('sessionID',$sessionID);
				$smarty->assign('userID',$userID);
				$smarty->assign('publicKey',$publicKey);		
				$smarty->assign('questions',$questions);
				$smarty->assign('receiptLength',$receiptLength);
				$smarty->assign('previousreceipts',$previousReceipts);
				$smarty->display('questionerror.tpl');
				exit;
			}
		}
		//display question
		$questionType = (string) $xml->questiontype->typenumber;
		$smarty->assign('questionText',(string) $xml->questiontext);
		$smarty->assign('questionPass',urlencode((string) $xml->questiontext));
		$smarty->assign('questionType',$questionType);
		$smarty->assign('questionID',$nextQuestion);
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('userID',$userID);
		$smarty->assign('publicKey',$publicKey);		
		$smarty->assign('questions',$questions);
		$smarty->assign('receiptLength',$receiptLength);
		$smarty->assign('previousreceipts',$previousReceipts);
		if (((string) $xml->finished) == "yes")
		{
			$smarty->display('webquestionfinished.tpl');
			exit;
		}
		if ($questionType == "1" || $questionType == "2")
		{	
			// get answers

			$answers = null;
			$urlanswers = null;
			$counter = 0;
			foreach ($xml->answers->children() as $answer)
			{
				$answers[$counter]['answer'] = (string) $answer->text;
				$answers[$counter]['number'] = (string) $answer->number;
				$urlanswers[$counter]['answer'] = urlencode((string) $answer->text);
				$urlanswers[$counter]['number'] = (string) $answer->number;			
				$counter++;
			}
			
			$smarty->assign('answers',$answers);
			$smarty->assign('urlanswers',$urlanswers);
			$smarty->assign('possibleanswers',$counter);

			
			//display page
			if ($questionType == "2")
			{
				$maxanswers = (string) $xml->questiontype->maxanswers;
				$smarty->assign('maxanswers',$maxanswers);
				$smarty->display('type2WebQuestion.tpl');
			}
			else
			{
				$smarty->assign('maxanswers',"1");
				$smarty->display('type1WebQuestion.tpl');
			}

		
		}
		else
		{
			$smarty->assign('previousreceipts',$previousReceipts);
			$smarty->display('type34WebQuestion.tpl');
		}
		
	}
	
?>
