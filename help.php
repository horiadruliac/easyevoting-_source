<?php
require_once ('./classroominclude.php');

$smarty->assign('highlighted','help');

session_start();
if (isset($_SESSION['USERNAME']))
{
	$smarty->assign('username',$_SESSION['USERNAME']);
}
if(isset($_SESSION['is_admin']))
{
		$smarty->assign('admin','true');
}	
$smarty->display('help/help.tpl');
?>