 <?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

	require_once ('./classroominclude.php');
	require_once ('./getclassroomsignature.php');
	if ( isset($_GET['sessionid']))
	{
		$sessionID=$_GET['sessionid'];
		$statement = $db->prepare("UPDATE Type3Questions SET finished = true WHERE sessionID = ?");
		$statement->bindValue(1, $sessionID);	
		$statement->execute();	

		$statement = $db->prepare("UPDATE Type4Questions SET finished = true WHERE sessionID = ?");
		$statement->bindValue(1, $sessionID);	
		$statement->execute();		

		$statement = $db->prepare("UPDATE Type12Questions SET finished = true WHERE sessionID = ?");
		$statement->bindValue(1, $sessionID);	
		$statement->execute();	
			//loop through all ballots that don't have a stage two receipt
			$statement = $db->prepare("SELECT ballotID,stageOneReceipt,questionID FROM Ballots WHERE sessionID = ? AND stageTwoReceipt is NULL");
			$statement->bindValue(1, $sessionID);	
			$statement->execute();		
			while($row = $statement->Fetch())
			{
				$questionNumber = $row[2];
				//echo("ballot number is ".$row[0]."<br/>");
				//check if they have a stage one receipt
				if (!is_null($row[1]))
				{				
					//if yes, do a cancel
						
					//generate stage two xml
					$receipt="<ballotreceipt>";
					$receipt=$receipt."<sessionid>".$sessionID."</sessionid>";
					$receipt=$receipt."<questionid>".$questionNumber."</questionid>";
					$receipt=$receipt."<stage>2</stage>";	
					$receipt=$receipt."<ballotid>".$row[0]."</ballotid>";
					$receipt=$receipt."<type>audit</type>";	
					$statement2 = $db->prepare("SELECT currentChoice FROM Type12Status WHERE sessionID = ? AND questionID = ? AND currentBallot = ?");
					$statement2->bindValue(1, $sessionID);	
					$statement2->bindValue(2, $questionNumber);
					$statement2->bindValue(3, $row[0]);					
					$statement2->execute();					
					$row2 = $statement2->Fetch();
					$answer = preg_split("%,%",$row2[0]);
					$choiceString = $row2[0];
		
					//loop through all possible answers and add the opposite XML to what was given at stage one
					$statement2 = $db->prepare("SELECT answerNumber,publicKey,restructuredKey,yesValue,noValue FROM BallotXML WHERE sessionID = ? AND questionID = ? AND ballotID = ?");
					$statement2->bindValue(1, $sessionID);
					$statement2->bindValue(2, $questionNumber);
					$statement2->bindValue(3, $row[0]);	
					$statement2->execute();	

					// loop through answers
					while($row2 = $statement2->Fetch())
					{
						$receipt=$receipt."<answer>";
						$receipt=$receipt."<answertext>";
						$statement3 = $db->prepare("SELECT answerText FROM Type12Answers WHERE sessionID = ? AND questionID = ? AND answerNumber = ?");
						$statement3->bindValue(1, $sessionID);
						$statement3->bindValue(2, $questionNumber);
						$statement3->bindValue(3, $row[0]);	
						$statement3->execute();	
						$row3 = $statement3->Fetch();
						$receipt=$receipt.$row3[0];
						$receipt=$receipt."</answertext>";			
						$receipt=$receipt.$row2[1];			
						$receipt=$receipt.$row2[2];				
						//for each answer loop through choices and if this is a choice include yes XML else include no XML
						$chosen = false;
						for ($i=0;$i<count($answer);$i++)
						{
							if ($row2[0] == $answer[$i])
							{
								$chosen = true;
							}
			
						}
						//get the opposite value to the one for stage one
						if ($chosen)
						{
							$receipt=$receipt.$row2[4];	
						}
						else
						{
							//change this so that it says yesvalue
							$receiptValue=str_replace("<value>","<yesvalue>",$row2[3]);
							$receiptValue=str_replace("<proof>","<yesproof>",$receiptValue);
							$receiptValue = str_replace("</value>","</yesvalue>",$receiptValue);
							$receiptValue = str_replace("</proof>","</yesproof>",$receiptValue);				
							$receipt=$receipt.$receiptValue;	
						}
						$receipt=$receipt."</answer>";			
						//add in the stage one signature
						//get stage one receipt
						$statement2 = $db->prepare("SELECT stageOneReceipt FROM Ballots WHERE sessionID = ? AND questionID = ? AND ballotID = ?");
						$statement2->bindValue(1, $sessionID);
						$statement2->bindValue(2, $questionNumber);
						$statement2->bindValue(3, $row[0]);	
						$statement2->execute();	
						$row2 = $statement2->Fetch();
						//cut up to get signature
						$stageOneSignature = preg_split("%<signature>%",$row2[0]);
						$stageOneSignature = $stageOneSignature[1];
						$stageOneSignature = preg_split("%</signature>%",$stageOneSignature);
						$stageOneSignature = $stageOneSignature[0];
						$receipt = $receipt."<stageonesignature>".$stageOneSignature."</stageonesignature>";
			
						// sign the receipt
						$statement3 = $db->prepare("SELECT value FROM Constants WHERE constant = 'privateKey'");
						$statement3->execute();	
						$row3 = $statement3->Fetch();
																
						$signature = getSignature($receipt."<key>".$row3[0]."</key>"."</ballotreceipt>");
						$receipt = $receipt."<signature>".$signature."</signature></ballotreceipt>";
			
			
						//clear temporary choices and decrement the amount of ballots allowed
						$statement2 = $db->prepare("UPDATE Type12Status SET currentBallot = null, currentChoice=null, status = 0 WHERE sessionID = ? AND questionID = ? AND currentBallot = ?");
						$statement2->bindValue(1, $sessionID);
						$statement2->bindValue(2, $questionNumber);
						$statement2->bindValue(3, $row[0]);	
						$statement2->execute();	
			
		
			
						//store receipt in ballots table
						$statement2 = $db->prepare("UPDATE Ballots SET stageTwoReceipt = ?, status = 3, cancelledFor = ? WHERE sessionID = ? AND questionID = ? AND ballotID = ?");
						$statement2->bindValue(1, $receipt);
						$statement2->bindValue(2, $choiceString);						
						$statement2->bindValue(3, $sessionID);
						$statement2->bindValue(4, $questionNumber);
						$statement2->bindValue(5, $row[0]);	
						$statement2->execute();	
			
							
						}	
						
					}
					else
					{
						//if no, do an unused
						//generate stage two xml
						$receipt="<ballotreceipt>";
						$receipt=$receipt."<sessionid>".$sessionID."</sessionid>";
						$receipt=$receipt."<questionid>".$questionNumber."</questionid>";
						$receipt=$receipt."<stage>2</stage>";
						$receipt=$receipt."<ballotid>".$row[0]."</ballotid>";						
						$receipt=$receipt."<type>unused</type>";	
						
		
						//loop through all possible answers and add the opposite XML to what was given at stage one
						$statement2 = $db->prepare("SELECT answerNumber,publicKey,restructuredKey,yesValue,noValue FROM BallotXML WHERE sessionID = ? AND questionID = ? AND ballotID = ?");
						$statement2->bindValue(1, $sessionID);
						$statement2->bindValue(2, $questionNumber);
						$statement2->bindValue(3, $row[0]);	
						$statement2->execute();

						// loop through answers
						while($row2 = $statement2->Fetch())
						{
							$receipt=$receipt."<answer>";
							$receipt=$receipt."<answertext>";
							$statement3 = $db->prepare("SELECT answerText FROM Type12Answers WHERE sessionID = ? AND questionID = ? AND answerNumber = ?");
							$statement3->bindValue(1, $sessionID);
							$statement3->bindValue(2, $questionNumber);
							$statement3->bindValue(3, $row2[0]);	
							$statement3->execute();
							$row3 = $statement3->Fetch();
							$receipt=$receipt.$row3[0];
							$receipt=$receipt."</answertext>";			
							$receipt=$receipt.$row2[1];			
							$receipt=$receipt.$row2[2];				

							$receipt=$receipt.$row2[4];	

							//change this so that it says yesvalue
							$receiptValue=str_replace("<value>","<yesvalue>",$row2[3]);
							$receiptValue=str_replace("<proof>","<yesproof>",$receiptValue);
							$receiptValue = str_replace("</value>","</yesvalue>",$receiptValue);
							$receiptValue = str_replace("</proof>","</yesproof>",$receiptValue);				
							$receipt=$receipt.$receiptValue;	
							$receipt=$receipt."</answer>";
					
			
							
			
							
						}	
						// sign the receipt
						$statement3 = $db->prepare("SELECT value FROM Constants WHERE constant = 'privateKey'");
						$statement3->execute();
						$row3 = $statement3->Fetch();
																
						$signature = getSignature($receipt."<key>".$row3[0]."</key>"."</ballotreceipt>");
						$receipt = $receipt."<signature>".$signature."</signature></ballotreceipt>";
					
						//store receipt in ballots table
						$statement3 = $db->prepare("UPDATE Ballots SET stageTwoReceipt = ?, status = 3 WHERE sessionID = ? AND questionID = ? AND ballotID = ?");
						$statement3->bindValue(1, $receipt);	
						$statement3->bindValue(2, $sessionID);
							$statement3->bindValue(3, $questionNumber);
							$statement3->bindValue(4, $row[0]);	
							$statement3->execute();
					}
				}
							$smarty->display('finishedallquestions.tpl');
		}
	
			
	else
	{
		//maybe add some sort of error here
	}
?>
