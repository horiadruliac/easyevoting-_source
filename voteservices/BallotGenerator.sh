#!/bin/bash
MAINDIR=.

if [ "$1" == 'start' ]
 then
 umask 002 
 if [ -f $MAINDIR/ClassroomGenerator.pid ]
 then
 hashPID="$(<$MAINDIR/ClassroomGenerator.pid)"
  kill $hashPID

  while [[ ( -d /proc/$hashPID ) && ( -z `grep zombie /proc/$hashPID/status` ) ]]; do
    sleep 1
done
  
  rm -f $MAINDIR/ClassroomGenerator.pid
fi


 if [ -f $MAINDIR/ClassroomSigGen.pid ]
 then
 hashPID="$(<$MAINDIR/ClassroomSigGen.pid)"
  kill $hashPID

  while [[ ( -d /proc/$hashPID ) && ( -z `grep zombie /proc/$hashPID/status` ) ]]; do
    sleep 1
done

  rm -f $MAINDIR/ClassroomSigGen.pid
fi


  echo "Starting Classroom Signature Generator"
  touch $MAINDIR/ClassroomSigGen.pid
  nohup java -jar $MAINDIR/signatures.jar $MAINDIR/ 1>/dev/null 2>&1 &
  echo $! >> $MAINDIR/ClassroomSigGen.pid
 
  echo "Starting Classroom Generator"
  touch $MAINDIR/ClassroomGenerator.pid
  nohup java -jar $MAINDIR/generator.jar $MAINDIR/ 1>/dev/null 2>&1 &
  echo $! >> $MAINDIR/ClassroomGenerator.pid
 
 
elif [ "$1" = 'stop' ]
 then 
# if [ -f BallotGenerator.pid ]
# then
#  echo "Stopping Ballot Generator"
#  ballotPID="$(<BallotGenerator.pid)"
#  kill $ballotPID
#  rm -f BallotGenerator.pid
# else
#  echo "Ballot Generator is not running"
# fi
 
# if [ -f SignatureGenerator.pid ]
# then
#  signaturePID="$(<SignatureGenerator.pid)"
#  kill $signaturePID
#  rm -f SignatureGenerator.pid
# else
#  echo "Signature Generator is not running"
# fi

 #if [ -f /addon/local/httpd/html/HashGenerator.pid ]
# then
#  echo "Stopping Hash Generator"
#  hashPID="$(</addon/local/httpd/html/HashGenerator.pid)"
#  kill $hashPID
#  rm -f /addon/local/httpd/html/HashGenerator.pid
# else
#  echo "Hash Generator is not running"
# fi
 
  if [ -f $MAINDIR/ClassroomSigGen.pid ]
 then
  echo "Stopping Classroom Signature Generator"
  hashPID="$(<$MAINDIR/ClassroomSigGen.pid)"
  kill $hashPID
  rm -f $MAINDIR/ClassroomSigGen.pid
 else
  echo "Classroom Signature Generator is not running"
 fi
 
  if [ -f $MAINDIR/ClassroomGenerator.pid ]
 then
  echo "Stopping Classroom Generator"
  hashPID="$(<$MAINDIR/ClassroomGenerator.pid)"
  kill $hashPID
  rm -f $MAINDIR/ClassroomGenerator.pid
 else
  echo "Classroom Generator is not running"
 fi


fi

exit

