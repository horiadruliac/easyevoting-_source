<?php 
ini_set('display_errors', 1);
error_reporting(E_ALL);

	require_once ('./classroominclude.php');
	require_once ('./gethash.php');
	$smarty->assign('highlighted','bulletinboard');
	session_start();
	if (isset($_SESSION['USERNAME']))
	{
		$smarty->assign('username',$_SESSION['USERNAME']);
	}
	if(isset($_SESSION['is_admin']))
	{
			$smarty->assign('admin','true');
	}	
	if (isset($_GET['questionid']) && $_GET['questionid']!="" && isset($_GET['sessionid']) && ctype_digit($_GET["questionid"]) && ctype_digit($_GET["sessionid"]))
  	{
                // $sessionID=mysql_real_escape_string($sessionID); // PDO already does it, in more recent versions of PHP it may give an error/warning and clear the string
		// $questionID=mysql_real_escape_string($_GET['questionid']);		
		$sessionID=$_GET['sessionid'];
                $questionID=$_GET['questionid'];
		
		$smarty->assign('sessionID',$sessionID);
		$smarty->assign('questionID',$questionID);
		
		if ($sessionID =="")
		{
			$smarty->display('classroombulletinboard/classroombulletinmenu.tpl');
			exit;
		}
		
		//check if this is a hidden question and display menu if it is
		$statement = $db->prepare("SELECT hidden FROM Sessions WHERE sessionID=?");
		$statement->bindValue(1, $sessionID);	
		$statement->execute();		

		if ($row = $statement->Fetch())
		{
			if ($row[0])
			{
				$smarty->display('allresults/hidden.tpl');
				exit;
			}
		}


		//check these are real question and session IDs
		
		//if not do title page
		
		$statement = $db->prepare("SELECT finished,questionText FROM Type12Questions WHERE sessionID = ? AND questionID = ?");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $questionID);	
		$statement->execute();		

		$row = $statement->Fetch();
		if ($row == false)
		{
			$statement = $db->prepare("SELECT questionText,finished FROM Type3Questions WHERE sessionID = ? AND questionID = ?");
			$statement->bindValue(1, $sessionID);
			$statement->bindValue(2, $questionID);	
			$statement->execute();	
			$row = $statement->Fetch();
			if ($row == false)
			{
				$statement = $db->prepare("SELECT questionText,finished FROM Type4Questions WHERE sessionID = ? AND questionID = ?");
				$statement->bindValue(1, $sessionID);
				$statement->bindValue(2, $questionID);	
				$statement->execute();	
				$row = $statement->Fetch();
				if ($row == false)
				{
					$smarty->display('classroombulletinboard/classroombulletinmenu.tpl');
				}
				else
				{
					$smarty->assign('questiontext',$row[0]);		
					$smarty->assign('finished',$row[1]);
			
					$statement = $db->prepare("SELECT answer FROM Type4Answers WHERE sessionID = ? AND questionID = ?");
					$statement->bindValue(1, $sessionID);
					$statement->bindValue(2, $questionID);	
					$statement->execute();	
									

					$counter = 0;
					$results = null;
					while ($row = $statement->Fetch())
					{
						$results[$counter]['answer']=$row[0];	
						$counter++;					
					}
					$smarty->assign('results',$results);
					$smarty->display('classroombulletinboard/results4.tpl');
				}
			}
			else
			{
				$smarty->assign('questiontext',$row[0]);		
				$smarty->assign('finished',$row[1]);
				$statement = $db->prepare("SELECT answer,COUNT(answer) FROM Type3Answers WHERE sessionID = ? AND questionID = ? GROUP BY answer ORDER BY count(answer) DESC,answer ASC");
				$statement->bindValue(1, $sessionID);
				$statement->bindValue(2, $questionID);	
				$statement->execute();				

				$counter = 0;
				$results = null;
				while ($row = $statement->Fetch())
				{
					$results[$counter]['answer']=$row[0];	
					$results[$counter]['votes']=$row[1];	
					$counter++;					
				}
				$smarty->assign('results',$results);
				$smarty->display('classroombulletinboard/results3.tpl');
			}
		}
		else
		{
			$questionText=$row[1];
			$smarty->assign('questiontext',$questionText);
			if ($row[0] == true)
			{
				$statement = $db->prepare("SELECT answerText,votesCast FROM Type12Answers WHERE sessionID = ? AND questionID = ? ORDER BY votesCast DESC");
				$statement->bindValue(1, $sessionID);
				$statement->bindValue(2, $questionID);	
				$statement->execute();	

				$counter = 0;
				$results = null;
				while ($row = $statement->Fetch())
				{
					$results[$counter]['answer']=$row[0];
					$results[$counter]['votes']=$row[1];	
					$counter++;					
				}
				$smarty->assign('results',$results);
				$statement = $db->prepare("SELECT count(*) FROM Ballots WHERE sessionID = ? AND questionID = ? AND status = 2");
				$statement->bindValue(1, $sessionID);
				$statement->bindValue(2, $questionID);	
				$statement->execute();	
				$row = $statement->Fetch();
				$smarty->assign('confirmed',$row[0]);
				
				$statement = $db->prepare("SELECT count(*) FROM Ballots WHERE sessionID = ? AND questionID = ? AND status = 3 AND stageOneReceipt IS NULL");
				$statement->bindValue(1, $sessionID);
				$statement->bindValue(2, $questionID);	
				$statement->execute();	
				$row = $statement->Fetch();
				$smarty->assign('unused',$row[0]);
				$statement = $db->prepare("SELECT count(*) FROM Ballots WHERE sessionID = ? AND questionID = ? AND status = 3 AND stageOneReceipt IS NOT NULL");
				$statement->bindValue(1, $sessionID);
				$statement->bindValue(2, $questionID);	
				$statement->execute();	
				$row = $statement->Fetch();
				$smarty->assign('cancelled',$row[0]);	

			}
			if (isset($_GET['machine']))
			{
				if ($_GET['machine'] == "ballotreceipts")
				{
					$receipt = "<ballotreceipts>";
					// add in parameters
					$statement = $db->prepare("SELECT value FROM Constants WHERE constant = 'publicKey'");	
					$statement->execute();
	
					$row = $statement->Fetch();
					$receipt=$receipt."<publickey>".$row[0]."</publickey>";
					$statement = $db->prepare("SELECT securityLevel FROM Sessions WHERE sessionID = ?");
					$statement->bindValue(1, $sessionID);	
					$statement->execute();	
					$row = $statement->Fetch();

					$statement = $db->prepare("SELECT ParaA,ParaB,PointX,PointY,PrimeModulus,PointOrder FROM ECParameters WHERE securityLevel = ?");
					$statement->bindValue(1, $row[0]);	
					$statement->execute();	
					$row = $statement->Fetch();				
					$receipt=$receipt."<a>".$row[0]."</a><b>".$row[1]."</b><primemodulus>".$row[4]."</primemodulus><order>".$row[5]."</order><pointx>".$row[2]."</pointx><pointy>".$row[3]."</pointy>";				
					//add in results
					$receipt=$receipt."<results>";
					$statement = $db->prepare("SELECT answerText,votesCast FROM Type12Answers WHERE sessionID = ? AND questionID = ? ORDER BY answerNumber");
					$statement->bindValue(1, $sessionID);
					$statement->bindValue(2, $questionID);	
					$statement->execute();	

					while ($row = $statement->Fetch())
					{
						$receipt=$receipt."<result><answertext>".$row[0]."</answertext> <votes>".$row[1]."</votes></result>";
					}
					$receipt=$receipt."</results>";	
					$statement = $db->prepare("SELECT stageOneReceipt, stageTwoReceipt FROM Ballots WHERE sessionID = ? AND questionID = ? ORDER BY ballotID");
					$statement->bindValue(1, $sessionID);
					$statement->bindValue(2, $questionID);	
					$statement->execute();			
					while ($row = $statement->Fetch())
					{
						if (!is_null($row[0]))
						{
							$receipt = $receipt.$row[0];
						}
						if (!is_null($row[1]))
						{
							$receipt = $receipt.$row[1];
						}
					}
					$receipt = $receipt."</ballotreceipts>";
					$smarty->assign('receipt',$receipt);
				}
				else if ($_GET['machine'] == "publickeys")
				{
					$receipt = "<ballotreceipts>";
					// add in parameters
					$statement = $db->prepare("SELECT value FROM Constants WHERE constant = 'publicKey'");	
					$statement->execute();	
;
					$row = $statement->Fetch();
					$receipt=$receipt."<publickey>".$row[0]."</publickey>";
					$statement = $db->prepare("SELECT securityLevel FROM Sessions WHERE sessionID = ?");
					$statement->bindValue(1, $sessionID);	
					$statement->execute();	
					$row = $statement->Fetch();
					$statement = $db->prepare("SELECT ParaA,ParaB,PointX,PointY,PrimeModulus,PointOrder FROM ECParameters WHERE securityLevel = ?");
					$statement->bindValue(1, $row[0]);	
					$statement->execute();	
					$row = $statement->Fetch();			
					$receipt=$receipt."<a>".$row[0]."</a><b>".$row[1]."</b><primemodulus>".$row[4]."</primemodulus><order>".$row[5]."</order><pointx>".$row[2]."</pointx><pointy>".$row[3]."</pointy>";				
					$receipt=$receipt."<sessionid>".$sessionID."</sessionid>";
					$receipt=$receipt."<questionid>".$questionID."</questionid>";				
					//add in results
					$receipt=$receipt."<ballots>";
					$statement = $db->prepare("SELECT answerText,ballotID,publicKey,restructuredKey FROM BallotXML inner join Type12Answers WHERE BallotXML.answerNumber = Type12Answers.answerNumber AND BallotXML.questionID = Type12Answers.questionID AND BallotXML.sessionID = Type12Answers.sessionID AND BallotXML.sessionID = ? AND BallotXML.questionID = ? ORDER BY answerText");
					$statement->bindValue(1, $sessionID);
					$statement->bindValue(2, $questionID);	
					$statement->execute();	
	
					while ($row = $statement->Fetch())
					{
						$receipt=$receipt."<ballot><answertext>".$row[0]."</answertext><ballotid>".$row[1]."</ballotid>".$row[2].$row[3]."</ballot>";
					}
					$receipt=$receipt."</ballots>";	
					$receipt = $receipt."</ballotreceipts>";	
					$smarty->assign('receipt',$receipt);					
				}

				$smarty->display('classroombulletinboard/classroommachinereceipts.tpl');
			}
			else if (isset($_GET['ballot']))
			{
				$ballot = $_GET['ballot'];
				$statement = $db->prepare("SELECT stageOneReceipt,stageTwoReceipt,status,cancelledFor FROM Ballots WHERE sessionID = ? AND questionID = ? AND ballotID = ?");
				$statement->bindValue(1, $sessionID);
				$statement->bindValue(2, $questionID);
				$statement->bindValue(3, $ballot);	
				$statement->execute();

				$row = $statement->Fetch();
				if ($row == false)
				{
					$smarty->display('classroombulletinboard/classroombulletinmenu.tpl');
				}
				else
				{
					$statement2 = $db->prepare("SELECT receiptLength FROM Sessions WHERE sessionID = ?");
					$statement2->bindValue(1, $sessionID);
					$statement2->execute();
					$row2 = $statement2->Fetch();
					$smarty->assign('ballotNumber',$_GET['ballot']);
					$smarty->assign('sessionID',$sessionID);					
					$smarty->assign('questionID',$questionID);					
					
					if (is_null($row[0]))
					{
						if (!is_null($row[1]))
						{
							$smarty->assign('type',"unused");
							$smarty->assign('receiptString',generateReceipt($row[1],$row2[0]));
						}
					}
					else
					{
						if (is_null($row[1]))
						{
							$smarty->assign('type',"inprogress");
							$smarty->assign('receiptString',generateReceipt($row[0],$row2[0]));
						}
						else
						{
							//check if audit or confirmed
							if ($row[2] == 3)
							{
								$smarty->assign('type',"audit");
								//TODO modify to get names rather than numbers
								$cancelString ="";

								$cancelArray = preg_split("%,%",$row[3]);
								for ($i=0;$i<count($cancelArray)-1;$i++)
								{
									$statement3 = $db->prepare("SELECT answerText FROM Type12Answers WHERE sessionID = ? AND questionID = ? AND answerNumber = ?");
									$statement3->bindValue(1, $sessionID);
									$statement3->bindValue(2, $questionID);
									$statement3->bindValue(3, $cancelArray[$i]);
									$statement3->execute();
									$row3 = $statement3->Fetch();
									$cancelString = $cancelString.$row3[0].",";
								}
								if (count($cancelArray) >0)
								{
									$statement3 = $db->prepare("SELECT answerText FROM Type12Answers WHERE sessionID = ? AND questionID = ? AND answerNumber = ?");
									$statement3->bindValue(1, $sessionID);
									$statement3->bindValue(2, $questionID);
									$statement3->bindValue(3, $cancelArray[(count($cancelArray)-1)]);
									$statement3->execute();
									$row3 = $statement3->Fetch();
									$cancelString = $cancelString.$row3[0];
								}
								$smarty->assign('cancelchoice',$cancelString);
							}
							else
							{							
								$smarty->assign('type',"confirm");
							}
//							$smarty->assign('receiptString',generateReceipt($row[0],$row2[0])." ".generateReceipt($row[1],$row2[0]));
							$smarty->assign('receiptString',generateReceipt($row[0],$row2[0]));						
						}
					}
					
					$smarty->display('classroombulletinboard/classroomreceipt.tpl');
				}
			}
			else
			{
				$statement = $db->prepare("SELECT ballotID,status FROM Ballots WHERE sessionID = ? AND questionID = ?");
				$statement->bindValue(1, $sessionID);
				$statement->bindValue(2, $questionID);	
				$statement->execute();
				$ballots=null;
				while ($row = $statement->Fetch())
				{
					//do this properly
					if ($row[1] == 0)
					{
						$ballots[$row[0]]["link"]=-1;
					}
					else
					{
						$ballots[$row[0]]["link"]=$row[0];
					}
					$ballots[$row[0]]["number"]=$row[0];
				}
				$smarty->assign('ballots',$ballots);
				$smarty->display('classroombulletinboard/classroombulletinlist.tpl');
			}
		}
	}
	else if (isset($_GET['sessionid']) && ctype_digit($_GET["sessionid"]))
	{
                // $sessionID=mysql_real_escape_string($sessionID); // PDO already does it, in more recent versions of PHP it may give an error/warning and clear the string
		$sessionID=$_GET['sessionid'];	
		
		$smarty->assign('sessionID',$sessionID);
		

		
		//check this is a real session ID
		
		if ($sessionID == "")
		{		
			$smarty->display('classroombulletinboard/classroombulletinmenu.tpl');
			exit;
		}

		//Get title
		$statement = $db->prepare("SELECT title,hidden FROM Sessions WHERE sessionID = ?");
		$statement->bindValue(1, $sessionID);	
		$statement->execute();

		if($row = $statement->Fetch())
		{
			if ($row[1])
			{
				$smarty->display('allresults/hidden.tpl');
				exit;
			}
		
			$title = $row[0];
			if($title == "") $title = "&lt;None&gt;";
			$smarty->assign('sessionTitle',$title);
		}
		else
		{
			$smarty->display('classroombulletinboard/classroombulletinmenu.tpl');
			exit;
		}
		
		$results = null;
		$statement = $db->prepare("SELECT finished,questionText,questionID FROM Type12Questions WHERE sessionID = ?");
		$statement->bindValue(1, $sessionID);	
		$statement->execute();
		while ($row = $statement->Fetch())
		{
			$results[$row[2]]['type'] = "12";
			$results[$row[2]]['questiontext'] = $row[1];
			$results[$row[2]]['questionnumber'] = $row[2];
			if ($row[0] == true)
			{	
				$results[$row[2]]['finished']=true;
				$statement2 = $db->prepare("SELECT answerText,votesCast FROM Type12Answers WHERE sessionID = ? AND questionID = ? ORDER BY answerNumber");
				$statement2->bindValue(1, $sessionID);	
				$statement2->bindValue(2, $row[2]);
				$statement2->execute();

				$counter = 0;
				while ($row2 = $statement2->Fetch())
				{
					$results[$row[2]]['results'][$counter]['answer']=$row2[0];
					$results[$row[2]]['results'][$counter]['votes']=$row2[1];	
					$counter++;					
				}			

			}
			else
			{
				$results[$row[2]]['finished']=false;				
			}
		}
		$statement = $db->prepare("SELECT questionText,questionID,finished FROM Type3Questions WHERE sessionID = ?");
		$statement->bindValue(1, $sessionID);	
		$statement->execute();
		while($row = $statement->Fetch())
		{
			$results[$row[1]]['questiontext'] = $row[0];
			$results[$row[1]]['questionnumber'] = $row[1];			
			$results[$row[1]]['finished'] = $row[2];			
			$results[$row[1]]['type'] = "3";
			$statement2 = $db->prepare("SELECT answer, COUNT(answer) FROM Type3Answers WHERE sessionID = ? AND questionID = ? GROUP BY answer ORDER BY COUNT(answer) DESC, answer ASC");
			$statement2->bindValue(1, $sessionID);	
			$statement2->bindValue(2, $row[1]);
			$statement2->execute();		

			$counter = 0;
			while ($row2 = $statement2->Fetch())
			{
				$results[$row[1]]['results'][$counter]['answer']=$row2[0];
				$results[$row[1]]['results'][$counter]['votes']=$row2[1];
				$counter++;					
			}

		}
		$statement = $db->prepare("SELECT questionText,questionID,finished FROM Type4Questions WHERE sessionID = ?");
		$statement->bindValue(1, $sessionID);	
		$statement->execute();
		while($row = $statement->Fetch())
		{
			$results[$row[1]]['questiontext'] = $row[0];		
			$results[$row[1]]['finished'] = $row[2];			
			$results[$row[1]]['type'] = "4";
			$statement2 = $db->prepare("SELECT answer FROM Type4Answers WHERE sessionID = ? AND questionID = ?");
			$statement2->bindValue(1, $sessionID);	
			$statement2->bindValue(2, $row[1]);
			$statement2->execute();			
			$counter = 0;
			while ($row2 = $statement2->Fetch())
			{
				$results[$row[1]]['results'][$counter]['answer']=$row2[0];	
				$counter++;					
			}

		}
		if (!is_null($results))
		{
			ksort($results);
		}
		$smarty->assign('results',$results);
		$smarty->display('allresults/allresults.tpl');
	}
	else
	{ 
		$smarty->display('classroombulletinboard/classroombulletinmenu.tpl');
	}



